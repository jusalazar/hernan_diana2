<?php

/**
 * Description of MY_Controller
 * @property CI_Loader $load
 * @author mato
 * @property CI_Form_validation $form_validation
 * @property CI_Output $output
 * @property CI_Session $session
 * @property CI_Router $router
 * @property General_model $general
 * @property Twig_Loader_Filesystem $loader
 * @property Twig_Environment $twig
 *
 *
 */
class AW_Controller extends MX_Controller {

    /**
     *
     * @var Twig_Loader_Filesystem $loader
     */
    protected $loader;

    /**
     *
     * @var Twig_Environment $twig
     */
    protected $twig;
    protected $templateDefault;

    function __construct() {
        parent::__construct();
        $this->load->model("general_model", "general");
        $this->load->library("mobdetector");
        $this->mobile_detect = new Mobdetector();
        $paths = array(
            APPPATH . 'views/generic_templates',
            APPPATH . "modules" . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . "templates"
        );
        $this->loader = new Twig_Loader_Filesystem($paths);
        $this->twig = new Twig_Environment($this->loader, TWIG_OPTIONS);
        $this->addTwigFunctions();
    }

    function requiereLogin() {
        if (!Auth::estaLogeado()) {
            redirect("bienvenido");
        }
    }

    private function addTwigFunctions() {
        $this->load->model("tools/Usuario_model", "usuario");
        $this->twig->addGlobal('session', $_SESSION);
        $this->twig->addGlobal('server', $_SERVER);
        $this->twig->addGlobal('get', $this->input->get());
        $this->twig->addGlobal('metodo_actual', $this->router->fetch_method());
        $this->twig->addGlobal('esta_logeado', Auth::estaLogeado());
        $this->twig->addGlobal('es_mobile', $this->mobile_detect->isCelular());
        $this->twig->addGlobal('usuario', $this->usuario->getPerfil());
        $this->twig->addGlobal('base_url_path', base_url());
        $function = new Twig_Function("base_url", function ($url) {
            return base_url($url);
        });
        $this->twig->addFunction($function);
        $form_open = new Twig_Function("form_open", function($action, $atributos = array(), $hidden = array()) {
            return form_open($action, $atributos, $hidden);
        });
        $this->twig->addFunction($form_open);

        $form_open_multi = new Twig_Function("form_open_multipart", function($action, $atributos = array(), $hidden = array()) {
            return form_open_multipart($action, $atributos, $hidden);
        });
        $this->twig->addFunction($form_open_multi);

        $token_name = new Twig_Function("get_token_name", function() {
            return $this->security->get_csrf_token_name();
        });
        $this->twig->addFunction($token_name);
        $token_hash = new Twig_Function("get_token_hash", function() {
            return $this->security->get_csrf_hash();
        });
        $this->twig->addFunction($token_hash);



        $this->twig->addExtension(new Twig_Extension_Debug());
    }

    function loadView($vista, $data, $output = false) {
        $this->load->view($vista, $data, $output);
    }

    function getCantidadOF() {
        return $this->cantidadOF;
    }

    function getCantidadClientes() {
        return $this->cantidadClientes;
    }

    function post($args = false) {
        if ($args) {
            return $this->input->post($args);
        } else {
            return $this->input->post();
        }
    }

    function activarDebug() {
        $this->output->enable_profiler(true);
    }

    function imprimirPost($terminar = true) {
        echo '<pre>';
        print_r($this->post());
        if ($terminar) {
            die();
        }
    }

    function loadTemplateDefault($vista = false, array $datos = null, $template = false) {
        $datos['vista'] = $vista;
        if ($template) {
            $this->load->view($template, $datos);
        } else {
            $this->load->view($this->getTemplateDefault(), $datos);
        }
    }

    function getTemplateDefault() {
        return $this->templateDefault;
    }

    function setTemplateDefault($templateDefault) {
        $this->templateDefault = $templateDefault;
    }

}
