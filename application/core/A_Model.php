<?php

/**
 * Description of A_model
 *
 * @author mato
 * @property Doctrine\ORM\EntityManager $orm
 * @property CI_Loader $load
 * @property  CI_DB_query_builder $db
 */
class A_Model extends CI_Model {

    function startStackDoctrine(&$orm) {
        $doctrineConnection = $orm->getConnection();
        $stack = new \Doctrine\DBAL\Logging\DebugStack();
        $doctrineConnection->getConfiguration()->setSQLLogger($stack);
        return $stack;
    }

    public function getCollectionByid($clase, $id) {

        $bag = $this->orm->getRepository($clase);

        return $bag->find($id);
    }

    function getAllCollection($clase) {

        $bag = $this->orm->getRepository($clase);

        return $bag->findAll();
    }

    function __construct() {
        parent::__construct();
        $this->load->library("doctrine");
        $this->orm = $this->doctrine->em;
    }

    function getQuery($consulta) {
        return $this->orm->createQuery($consulta)->getResult();
    }

    function getArrayQuery($consulta) {
        return $this->orm->createQuery($consulta)->getArrayResult();
    }

    function post($args = false) {
        if ($args) {
            return $this->input->post($args);
        } else {
            return $this->input->post();
        }
    }

    function persistir($objeto) {
        $this->orm->persist($objeto);
        $this->orm->flush();
    }

    function getObjeto($clase, $id) {
        return $this->orm->find($clase, $id);
    }

    /**
     *
     * @return Doctrine\ORM\QueryBuilder
     */
    function getQueryBuilder() {
        return $this->orm->createQueryBuilder();
    }

    public function get_mensaje($texto, $exito = true) {
        if ($exito) {
            $color_cartel = "success";
        } else {
            $color_cartel = "danger";
        }
        return array("mensaje" => $texto, "exito" => $color_cartel);
    }

    public function toJson($objetos) {
        $response = array();
        if (is_array($objetos)) {
            foreach ($objetos as $item) {
                $response[] = $item->toJson();
            }
            return $response;
        }
        return $objetos->toJson();
    }

    public function mandaMail($para, $titulo, $texto, $archivo_array = false) {
        $mailing = new \Web\Mailing();
        $mailing->setSubject($titulo);
        $mailing->setFecha_in(new \DateTime());
        $mailing->setUsuario(get_user_id() ? get_user_id() : 1);
        $mailing->setDestino($para);
        $this->orm->persist($mailing);
        $this->orm->flush();
        $texto = trim($texto);
        if (strlen($texto) < 1000) {
            $mailing->setMensaje($texto);
        } else {
            file_put_contents(MAILS_PATH . "template_" . $mailing->getId(), $texto);
            $mailing->setTemplate("template_" . $mailing->getId());
        }
        $this->orm->flush();
        //exec("php ".PATH_BACKGROUND." web Background_controller enviarEmailsPendientes > /dev/null 2>&1 &");
    }

    public function mandaMailDirecto(&$mail, $para, $titulo, $texto, $archivo_array = false) {
        $mail->AddAddress($para, MAIL_FROM_NAME);
        $mail->SMTPDebug = 2;
        if ($archivo_array) {
            //$mail->AddAttachment(FACTURAS_COMPRAS . $archivo_array["archivo"], $archivo_array["nombre"], $archivo_array["base"], $archivo_array["type"]);
        }
        //$mail->addCC(CC_MAIL2);
        $mail->isHTML(true);
        $mail->Subject = $titulo;
        $mail->Body = $texto;
        if (!$mail->send()) {
            $response["status"] = false;
            $response["mensaje"] = $mail->ErrorInfo;
            return $response;
        } else {
            $response["status"] = true;
            $response["mensaje"] = "Mail enviado correctamente";
            return $response;
        }
    }

}
