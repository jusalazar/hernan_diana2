<?php

/**
 * Description of MY_Controller
 * @property CI_Loader $load
 * @author mato
 * @property CI_Form_validation $form_validation
 * @property CI_Output $output
 * @property CI_Session $session
 * @property CI_Router $router
 * @property General_model $general
 * @property Twig_Loader_Filesystem $loader
 * @property Twig_Environment $twig
 *
 *
 */
class A_Controller extends MX_Controller {

    /**
     *
     * @var Twig_Loader_Filesystem $loader
     */
    protected $loader;

    /**
     *
     * @var Twig_Environment $twig
     */
    protected $twig;

    function __construct() {

        parent::__construct();
        $this->controlarSesion();
        $this->load->model("general_model", "general");
        $paths = array(
            APPPATH . 'views/generic_templates',
            APPPATH . "modules" . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "templates",
            APPPATH . "modules" . DIRECTORY_SEPARATOR . "tools" . DIRECTORY_SEPARATOR . "templates"
        );
        $this->loader = new Twig_Loader_Filesystem($paths);
        $this->twig = new Twig_Environment($this->loader, TWIG_OPTIONS);
        $this->addTwigFunctions();
    }

    private function addTwigFunctions() {
        $this->twig->addGlobal('session', $_SESSION);
        $this->twig->addGlobal('get', $this->input->get());
        $this->twig->addGlobal('server', $_SERVER);
        $function = new Twig_Function("base_url", function ($url) {
            return base_url($url);
        });
        $this->twig->addFunction($function);
        $form_open = new Twig_Function("form_open", function($action, $atributos = array(), $hidden = array()) {
            return form_open($action, $atributos, $hidden);
        },array('is_safe' => array('html')));
        $this->twig->addFunction($form_open);
        $form_close = new Twig_Function("form_close", function() {
            return '</form>';
        }, array('is_safe' => array('html')));
        $this->twig->addFunction($form_close);

        $form_open_multi = new Twig_Function("form_open_multipart", function($action, $atributos = array(), $hidden = array()) {
            return form_open_multipart($action, $atributos, $hidden);
        });
        $this->twig->addFunction($form_open_multi);

        $token_name = new Twig_Function("get_token_name", function() {
            return $this->security->get_csrf_token_name();
        });
        $this->twig->addFunction($token_name);
        $token_hash = new Twig_Function("get_token_hash", function() {
            return $this->security->get_csrf_hash();
        });
        $this->twig->addFunction($token_hash);

        $form_date = new Twig_Function("form_date", function($name,$value = "",$clase = "",$id = "",$atributos = "") {
            return form_date($name,$value,$clase,$id,$atributos);
        }, array('is_safe' => array('html')));
        $this->twig->addFunction($form_date);

        $this->twig->addExtension(new Twig_Extension_Debug());
    }

    function loadView($vista, $data, $output = false) {
        $this->load->view($vista, $data, $output);
    }

    function getCantidadOF() {
        return $this->cantidadOF;
    }

    function getCantidadClientes() {
        return $this->cantidadClientes;
    }

    function post($args = false) {
        if ($args) {
            return $this->input->post($args);
        } else {
            return $this->input->post();
        }
    }

    private function controlarSesion() {
        if ($this->session->has_userdata("logged")) {
            if (!$this->session->logged) {
                if ($this->router->class != "login_controller") {
                    redirect("login");
                }
            }
        } else {
            $this->completarSession();
            redirect("login");
        }
    }

    private function completarSession() {
        $data = array(
            "logged" => FALSE,
            "usu_id" => "0",
            "nombre" => "",
            "rol_id" => "0",
            "rol_nombre" => "",
            "usu_nombre" => "",
            "rol_panel" => "",
            "cantidad_intentos" => 0,
            "error" => ""
        );
        $this->session->set_userdata($data);
    }

    function activarDebug() {
        $this->output->enable_profiler(true);
    }

    function imprimirPost($terminar = true) {
        echo '<pre>';
        print_r($this->post());
        if ($terminar) {
            die();
        }
    }

    function sinPermiso() {
        $respuesta['mensaje'] = "No tiene permisos para realizar accion";
        $respuesta['exito'] = "danger";
    }

}
