<?php

function cargarModelosPropios($class) {
    $class = str_replace("\\", DIRECTORY_SEPARATOR, $class);
    $clases = explode(DIRECTORY_SEPARATOR, $class);

    $class = end($clases);
    $claseReal = $class;
    //echo $class.'<br>';
    if (strpos($class, 'CI_') !== 0 && strpos($class, 'MX_') !== 0) {
        $ruta_pre = APPPATH . "modules" . DIRECTORY_SEPARATOR;
        $ruta_post = DIRECTORY_SEPARATOR . "clases" . DIRECTORY_SEPARATOR;
        $ubicaciones = array("admin", "pedidos", "tools", "web");
        $default = APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'modelo' . DIRECTORY_SEPARATOR . $class . ".php";
        $encontrado = FALSE;
        if (file_exists($default)) {
            include_once($default);
            $encontrado = TRUE;
        }

        if (!$encontrado) {
            //echo $class . '<br>';
            foreach ($ubicaciones as $item) {
                $archivo = $ruta_pre . $item . $ruta_post . $claseReal . ".php";
                //echo $class . ' en ruta:' . $archivo . "<br>";
                if (file_exists($archivo)) {
                    //echo 'encontrado<br>';
                    include_once($archivo);
                } else {
                    continue;
                }
            }
        }
    }
}

spl_autoload_register("cargarModelosPropios");

