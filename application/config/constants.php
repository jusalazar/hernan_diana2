<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
* |--------------------------------------------------------------------------
* | Display Debug backtrace
* |--------------------------------------------------------------------------
* |
* | If set to TRUE, a backtrace will be displayed along with php errors. If
* | error_reporting is disabled, the backtrace will not display, regardless
* | of this setting
* |
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
* |--------------------------------------------------------------------------
* | File and Directory Modes
* |--------------------------------------------------------------------------
* |
* | These prefs are used when checking and setting modes when working
* | with the file system. The defaults are fine on servers with proper
* | security, but you may wish (or even need) to change the values in
* | certain environments (Apache running a separate process for each
* | user, PHP under CGI with Apache suEXEC, etc.). Octal values should
* | always be used to set the mode correctly.
* |
*/
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);


/*
* |--------------------------------------------------------------------------
* | File Stream Modes
* |--------------------------------------------------------------------------
* |
* | These modes are used when working with fopen()/popen()
* |
*/
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
* |--------------------------------------------------------------------------
* | Exit Status Codes
* |--------------------------------------------------------------------------
* |
* | Used to indicate the conditions under which the script is exit()ing.
* | While there is no universal standard for error codes, there are some
* | broad conventions. Three such conventions are mentioned below, for
* | those who wish to make use of them. The CodeIgniter defaults were
* | chosen for the least overlap with these conventions, while still
* | leaving room for others to be defined in future versions and user
* | applications.
* |
* | The three main conventions used for determining exit status codes
* | are as follows:
* |
* | Standard C/C++ Library (stdlibc):
* | http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
* | (This link also contains other GNU-specific conventions)
* | BSD sysexits.h:
* | http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
* | Bash scripting:
* | http://tldp.org/LDP/abs/html/exitcodes.html
* |
*/
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code






define("TWIG_OPTIONS", array(
    'cache' => FALSE,
    'debug' => TRUE
));
if (ENVIRONMENT == "production") {
    define("TWIG_OPTIONS", array(
        'cache' => APPPATH . 'views' . DIRECTORY_SEPARATOR . 'compilation_cache',
        'debug' => FALSE
    ));
}
/* MAKER OLD */
define('PATH_FORMULARIO_DINAMICO', 'public/formularios/');
define('MODULE', 'admin');
define('MODULE_NAMESPACE', 'Admin\\');
define('ROUTE_PATH', 'application/config/routes.php');
define('CONTROLLER', 'application/modules/' . MODULE . '/controllers/');
define('ENTITY', 'application/modules/' . MODULE . '/clases/');
define('MODELO', 'application/modules/' . MODULE . '/models/');
define('VIEW', 'application/modules/' . MODULE . '/templates/');

/* FOTOS PRODUCTOS */
define('FOTOS_PINTURAS', 'public' . DIRECTORY_SEPARATOR . 'pinturas' . DIRECTORY_SEPARATOR);
define('FOTOS_ARTISTAS', 'public' . DIRECTORY_SEPARATOR . 'artistas' . DIRECTORY_SEPARATOR);
define('FOTO_PRODUCTO_WIDTH', 1920);
define('FOTO_PRODUCTO_HEIGHT', 1080);


/* PUNTO VENTA */
define('RADIO_ACCION', '5000');

/* LIMITES QUERYES WEB */
define('MAX_RESULT_BUSQUEDA', 18);

/* MAILING */
define("MAIL_HOST", "mail.argit.com.ar");
define('MAIL_PORT', 465);
define('MAIL_SECURE', "ssl");
define('MAIL_USER', 'gdedieu@argit.com.ar');
define("MAIL_PASSWORD", "Urudata25267!");
define('MAIL_SUBNAME', "PaintNow");
define('MAIL_FROM', "gdedieu@argit.com.ar");
define('MAIL_FROM_NAME', "PaintNow");
define('MAILS_PATH', "public/mails_pendientes/");
define('CC_MAIL', "gdedieu@arg-it.com");
define('CC_MAIL2', "gdedieu@arg-it.com");


define('MAIL_TEMPLATE_PATH', 'public/templates_mail/');
define('JS_LOG_ERROR', 'public/log_js/');
define('SYSTEM_TITLE', 'Hernan Diana');
define('SYSTEM_TITLE_SHORT', 'HD');
define('WEB_TITLE_APP', "Hernan Diana");

/*MP*/
define('MP_ESTADO_SUCCESS', 'approved');
define('MP_ESTADO_INPROGRESS', 'in_process');
define('MP_ESTADO_PENDING', 'pending');
define('MP_ESTADO_REJECTED', 'rejected');
define("CLIENT_ID","4827419171820660");
define("CLIENT_TOKEN","LfP2UDR92srMkrCuWtBRXOYJ8CuyD4KD");

define('PREFIX_COMPRA',"PAINTC#");
define('GMAPS_KEY','AIzaSyBmZkx2SlvDbhVMx4hhklPRTHNzRFzJrEQ');


/*tipo pago alquiler*/
define('TIPO_PAGO_MENSUAL','mensual');
define('TIPO_PAGO_CUATRIMESTRAL','cuatrimestral');
define('TIPO_PAGO_SEMESTRAL','semestral');
define('TIPO_PAGO_ANUAL','anual');

/*reportes*/
define('PATH_TEXTOS_REPORTES', 'public/textos/');
define('HEIGHT_FIELD',11);
define('HEIGHT_FIELD_COOPERATIVA',10);
define('PATH_IMG', 'public/img/');
define('PDF_LOGO_DEFAULT',PATH_IMG . 'logo.png');
