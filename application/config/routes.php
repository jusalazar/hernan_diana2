<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* DEFAULTS */
$route['default_controller'] = "web/Web_controller/index";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/* FIN DEFAULTS */


/* TOOLS */
$route['usuarios'] = "tools/usuario_controller/index";
$route['usuarios/(:num)'] = "tools/usuario_controller/index/$1";
$route['getusuarios'] = "tools/usuario_controller/getUsuarios";
$route['login'] = "tools/login_controller/index";
$route['vendedores/login'] = "tools/login_controller/index";
$route['asociados'] = "tools/login_controller/index";
$route['logout'] = "tools/login_controller/logout";
$route['validar_acceso'] = "tools/login_controller/validar";
$route['admin_guardar_usuario'] = "tools/usuario_controller/guardar";
/* FIN TOOLS */


/* REPORTES */
$route['reporte-pdf'] = "reporte/Reporte_controller/contratoLocacion";

/* FIN REPORTES */

/* ADMIN */
$route['admin-dashboard'] = "admin/Admin_controller/dashboard";
$route['getRenglonesDemorados'] = "admin/Admin_controller/getRenglonesDemorados";
$route['admin-panel'] = "admin/panel_controller/index";
$route['maker'] = 'admin/panel_controller/maker';
$route['nuevo_form'] = 'admin/panel_controller/maker';
$route['admin'] = "admin/Roles_controller/index";
$route['getroles'] = "admin/Roles_controller/show";
$route['roles'] = "admin/Roles_controller/index";
$route['permisos'] = "admin/Permisos_controller/index";
$route['getpermisos'] = "admin/Permisos_controller/show";
$route['admin-productos'] = "admin/productos_controller/index";
$route['admin-precios'] = "admin/producto_controller/precios";
$route["actualizarPreciosAjax"] = "admin/producto_controller/actualizar_precios";
/* FIN ADMIN */

/* compras admin */
$route['admin-compras'] = "admin/compra_controller/index";
$route['admin-compra/(:num)'] = 'admin/compra_controller/ver_compra/$1';
$route['admin-compras/listado'] = 'admin/compra_controller/index';
$route['admin-compras/reporte/(:num)'] = 'admin/compra_controller/reporte/$1';
$route['admin-compra/borrar/(:num)'] = 'admin/compra_controller/borrar/$1';
$route['getComprasAjax'] = 'admin/compra_controller/getCompraAjax';
$route['getProductosSelect2'] = 'admin/producto_controller/getProductosSelect2';


/* admin punto venta */
$route['admin-puntoventa'] = "admin/proveedor/proveedor_controller/index";
$route['admin-puntoventa/(:num)'] = "admin/proveedor/proveedor_controller/index/$1";

/* admin proveedor */
$route['admin-proveedor-mis-datos'] = "admin/proveedor/proveedor_controller/index";
$route['admin-proveedor-productos-que-vendemos'] = "admin/proveedor/proveedor_controller/productos_vendemos";
$route['admin-proveedor-mis-compras'] = "admin/proveedor/proveedor_controller/mis_compras";
$route['admin-proveedor-mis-liquidaciones'] = "admin/proveedor/proveedor_controller/mis_liquidaciones";
$route['admin-proveedor-mis-comisiones/(:num)'] = "admin/proveedor/proveedor_controller/mis_comisiones/$1";
$route['admin-proveedor-consultas-administrador'] = "admin/proveedor/proveedor_controller/mis_consultas";
$route['admin-proveedor-radio-entrega'] = "admin/proveedor/proveedor_controller/radio_entrega";
$route['punto_venta_zona'] = "admin/proveedor/proveedor_controller/radio_entrega";
$route['admin-proveedor-dashboard'] = "admin/proveedor/proveedor_controller/dashboard";
$route['admin-compra/proveedor-delegar/(:num)'] = "admin/compra_controller/proveedor_delegar/$1";
/* fin admin proveedor */

/* producto rutas */
$route["admin-producto"] = "admin/Producto_controller/index";
$route["admin-producto/(:num)"] = "admin/Producto_controller/index/$1";
$route["admin-producto/borrar/(:num)"] = "admin/Producto_controller/borrar/$1";
$route["getProductoAjax"] = "admin/Producto_controller/getProductoAjax";
$route["getProductoVariacionesAjax"] = "admin/Producto_controller/getProductoVariacionesAjax";


/* tipoatributo rutas */
$route["admin-tipoatributo"] = "admin/Tipoatributo_controller/index";
$route["admin-tipoatributo/(:num)"] = "admin/Tipoatributo_controller/index/$1";
$route["admin-tipoatributo/borrar/(:num)"] = "admin/Tipoatributo_controller/borrar/$1";
$route["getTipoatributoAjax"] = "admin/Tipoatributo_controller/getTipoatributoAjax";
/* atributo rutas */
$route["admin-atributo"] = "admin/Atributo_controller/index";
$route["admin-atributo/(:num)"] = "admin/Atributo_controller/index/$1";
$route["admin-atributo/borrar/(:num)"] = "admin/Atributo_controller/borrar/$1";
$route["getAtributoAjax"] = "admin/Atributo_controller/getAtributoAjax";


/* web */
$route["bienvenido"] = "web/Web_controller/index";
$route["contrato-alquiler"] = "web/Web_controller/contrato_alquiler";
$route["ajax_contrato_alquiler"]= "web/Web_controller/contrato_alquiler";


/* cliente rutas */
$route["admin-cliente"] = "admin/Cliente_controller/index";
$route["admin-cliente/(:num)"] = "admin/Cliente_controller/index/$1";
$route["admin-cliente/borrar/(:num)"] = "admin/Cliente_controller/borrar/$1";
$route["getClienteAjax"] = "admin/Cliente_controller/getClienteAjax";


/* licencia rutas */
$route["admin-licencia"] = "admin/Licencia_controller/index";
$route["admin-licencia/(:num)"] = "admin/Licencia_controller/index/$1";
$route["admin-licencia/borrar/(:num)"] = "admin/Licencia_controller/borrar/$1";
$route["getLicenciaAjax"] = "admin/Licencia_controller/getLicenciaAjax";
/* licencia rutas */
$route["admin-licencia"] = "admin/Licencia_controller/index";
$route["admin-licencia/(:num)"] = "admin/Licencia_controller/index/$1";
$route["admin-licencia/borrar/(:num)"] = "admin/Licencia_controller/borrar/$1";
$route["getLicenciaAjax"] = "admin/Licencia_controller/getLicenciaAjax";
/* tipopintura rutas */
$route["admin-tipopintura"] = "admin/Tipopintura_controller/index";
$route["admin-tipopintura/(:num)"] = "admin/Tipopintura_controller/index/$1";
$route["admin-tipopintura/borrar/(:num)"] = "admin/Tipopintura_controller/borrar/$1";
$route["getTipopinturaAjax"] = "admin/Tipopintura_controller/getTipopinturaAjax";
/* lugar rutas */
$route["admin-lugar"] = "admin/Lugar_controller/index";
$route["admin-lugar/(:num)"] = "admin/Lugar_controller/index/$1";
$route["admin-lugar/borrar/(:num)"] = "admin/Lugar_controller/borrar/$1";
$route["getLugarAjax"] = "admin/Lugar_controller/getLugarAjax";
/* pintura rutas */
$route["admin-pintura"] = "admin/Pintura_controller/index";
$route["admin-pintura/(:num)"] = "admin/Pintura_controller/index/$1";
$route["admin-pintura/borrar/(:num)"] = "admin/Pintura_controller/borrar/$1";
$route["getPinturaAjax"] = "admin/Pintura_controller/getPinturaAjax";
/* artista rutas */
$route["admin-artista"] = "admin/Artista_controller/index";
$route["admin-artista/(:num)"] = "admin/Artista_controller/index/$1";
$route["admin-artista/borrar/(:num)"] = "admin/Artista_controller/borrar/$1";
$route["getArtistaAjax"] = "admin/Artista_controller/getArtistaAjax";
/* empresa rutas */
$route["admin-empresa"] = "admin/Empresa_controller/index";
$route["admin-empresa/(:num)"] = "admin/Empresa_controller/index/$1";
$route["admin-empresa/borrar/(:num)"] = "admin/Empresa_controller/borrar/$1";
$route["getEmpresaAjax"] = "admin/Empresa_controller/getEmpresaAjax";
/* asistente rutas */
$route["admin-asistente"] = "admin/Asistente_controller/index";
$route["admin-asistente/(:num)"] = "admin/Asistente_controller/index/$1";
$route["admin-asistente/borrar/(:num)"] = "admin/Asistente_controller/borrar/$1";
$route["getAsistenteAjax"] = "admin/Asistente_controller/getAsistenteAjax";
/* evento rutas */
$route["admin-evento"] = "admin/Evento_controller/index";
$route["admin-evento/(:num)"] = "admin/Evento_controller/index/$1";
$route["admin-evento/borrar/(:num)"] = "admin/Evento_controller/borrar/$1";
$route["getEventoAjax"] = "admin/Evento_controller/getEventoAjax";

/* banco rutas */
$route["admin-banco"] = "admin/Banco_controller/index";
$route["admin-banco/(:num)"] = "admin/Banco_controller/index/$1";
$route["admin-banco/borrar/(:num)"] = "admin/Banco_controller/borrar/$1";
$route["getBancoAjax"] = "admin/Banco_controller/getBancoAjax";
/* descuento rutas */
$route["admin-tarjeta-regalo"] = "admin/Descuento_controller/tarjeta_regalo";
$route["admin-descuento"] = "admin/Descuento_controller/index";
$route["admin-descuento/(:num)"] = "admin/Descuento_controller/index/$1";
$route["admin-descuento/borrar/(:num)"] = "admin/Descuento_controller/borrar/$1";
$route["getDescuentoAjax"] = "admin/Descuento_controller/getDescuentoAjax";
/* contacto rutas */
$route["admin-contacto"] = "admin/Contacto_controller/index";
$route["admin-contacto/(:num)"] = "admin/Contacto_controller/index/$1";
$route["admin-contacto/borrar/(:num)"] = "admin/Contacto_controller/borrar/$1";
$route["getContactoAjax"] = "admin/Contacto_controller/getContactoAjax";
/* tipoatributo rutas */
$route["admin-tipoatributo"] = "admin/Tipoatributo_controller/index";
$route["admin-tipoatributo/(:num)"] = "admin/Tipoatributo_controller/index/$1";
$route["admin-tipoatributo/borrar/(:num)"] = "admin/Tipoatributo_controller/borrar/$1";
$route["getTipoatributoAjax"] = "admin/Tipoatributo_controller/getTipoatributoAjax";
/* contrato rutas */
$route["admin-contrato"] = "admin/Contrato_controller/index";
$route["admin-contrato/(:num)"] = "admin/Contrato_controller/index/$1";
$route["admin-contrato/borrar/(:num)"] = "admin/Contrato_controller/borrar/$1";
$route['admin-contrato/reporte/(:num)'] = "reporte/Reporte_controller/contratoLocacion/$1";
$route["getContratoAjax"] = "admin/Contrato_controller/getContratoAjax";
