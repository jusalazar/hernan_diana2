<?php

/**
 * Class Migrate
 * @property CI_Migration $migration
 */
class Welcome extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Auth");
    }

    public function index()
    {
        $user = Auth::user(11);
        echo $user->getEmail();
    }
}
