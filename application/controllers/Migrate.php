<?php

/**
 * Class Migrate
 * @property CI_Migration $migration
 */
class Migrate extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->input->is_cli_request()
        or exit("Forbidden");
        $this->load->library('migration');
    }
    /**
     * [index runs all migrations that were not executed]
     * @return string
     */
    public function index()
    {
        if (! $this->migration->latest()) {
            echo 'Error' . $this->migration->error_string();
        } else {
            echo 'Migrations ran successfully!'.PHP_EOL;
        }
    }
    /**
     * [reset resets the migrations to 0 and runs all]
     */
    public function reset()
    {
        $this->migration->version(0);
        echo 'Migrations reseted.' . PHP_EOL;
        $this->index();
    }
}
