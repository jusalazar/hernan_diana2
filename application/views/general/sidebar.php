<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <ul class="sidebar-menu">           
            <li class="treeview">
                 <a href="#">
                     <i class="fa fa-users"></i> <span>Config. Usuarios</span>
                     <span class="pull-right-container">
                         <i class="fa fa-angle-left pull-right"></i>
                     </span>
                 </a>
                 <ul class="treeview-menu">
                     <li><a href="<?= base_url("roles") ?>"><i class="fa fa-circle-o text-yellow"></i>Roles</a></li>
                     <li><a href="<?= base_url("permisos") ?>"><i class="fa fa-circle-o text-green"></i>Permisos</a></li>
                     <li><a href="<?= base_url("usuarios") ?>"><i class="fa fa-users"></i> <span>Usuarios</span></a></li>
                 </ul>
             </li>
    <!-- FIN DE USUARIOS -->

        </ul>
    </section>
</aside>