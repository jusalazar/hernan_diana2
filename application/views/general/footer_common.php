<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url("public/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/bootstrap-confirmation.min.js") ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("public/plugins/slimScroll/jquery.slimscroll.min.js") ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("public/plugins/fastclick/fastclick.js") ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("public/js/app.min.js") ?>"></script>
<link rel="stylesheet" href="<?= base_url("public/js/table/bootstrap-table.min.css") ?>">
<script src="<?php echo base_url("public/js/table/bootstrap-table.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/table/locale/bootstrap-table-es-AR.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/context/bootstrap-table-contextmenu.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/form/jquery.form.js") ?>"></script>
<link rel="stylesheet" href="<?php echo base_url("public/js/notify/animate.css") ?>">
<script src="<?php echo base_url("public/js/notify/bootstrap-notify.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/validate/jquery.validate.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/validate/additional-methods.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/validate/localization/messages_es_AR.min.js") ?>"></script>
<link rel="stylesheet" href="<?php echo base_url("public/js/sweetalert/sweetalert.css") ?>">
<link rel="stylesheet" href="<?php echo base_url("public/js/select2/css/select2.min.css") ?>">
<link rel="stylesheet" href="<?php echo base_url("public/js/select2/css/select2-bootstrap.min.css") ?>">
<script src="<?php echo base_url("public/js/sweetalert/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/sweetalert/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/select2/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("public/js/select2/js/i18n/es.js") ?>"></script>
<script src="<?php echo base_url("public/js/mask/jquery.mask.min.js") ?>"></script>


<script>
    function base_url() {
        return "<?=base_url()?>";
    }

    var csrf_token_name = "<?= $this->security->get_csrf_token_name(); ?>";
    var csrf_token_value = "<?= $this->security->get_csrf_hash() ?>";

    function notificar(texto, exito) {
        $.notify('<strong>' + texto + '</strong>', {
            type: exito,
            allow_dismiss: true,
            delay: 500
        });
    }
</script>
<?php
if (isset($scripts) && is_array($scripts) && count($scripts) > 0) {
    foreach ($scripts as $item) {
        $this->load->view($item);
    }
}
?>


