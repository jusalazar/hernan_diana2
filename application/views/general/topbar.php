<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?=base_url("admin-producto")?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini" style="font-size: 12px;"><b>MP</b>CO</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>MP</b>Industrias</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->


                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?=base_url("public/img/logo_baco.JPG")?>" class="user-image" alt="User Image">
                            <span class="hidden-xs">Industrias BACO</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?=base_url("public/img/logo_baco.JPG")?>" class="img-circle" alt="User Image">

                                <p>
                                    <?=$_SESSION['nombre']?> - <?=$_SESSION['rol_nombre']?>
                                    <small>Industrias Baco SAIC</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">

                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">

                                <div class="pull-right">
                                    <a href="<?=base_url("logout")?>" class="btn btn-danger btn-flat" title="Salir"><span class="glyphicon glyphicon-log-out"></span></a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>


