<?php

/**
 * Description of Sesion
 *
 * @author mato
 */
class Sesion {
    
    /**
     *
     * @var boolean $logged 
     */
    private $logged;
    /**
     *
     * @var Usuario $usuario 
     */
    private $usuario;

    function __construct() {
        if (!isset($_SESSION['logged'])) {
            $_SESSION['logged'] = FALSE;
            $this->logged = FALSE;
            $_SESSION['usuario'] = FALSE;
            $this->usuario = FALSE;
        }
    }

}
