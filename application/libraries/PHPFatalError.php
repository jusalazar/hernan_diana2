<?php

class PHPFatalError
{

    public function setHandler()
    {
        register_shutdown_function(array(
            $this,
            'handleShutdown'
        ));
    }
    
    public function handleShutdown() {
        $error = error_get_last();
        if($error) {
            $error = "FATAL:\n". print_r($error,true);
            echo $error;
            log_message('ERROR', $error);
        }
    }
}