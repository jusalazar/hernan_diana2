<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    /**
    * [20170620224343_create_table_categorias]
    * @property CI_DB_forge $dbforge
    */
class Migration_create_table_categorias extends CI_Migration {

    /**
    * [$table name of the table]
    * @var string
    */
    private $table ="categorias";

    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'auto_increment' => true
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => 96,
                'unique' => true
            ),
            'modified_at' => array(
                'type' => 'TIMESTAMP',
                'null' => 'CURRENT_TIMESTAMP'
            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table);
    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170620224343_create_table_categorias.php */
/* Location: ./application/migration/20170620224343_create_table_categorias.php */
