<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * [20170719210905_alter_db]
 * 
 * @property CI_DB_forge $dbforge
 */
class Migration_alter_db extends CI_Migration {

    /**
     * [$table name of the table]
     * 
     * @var string
     */
    private $table = "domicilios";

    /**
     * [up makes databases changes]
     * 
     * @return mixed
     */
    public function up() {
        $this->db->query("DROP TABLE atributos");
        $this->db->query("DROP TABLE marcas");
        $this->db->query("DROP TABLE categorias");
    }

    /**
     * [down rollbacks databases changes]
     * 
     * @return mixed
     */
    public function down() {
        //$this->dbforge->drop_table($this->table);
    }

}
