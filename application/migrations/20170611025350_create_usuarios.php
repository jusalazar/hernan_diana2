<?php
use Tools\Rol;
use Tools\Perfil;
use Tools\Permiso;
use Tools\Usuario;
use Carbon\Carbon;


defined('BASEPATH') OR exit('No direct script access allowed');
/**
* [20170611013847_create_usuarios]
* @property CI_DB_forge $dbforge
*/
class Migration_create_usuarios extends CI_Migration {


    private $table_usuarios = "usuarios";
    private $table_roles = "roles";
    private $table_permisos = "permisos";
    private $table_usuarios_roles = "usuarios_roles";
    private $table_roles_permisos = "roles_permisos";
    private $table_perfiles = "perfiles";
    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up()
    {
        $this->load->library("Doctrine","doctrine");
        $this->orm = $this->doctrine->em;
        // Creacion del usuario admin
        $admin = new Usuario;
        $admin->setEmail("admin@gmail.com")->setPassword(password_hash("@rgitRules2017!", PASSWORD_DEFAULT));
        $admin->setLast_login(Carbon::now());
        $this->orm->persist($admin);
        // Agregamos perfil
        $perfil = new Perfil;
        $perfil->setNombre("Admin")->setApellido("Admin")->setDni(1)->setUsuario($admin);
        $perfil->setFecha_nacimiento(Carbon::now());
        $this->orm->persist($perfil);
        // Creamos rol admin
        $rol = new Rol;
        $rol->setNombre("admin")->setPanel("admin")->setPeso(1000);
        $this->orm->persist($rol);
        // Creamos los permisos
        $permiso = new Permiso;
        $permiso->setNombre("GOD")->setDescripcion("Habilita el modo GOD");
        $permiso = new Permiso;
        $permiso->setNombre("administrar roles")->setDescripcion("administrar roles");
        $permiso = new Permiso;
        $permiso->setNombre("borrar roles")->setDescripcion("borrar roles");
        $permiso = new Permiso;
        $permiso->setNombre("administrar permisos")->setDescripcion("administrar permisos");
        $permiso = new Permiso;
        $permiso->setNombre("borrar permisos")->setDescripcion("borrar permisos");
        $this->orm->persist($permiso);
        $rol->getPermisos()->add($permiso);
        $admin->getRoles()->add($rol);
        $this->orm->flush();
    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down()
    {
        $this->db->query("delete from " . $this->table_roles_permisos);
        $this->db->query("delete from " . $this->table_usuarios_roles);
        $this->db->query("delete from " . $this->table_perfiles);
        $this->db->query("delete from " . $this->table_permisos);
        $this->db->query("delete from " . $this->table_roles);
        $this->db->query("delete from " . $this->table_usuarios);
    }
}
/* End of file 20170611013847_create_usuarios.php */
/* Location: ./application/migration/20170611013847_create_usuarios.php */
