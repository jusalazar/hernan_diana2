<?php
use Tools\Provincia;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * [20170620211646_add_provincias]
 * 
 * @property CI_DB_forge $dbforge
 */
class Migration_add_provincias extends CI_Migration
{

    /**
     * [$table name of the table]
     * 
     * @var string
     */
    private $table = "provincias";

    /**
     * [up makes databases changes]
     * 
     * @return mixed
     */
    public function up()
    {
        $this->load->library("Doctrine","doctrine");
        $this->orm = $this->doctrine->em;
        $this->factoryProvincia("Ciudad Autónoma de Buenos Aires", 1000);
        $this->factoryProvincia("Buenos Aires", 900);
        $this->factoryProvincia("Corrientes");
        $this->factoryProvincia("Entre Ríos");
        $this->factoryProvincia("Misiones");
        $this->factoryProvincia("Chaco");
        $this->factoryProvincia("Formosa");
        $this->factoryProvincia("Santa Fe");
        $this->factoryProvincia("Córdoba");
        $this->factoryProvincia("Tucumán");
        $this->factoryProvincia("San Luis");
        $this->factoryProvincia("San Juan");
        $this->factoryProvincia("Mendoza");
        $this->factoryProvincia("Catamarca");
        $this->factoryProvincia("Salta");
        $this->factoryProvincia("Santiago del Estero");
        $this->factoryProvincia("Santa Cruz");
        $this->factoryProvincia("Rio Negro");
        $this->factoryProvincia("Chubut");
        $this->factoryProvincia("Tierra del Fuego");
        $this->factoryProvincia("La Pampa");
        $this->factoryProvincia("Neuquén");
        $this->factoryProvincia("La Rioja");
        $this->orm->flush();
        
    }

    /**
     * [down rollbacks databases changes]
     * 
     * @return mixed
     */
    public function down()
    {
        $this->db->query("DELETE FROM ".$this->table);
    }
    
    private function factoryProvincia($nombre,$peso = 0) {
        $provincia = new Provincia();
        $provincia->setNombre($nombre)->setPeso($peso);
        $this->orm->persist($provincia);
    }
}
/* End of file 20170620211646_add_provincias.php */
/* Location: ./application/migration/20170620211646_add_provincias.php */
