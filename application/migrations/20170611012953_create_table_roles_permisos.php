<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* [20170611012953_create_table_roles_permisos]
* @property CI_DB_forge $dbforge
*/
class Migration_create_table_roles_permisos extends CI_Migration {

    /**
    * [$table name of the table]
    * @var string
    */
    private $table ="roles_permisos";

    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id'          => array(
                    'type'           => 'INT',
                    'auto_increment' => true
                ),
                'rol_id' => array(
                    'type' => 'INT',
                ),
                'permiso_id'    => array(
                    'type' => 'INT'
                ),
                'modified_at' => array(
                    'type' => 'TIMESTAMP',
                    'null' => 'CURRENT_TIMESTAMP'
                )
            )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(rol_id) REFERENCES roles(id)');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(permiso_id) REFERENCES permisos(id)');
        $this->dbforge->create_table($this->table);
    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170611012953_create_table_roles_permisos.php */
/* Location: ./application/migration/20170611012953_create_table_roles_permisos.php */
