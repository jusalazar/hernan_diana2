<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* [20170611025327_create_table_perfiles]
* @property CI_DB_forge $dbforge
*/
class Migration_create_table_perfiles extends CI_Migration {

    /**
    * [$table name of the table]
    * @var string
    */
    private $table ="perfiles";

    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id'          => array(
                    'type'           => 'INT',
                    'auto_increment' => true
                ),
                'nombre'       => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 128,
                    'null'     => true,
                ),
                'apellido'       => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 128,
                    'null'     => true,
                ),
                'dni'       => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 20,
                    'null'     => true,
                ),
                'usuario_id'          => array(
                    'type'           => 'INT',
                    'null' => true
                ),
                'fecha_nacimiento' => array(
                    'type' => 'DATETIME',
                    'null' => true
                ),
                'modified_at' => array(
                    'type' => 'TIMESTAMP',
                    'null' => 'CURRENT_TIMESTAMP'
                ),
                'created_at'  => array(
                    'type' => 'TIMESTAMP',
                    'null' => true
                )
            )
        );

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table);

    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170611025327_create_table_perfiles.php */
/* Location: ./application/migration/20170611025327_create_table_perfiles.php */
