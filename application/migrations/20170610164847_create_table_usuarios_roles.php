<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_table_usuarios_roles extends CI_Migration {

    /**
    * [$table name of the table]
    * @var string
    */
    private $table ="usuarios_roles";

    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up() {
        $this->dbforge->add_field(
            array(
                'id'          => array(
                    'type'           => 'INT',
                    'auto_increment' => true
                ),
                'usuario_id' => array(
                    'type' => 'INT',
                ),
                'rol_id'    => array(
                    'type' => 'INT'
                ),
                'modified_at' => array(
                    'type' => 'TIMESTAMP',
                    'null' => 'CURRENT_TIMESTAMP'
                )
            )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(usuario_id) REFERENCES usuarios(id)');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(rol_id) REFERENCES roles(id)');
        $this->dbforge->create_table($this->table);
    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down() {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170610164847_create_table_usuarios_roles.php */
/* Location: ./application/migration/20170610164847_create_table_usuarios_roles.php */
