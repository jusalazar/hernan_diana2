<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Tabla usuarios
 * @property CI_DB_forge $dbforge dtabase manipulation
 */
class Migration_create_table_usuarios extends CI_Migration
{

    private $table ="usuarios";

    public function up()
    {
        $this->dbforge->add_field(
           array(
               'id'          => array(
                   'type'           => 'INT',
                   'auto_increment' => true
               ),
               'email'       => array(
                   'type'       => 'VARCHAR',
                   'constraint' => 96,
                   'unique'     => true,
               ),
               'password'    => array(
                   'type'       => 'VARCHAR',
                   'constraint' => 60
               ),               
               'modified_at' => array(
                   'type' => 'TIMESTAMP',
                   'null' => 'CURRENT_TIMESTAMP'
               ),
               'created_at'  => array(
                   'type' => 'TIMESTAMP',
                   'null' => true
               ),
               'last_login'  => array(
                   'type' => 'TIMESTAMP',
                   'null' => true

               )
           )
        );

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170610002553_create_table_usuarios.php */
/* Location: ./application/migration/20170610002553_create_table_usuarios.php */
