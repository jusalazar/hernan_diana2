<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    /**
    * [20170620211357_create_table_provincias]
    * @property CI_DB_forge $dbforge
    */
class Migration_create_table_provincias extends CI_Migration {

    /**
    * [$table name of the table]
    * @var string
    */
    private $table ="provincias";

    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'auto_increment' => true
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => 96,
            ),
            'peso' => array(
                'type' => 'INT',
                'default' => 0
            ),
            'modified_at' => array(
                'type' => 'TIMESTAMP',
                'null' => 'CURRENT_TIMESTAMP'
            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table);
    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170620211357_create_table_provincias.php */
/* Location: ./application/migration/20170620211357_create_table_provincias.php */
