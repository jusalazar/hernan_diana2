<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
* [Migration_create_table_roles description]
*
*/
class Migration_create_table_roles extends CI_Migration
{

    private $table ="roles";

    /**
    * [up description]
    * @return [type] [description]
    */
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id'          => array(
                    'type'           => 'INT',
                    'auto_increment' => true
                ),
                'nombre'       => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 64,
                    'unique'     => true,
                ),
                'peso'       => array(
                    'type'       => 'INT',
                    'default' => 0
                ),
                'panel'       => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 128,
                ),
                'modified_at' => array(
                    'type' => 'TIMESTAMP',
                    'null' => 'CURRENT_TIMESTAMP'
                ),
                'created_at'  => array(
                    'type' => 'TIMESTAMP',
                    'null' => true
                )
            )
        );

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table);
    }

    /**
    * [down description]
    * @return mixed [description]
    */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170610163427_create_table_roles.php */
/* Location: ./application/migration/20170610163427_create_table_roles.php */
