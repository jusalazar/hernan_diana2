<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * [20170615181923_create_table_captcha]
 * 
 * @property CI_DB_forge $dbforge
 * @property CI_DB_query_builder $db
 */
class Migration_create_table_captcha extends CI_Migration
{

    /**
     * [$table name of the table]
     * 
     * @var string
     */
    private $table = "captcha";

    /**
     * [up makes databases changes]
     * 
     * @return mixed
     */
    public function up()
    {
        $sql = "CREATE TABLE `captcha` (
            `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
            `captcha_time` int(10) unsigned NOT NULL,
            `ip_address` varchar(45) NOT NULL,
            `word` varchar(20) NOT NULL,
            PRIMARY KEY (`captcha_id`),
            KEY `word` (`word`)
            ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8";
        
        $this->db->query($sql);
    }

    /**
     * [down rollbacks databases changes]
     * 
     * @return mixed
     */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170615181923_create_table_captcha.php */
/* Location: ./application/migration/20170615181923_create_table_captcha.php */
