<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* [20170611045510_create_argit_sessions]
* @property CI_DB_forge $dbforge
*/
class Migration_create_argit_sessions extends CI_Migration {

    /**
    * [$table name of the table]
    * @var string
    */
    private $table ="argit_sessions";

    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `argit_sessions` (
            `id` varchar(40) NOT NULL,
            `ip_address` varchar(45) NOT NULL,
            `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
            `data` blob NOT NULL,
            PRIMARY KEY (`id`,`ip_address`),
            KEY `argit_sessions` (`timestamp`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170611045510_create_argit_sessions.php */
/* Location: ./application/migration/20170611045510_create_argit_sessions.php */
