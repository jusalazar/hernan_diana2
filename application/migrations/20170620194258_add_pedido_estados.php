<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    /**
    * [20170620194258_add_pedido_estados]
    * @property CI_DB_forge $dbforge
    */
class Migration_add_pedido_estados extends CI_Migration {

    /**
    * [$table name of the table]
    * @var string
    */
    private $table ="pedido_estados";

    /**
    * [up makes databases changes]
    * @return mixed
    */
    public function up()
    {
        $this->load->library("Doctrine","doctrine");
        $this->orm = $this->doctrine->em;
        
    }

    /**
    * [down rollbacks databases changes]
    * @return mixed
    */
    public function down()
    {
        $this->db->query("DELETE FROM ".$this->table);
    }
}
/* End of file 20170620194258_add_pedido_estados.php */
/* Location: ./application/migration/20170620194258_add_pedido_estados.php */
