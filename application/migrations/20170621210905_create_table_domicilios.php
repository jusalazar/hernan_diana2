<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * [20170620210905_create_table_domicilio]
 * 
 * @property CI_DB_forge $dbforge
 */
class Migration_create_table_domicilios extends CI_Migration
{

    /**
     * [$table name of the table]
     * 
     * @var string
     */
    private $table = "domicilios";

    /**
     * [up makes databases changes]
     * 
     * @return mixed
     */
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'auto_increment' => true
            ),
            'calle' => array(
                'type' => 'VARCHAR',
                'constraint' => 96,
            ),
            'nro' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
            ),
            'piso' => array(
                'type' => 'VARCHAR',
                'constraint' => 5,
            ),
            'depto' => array(
                'type' => 'VARCHAR',
                'constraint' => 5,
            ),
            'cod_postal' => array(
                'type' => 'VARCHAR',
                'constraint' => 5,
            ),
            'localidad' => array(
                'type' => 'VARCHAR',
                'constraint' => 96,
            ),
            'provincia_id' => array(
                'type' => 'INT',
            ),
            'telefono' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
            ),
            'latitud' => array(
                'type' => 'VARCHAR',
                'constraint' => 25,
            ),
            'longitud' => array(
                'type' => 'VARCHAR',
                'constraint' => 25,
            ),
            'modified_at' => array(
                'type' => 'TIMESTAMP',
                'null' => 'CURRENT_TIMESTAMP'
            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(provincia_id) REFERENCES provincias(id)');
        $this->dbforge->create_table($this->table);
        
    }

    /**
     * [down rollbacks databases changes]
     * 
     * @return mixed
     */
    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
/* End of file 20170620210905_create_table_domicilio.php */
/* Location: ./application/migration/20170620210905_create_table_domicilio.php */
