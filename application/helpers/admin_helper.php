<?php

use \Doctrine\Common\Util\Debug;
use Symfony\Component\Translation\Dumper\DumperInterface;

if (!function_exists('imprimirFecha')) {

    function imprimirFecha($fecha) {
        if ($fecha == null) {
            return "";
        } else {
            return $fecha->format("d/m/Y");
        }
    }

}

function muestraImporte($importe) {
    return number_format($importe, 2, ",", ".");
}

function muestraDni($importe) {
    return number_format($importe, 0, ",", ".");
}

function muestraProducto() {
    /* $controller=new AW_Controller();
      $template = $this->twig->load('maker_default.twig');
      $data['nombre_formulario'] = "edad/edad_formulario.twig";
      $data['title'] = "edads";
      echo $template->render($data); */
}

function crearPassword() {
    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $cad = "";
    for ($i = 0; $i < 12; $i++) {
        $cad .= substr($str, rand(0, 62), 1);
    }
    return $cad;
}

function getdistance($lat1, $lon1, $lat2, $lon2, $unit = "M") {
    if (!$lat1 || !$lon1 || !$lat2 || !$lon2) {
        return "sin datos ";
    }
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "M") {
        $kms = $miles * 1.609344;
        if ($kms < 1) {
            return round($kms, 2) * 1000;
        } else {
            return round($kms, 1) * 1000;
        }
    } else if ($unit == "K") {
        $kms = $miles * 1.609344;
        if ($kms < 1) {
            return round($kms, 2);
        } else {
            return round($kms, 1);
        }
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}

if (!function_exists('get_user_id')) {

    function get_user_id() {
        return array_key_exists("usu_id", $_SESSION) ? $_SESSION ['usu_id'] : 0;
    }

}

if (!function_exists('mayusculas')) {

    function mayusculas($texto) {
        return mb_strtoupper($texto, "utf8");
    }

}

if (!function_exists('fecha_post')) {

    function fecha_post($fecha) {
        if (DateTime::createFromFormat("d/m/Y", $fecha) === false) {
            return null;
        } else {
            return DateTime::createFromFormat("d/m/Y", $fecha);
        }
    }

}

if (!function_exists('get_rol_id')) {

    function get_rol_id() {
        return $_SESSION ['rol_id'];
    }

}

if (!function_exists('get_home')) {

    function get_home() {
        return $_SESSION ['rol_panel'];
    }

}

if (!function_exists('dd')) {

    function dd($data) {
        echo '<pre>';
        if (is_object($data)) {
            Debug::dump($data);
        } else {
            print_r($data);
        }
        echo '</pre>';
        die();
    }

}

if (!function_exists('print_object')) {

    function print_object($objeto) {
        echo '<pre>';
        Debug::dump($objeto);
        echo '</pre>';
        die();
    }

}

if (!function_exists('json_out')) {

    function json_out($data) {
        echo json_encode($data);
    }

}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

function converArraySerializeToArrayData($information) {
    $out = array();
    foreach ($information as $parametro) {
        if ($parametro["value"] || $parametro["value"] == 0) {
            if (strpos($parametro["name"], "[]") !== false) {
                $out[str_replace("[]", "", $parametro['name'])][] = $parametro['value'];
            } else {
                $out[$parametro["name"]] = $parametro["value"];
            }
        }
    }

    return $out;
}

function imprimirYmorir($array, $print_r = false) {
    echo '<pre>';
    if ($print_r) {
        print_r($array);
    } else {
        var_dump($array);
    }
    die;
}

function imprimirArray($array) {
    echo '<pre>';
    var_dump($array);
}

function guardarFoto($foto, $destino, $nombre, $crop = false, $width = 270, $height = 270, $proporcional = false) {
    $extension = ($foto['type'] == "image/png") ? "png" : "jpg";
    $destino = $destino . $nombre . "." . $extension;
    $origen = $foto['tmp_name'];
    if (file_exists($destino)) {
        unlink($destino);
    }
    move_uploaded_file($origen, $destino);
    list($img_width, $img_height) = getimagesize($destino);
    if ($crop || ($img_width > $width && $img_height > $height)) {
        smart_resize_image($destino, null, $width, $height, $proporcional, $destino, false, false, 100);
    }

    return $nombre . "." . $extension;
}

/**
 * easy image resize function
 * @param  $file - file name to resize
 * @param  $string - The image data, as a string
 * @param  $width - new image width
 * @param  $height - new image height
 * @param  $proportional - keep image proportional, default is no
 * @param  $output - name of the new file (include path if needed)
 * @param  $delete_original - if true the original image will be deleted
 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
 * @param  $quality - enter 1-100 (100 is best quality) default is 100
 * @return boolean|resource
 */
function smart_resize_image($file, $string = null, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false, $quality = 100
) {

    if ($height <= 0 && $width <= 0)
        return false;
    if ($file === null && $string === null)
        return false;

# Setting defaults and meta
    $info = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
    $image = '';
    $final_width = 0;
    $final_height = 0;
    list($width_old, $height_old) = $info;
    $cropHeight = $cropWidth = 0;

# Calculating proportionality
    if ($proportional) {
        if ($width == 0)
            $factor = $height / $height_old;
        elseif ($height == 0)
            $factor = $width / $width_old;
        else
            $factor = min($width / $width_old, $height / $height_old);

        $final_width = round($width_old * $factor);
        $final_height = round($height_old * $factor);
    }
    else {
        $final_width = ( $width <= 0 ) ? $width_old : $width;
        $final_height = ( $height <= 0 ) ? $height_old : $height;
        $widthX = $width_old / $width;
        $heightX = $height_old / $height;

        $x = min($widthX, $heightX);
        $cropWidth = ($width_old - $width * $x) / 2;
        $cropHeight = ($height_old - $height * $x) / 2;
    }

# Loading image to memory according to type
    switch ($info[2]) {
        case IMAGETYPE_JPEG: $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);
            break;
        case IMAGETYPE_GIF: $file !== null ? $image = imagecreatefromgif($file) : $image = imagecreatefromstring($string);
            break;
        case IMAGETYPE_PNG: $file !== null ? $image = imagecreatefrompng($file) : $image = imagecreatefromstring($string);
            break;
        default: return false;
    }


# This is the resizing/resampling/transparency-preserving magic
    $image_resized = imagecreatetruecolor($final_width, $final_height);
    if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
        $transparency = imagecolortransparent($image);
        $palletsize = imagecolorstotal($image);

        if ($transparency >= 0 && $transparency < $palletsize) {
            $transparent_color = imagecolorsforindex($image, $transparency);
            $transparency = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
            imagefill($image_resized, 0, 0, $transparency);
            imagecolortransparent($image_resized, $transparency);
        } elseif ($info[2] == IMAGETYPE_PNG) {
            imagealphablending($image_resized, false);
            $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
            imagefill($image_resized, 0, 0, $color);
            imagesavealpha($image_resized, true);
        }
    }
    imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);


# Taking care of original, if needed
    if ($delete_original) {
        if ($use_linux_commands)
            exec('rm ' . $file);
        else
            @unlink($file);
    }

# Preparing a method of providing result
    switch (strtolower($output)) {
        case 'browser':
            $mime = image_type_to_mime_type($info[2]);
            header("Content-type: $mime");
            $output = NULL;
            break;
        case 'file':
            $output = $file;
            break;
        case 'return':
            return $image_resized;
        default:
            break;
    }

# Writing image according to type to the output destination and image quality
    switch ($info[2]) {
        case IMAGETYPE_GIF: imagegif($image_resized, $output);
            break;
        case IMAGETYPE_JPEG: imagejpeg($image_resized, $output, $quality);
            break;
        case IMAGETYPE_PNG:
            $quality = 9 - (int) ((0.9 * $quality) / 10.0);
            imagepng($image_resized, $output, $quality);
            break;
        default: return false;
    }

    return true;
}
