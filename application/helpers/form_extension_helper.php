<?php

/**
 * Created by PhpStorm.
 * User: mato
 * Date: 12/10/2016
 * Time: 12:46
 */
if (!function_exists('form_date')) {

    /**
     * @param $name
     * @param string $value
     * @param string $clase
     * @param string $id
     * @param string $atributos
     * @return string
     */
    function form_date($name, $value = "", $clase = "", $id = "", $atributos = "")
    {
        $attr = $atributos;
        if (is_array($atributos)) {
            foreach ($atributos as $key => $value) {
                $attr .= $key . '="' . $value . '"';
            }
        } 
        $html = '<input type="text" placeholder="__/__/____" data-mask="00/00/0000" name="' . $name;
        $html .= '" dateITA="true" data-msg="Escriba una fecha válida dd/mm/YYYY" class="form-control ' . $clase . '" value="' . $value . '" id="' . $id . '" ';
        $html .= $attr. ' data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="es"/>';

        return $html;
    }

}