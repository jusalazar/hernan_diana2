<?php

use Doctrine\ORM\EntityManager;
use Tools\Usuario;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * class Auth
 * Clase que maneja en forma est�tica el usuario y sus roles
 *
 * @author raulm
 *
 */
class Auth {

    /**
     *
     * @var EntityManager
     */
    private static $orm;

    /**
     *
     * @var Usuario $usuario
     */
    private static $user;
    private static $esta_logeado = false;

    /**
     * [_construct description]
     *
     * @return [type] [description]
     */
    public function __construct() {
        $CI = &get_instance();
        $CI->load->library("Doctrine", "doctrine");
        self::$orm = $CI->doctrine->em;
        if (isset($_SESSION["rol_id"])) {
            self::$user = self::user($_SESSION['rol_id']);
        }
        if (isset($_SESSION["logged"]) && $_SESSION["logged"]) {
            self::$esta_logeado = true;
        }
    }

    public static function estaLogeado() {
        return self::$esta_logeado;
    }

    /**
     * [user description]
     *
     * @param int $id
     *            [description]
     * @return Tools\Usuario $user
     */
    public static function user($id) {
        if (is_numeric($id) && 0 < $id) {
            $user = self::$orm->find("Tools\Usuario", $id);
            if (null != $user && is_numeric($user->getId())) {
                return $user;
            }
        }
        return (new Usuario ());
    }

    /**
     * Esto se debería usar solo para setear el usuario que se loguea
     *
     * @param Usuario $usuario
     */
    public static function setUser(Usuario $usuario) {
        self::$user = $usuario;
    }

    /**
     * devuelve true or false dado un permiso
     * @param $nombrePermiso
     * @return bool
     */
    public static function check($nombrePermiso) {
        if (self::$user->getRol()->getNombre() == "admin") {
            return true;
        }
        if (null != self::$user && self::$user->getRoles()->count() > 0 && is_numeric(self::$user->getId())) {
            $estaHabilitado = false;
            foreach (self::$user->getRoles() as $rol) {
                foreach ($rol->getPermisos() as $permiso) {
                    if ($permiso->getNombre() == $nombrePermiso) {
                        $estaHabilitado = true;
                    }
                }
            }
            return $estaHabilitado;
        }
        return false;
    }

    /**
     * si el usario no tiene permiso redirige al panel
     * @param $nombrePermiso
     */
    public static function can($nombrePermiso) {
        if (!self::check($nombrePermiso)) {
            $url = base_url(self::$user->getRol()->getPanel());
            redirect($url);
        }
    }

    /*
     * true si es admin, sino false
     * @return bool
     */

    public static function esAdmin() {
        if (self::$user->getRol()->getNombre() == "admin") {
            return true;
        }
        return false;
    }

    public static function esRol($rol_nombre) {
        if (self::$user->getRol()->getNombre() == $rol_nombre || self::esAdmin()) {
            return true;
        }
        return false;
    }

}

/* End of file Auth.php */
/* Location: ./application/models/Auth.php */
