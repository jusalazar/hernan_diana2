<?php

/**
 * Description of General_model
 * @author mato
 *
 *
 */
class General_model extends A_model
{

    function __construct()
    {
        parent::__construct();
    }

    function getCantidadOF() {
        $consulta = "select count(*) as cantidad from orden_fabricacion";
        return $this->db->query($consulta)->result_array()[0]['cantidad'];
    }

    function getCantidadClientes() {
        $consulta = "select count(*) as cantidad from cliente";
        return $this->db->query($consulta)->result_array()[0]['cantidad'];
    }

}