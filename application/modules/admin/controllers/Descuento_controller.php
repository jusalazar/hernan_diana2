<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Descuento_model $descuento
 * @property Compra_model $compra
 */
class Descuento_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("descuento/descuento_model", "descuento");
        $this->load->model("compra/compra_model", "compra");
    }

    public function tarjeta_regalo() {
        if ($this->post()) {
            $this->descuento->insertarTarjetaRegalo($this->post());
            redirect("admin-tarjeta-regalo");
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data["tarjeta_regalo"] = $this->compra->getTarjetaRegalo();
            $data['nombrePagina'] = "Gestión de descuentos";
            $data['nombre_formulario'] = "descuento/tarjeta_regalo.twig";
            $data['title'] = "descuentos";
            echo $template->render($data);
        }
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->descuento->insertar($this->post());
            redirect("admin-descuento");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["descuento"] = $this->descuento->getById($args);
            $data['nombrePagina'] = "Gestión de descuentos";
            $data['nombre_formulario'] = "descuento/descuento_formulario.twig";
            $data['title'] = "descuentos";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de descuentos";
            $data['nombre_formulario'] = "descuento/descuento_formulario.twig";
            $data['title'] = "descuentos";
            echo $template->render($data);
        }
    }

    public function getDescuentoAjax() {
        echo json_encode($this->descuento->getDescuentoAjax($this->input->get()));
    }

    public function borrar($des_id) {
        $this->descuento->borrar($des_id);
        redirect("admin-descuento");
    }

}
