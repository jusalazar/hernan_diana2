<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Artista_model $artista
 * @property Licencia_model $licencia
 * @property Banco_model $banco
 */
class Artista_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("artista/artista_model", "artista");
        $this->load->model("licencia/licencia_model", "licencia");
        $this->load->model("banco/banco_model", "banco");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->artista->insertar($this->post());
            redirect("admin-artista");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["artista"] = $this->artista->getById($args);
            $data['nombrePagina'] = "Gestión de artistas";
            $data['nombre_formulario'] = "artista/artista_formulario.twig";
            $data["licencias"] = $this->licencia->getLicenciasActivas();
            $data["bancos"] = $this->banco->getBancosActivos();
            $data['title'] = "artistas";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de artistas";
            $data['nombre_formulario'] = "artista/artista_formulario.twig";
            $data["licencias"] = $this->licencia->getLicenciasActivas();
            $data["bancos"] = $this->banco->getBancosActivos();
            $data['title'] = "artistas";
            echo $template->render($data);
        }
    }

    public function getArtistaAjax() {
        echo json_encode($this->artista->getArtistaAjax($this->input->get()));
    }

    public function borrar($art_id) {
        $this->artista->borrar($art_id);
        redirect("admin-artista");
    }

}
