<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Licencia_model $licencia
 */
class Licencia_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("licencia/licencia_model", "licencia");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->licencia->insertar($this->post());
            redirect("admin-licencia");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["licencia"] = $this->licencia->getById($args);
            $data['nombrePagina'] = "Gestión de licencias";
            $data['nombre_formulario'] = "licencia/licencia_formulario.twig";
            $data['title'] = "licencias";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de licencias";
            $data['nombre_formulario'] = "licencia/licencia_formulario.twig";
            $data['title'] = "licencias";
            echo $template->render($data);
        }
    }

    public function getLicenciaAjax() {
        echo json_encode($this->licencia->getLicenciaAjax($this->input->get()));
    }

    public function borrar($lic_id) {
        $this->licencia->borrar($lic_id);
        redirect("admin-licencia");
    }

}
