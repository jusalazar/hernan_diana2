<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Atributo_model $atributo
 */
class Atributo_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("atributo/atributo_model", "atributo");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->atributo->insertar($this->post());
            redirect("admin-atributo");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["atributo"] = $this->atributo->getById($args);
            $data['nombrePagina'] = "Gestión de atributos";
            $data['nombre_formulario'] = "atributo/atributo_formulario.twig";
            $data['tipos'] = $this->atributo->getAllCollection("Admin\Tipoatributo");
            $data['title'] = "atributos";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de atributos";
            $data['nombre_formulario'] = "atributo/atributo_formulario.twig";
            $data['tipos'] = $this->atributo->getAllCollection("Admin\Tipoatributo");
            $data['title'] = "atributos";
            echo $template->render($data);
        }
    }

    public function getAtributoAjax() {
        echo json_encode($this->atributo->getAtributoAjax($this->input->get()));
    }

    public function borrar($atr_id) {
        $this->atributo->borrar($atr_id);
        redirect("admin-atributo");
    }

}
