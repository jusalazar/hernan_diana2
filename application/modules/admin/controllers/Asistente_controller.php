<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Asistente_model $asistente
 * @property Licencia_model $licencia
 * @property Empresa_model $empresa
 */
class Asistente_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("asistente/asistente_model", "asistente");
        $this->load->model("empresa/empresa_model", "empresa");
        $this->load->model("licencia/licencia_model", "licencia");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->asistente->insertar($this->post());
            redirect("admin-asistente");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["asistente"] = $this->asistente->getById($args);
            $data['nombrePagina'] = "Gestión de asistentes";
            $data['nombre_formulario'] = "asistente/asistente_formulario.twig";
            $data["empresas"] = $this->empresa->getEmpresasActivas();
            $data["licencias"] = $this->licencia->getLicenciasActivas();
            $data['title'] = "asistentes";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de asistentes";
            $data['nombre_formulario'] = "asistente/asistente_formulario.twig";
            $data["empresas"] = $this->empresa->getEmpresasActivas();
            $data["licencias"] = $this->licencia->getLicenciasActivas();
            $data['title'] = "asistentes";
            echo $template->render($data);
        }
    }

    public function getAsistenteAjax() {
        echo json_encode($this->asistente->getAsistenteAjax($this->input->get()));
    }

    public function borrar($asi_id) {
        $this->asistente->borrar($asi_id);
        redirect("admin-asistente");
    }

}
