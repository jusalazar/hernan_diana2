<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Feriado_model $feriado
 */
class Calendario_controller extends A_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("admin/feriado_model","feriado");
    }

    function index()
    {
        $template = $this->twig->load('calendario/calendario.twig');
        $feriados = $this->feriado->getAll();
        $data['nombrePagina'] = "Gestión de feriados";
        $data['title'] = "Calendario";
        echo $template->render($data);
    }
}