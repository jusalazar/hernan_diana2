<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Contrato_model $contrato
 */
class Contrato_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("contrato/contrato_model", "contrato");
        $this->load->model("admin/atributo/atributo_model", "atributo");
        $this->load->library('NumberToLetterConverter');
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->contrato->insertarContratoAlquiler($this->post());
            redirect("admin-contrato");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["contrato"] = $this->contrato->getById($args);
            $data['nombrePagina'] = "Gestión de contratos";
            $data['nombre_formulario'] = "contrato/contrato_formulario.twig";
            $data["estado_propiedades"] = $this->atributo->getEstadosPropiedades();
            $data["tipo_propiedades"] = $this->atributo->getTipoPropiedades();
            $data["tipo_garantias"] = $this->atributo->getTipoGarantias();
            $data["atributos_propiedades"] = $this->atributo->getAtributosPropiedades();
            $data['title'] = "contratos";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de contratos";
            $data['nombre_formulario'] = "contrato/contrato_formulario.twig";
            $data["estado_propiedades"] = $this->atributo->getEstadosPropiedades();
            $data["tipo_propiedades"] = $this->atributo->getTipoPropiedades();
            $data["tipo_garantias"] = $this->atributo->getTipoGarantias();
            $data["atributos_propiedades"] = $this->atributo->getAtributosPropiedades();
            $data['title'] = "contratos";
            echo $template->render($data);
        }
    }

    public function getContratoAjax() {
        echo json_encode($this->contrato->getContratoAjax($this->input->get()));
    }

    public function borrar($con_id) {
        $this->contrato->borrar($con_id);
        redirect("admin-contrato");
    }

}
