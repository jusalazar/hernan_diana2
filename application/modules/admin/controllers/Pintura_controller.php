<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Pintura_model $pintura
 * @property Artista_model $artista
 *  @property Tipopintura_model $tipopintura
 */
class Pintura_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("pintura/pintura_model", "pintura");
        $this->load->model("artista/artista_model", "artista");
        $this->load->model("tipopintura/tipopintura_model", "tipopintura");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->pintura->insertar($this->post());
            redirect("admin-pintura");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["pintura"] = $this->pintura->getById($args);
            $data['nombrePagina'] = "Gestión de pinturas";
            $data['nombre_formulario'] = "pintura/pintura_formulario.twig";
            $data["artistas"] = $this->artista->getArtistasActivos();
            $data["tipos_pinturas"] = $this->tipopintura->getTiposActivos();
            $data['title'] = "pinturas";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de pinturas";
            $data['nombre_formulario'] = "pintura/pintura_formulario.twig";
            $data["artistas"] = $this->artista->getArtistasActivos();
            $data["tipos_pinturas"] = $this->tipopintura->getTiposActivos();
            $data['title'] = "pinturas";
            echo $template->render($data);
        }
    }

    public function getPinturaAjax() {
        echo json_encode($this->pintura->getPinturaAjax($this->input->get()));
    }

    public function borrar($pin_id) {
        $this->pintura->borrar($pin_id);
        redirect("admin-pintura");
    }

}
