<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Empresa_model $empresa
 */
class Empresa_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("empresa/empresa_model", "empresa");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->empresa->insertar($this->post());
            redirect("admin-empresa");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["empresa"] = $this->empresa->getById($args);
            $data['nombrePagina'] = "Gestión de empresas";
            $data['nombre_formulario'] = "empresa/empresa_formulario.twig";
            $data['title'] = "empresas";
            echo $template->render($data);            
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de empresas";
            $data['nombre_formulario'] = "empresa/empresa_formulario.twig";
            $data['title'] = "empresas";
            echo $template->render($data);
        }
    }

    public function getEmpresaAjax() {
        echo json_encode($this->empresa->getEmpresaAjax($this->input->get()));
    }
    
    public function borrar($emp_id){
        $this->empresa->borrar($emp_id);
        redirect("admin-empresa");
    }

}
