<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Roles_controller
 * @property Roles_model $roles
 */
class Roles_controller extends A_Controller {

    /**
     * [_construct description]
     * @return [type] [description]
     */
    public function __construct() {
        parent::__construct();
        $this->load->model("roles_model", "roles");
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index() {
        Auth::can("administrar roles");
        $template = $this->twig->load('roles.twig');
        $data['nombrePagina'] = "Admin Panel";
        $data['title'] = "Roles";
        echo $template->render($data);
    }

    public function show($id = false) {
        echo json_encode($this->roles->getRoles($id));
    }

    public function save() {
        if ($this->post("id") == 0) {
            echo json_encode($this->store());
        } else {
            echo json_encode($this->update($this->post('id')));
        }
    }

    public function create() {
        
    }

    public function store() {
        return $this->roles->store($this->post());
    }

    public function edit($id) {
        
    }

    public function update($id) {
        return $this->roles->update($id, $this->post());
    }

    public function destroy($id) {
        Auth::can("borrar roles");
        json_out($this->roles->destroy($id));
    }

}

/* End of file admin_controller.php */
/* Location: ./application/controllers/admin_controller.php */
