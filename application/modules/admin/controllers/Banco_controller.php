<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Banco_model $banco
 */
class Banco_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("banco/banco_model", "banco");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->banco->insertar($this->post());
            redirect("admin-banco");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["banco"] = $this->banco->getById($args);
            $data['nombrePagina'] = "Gestión de bancos";
            $data['nombre_formulario'] = "banco/banco_formulario.twig";
            $data['title'] = "bancos";
            echo $template->render($data);            
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de bancos";
            $data['nombre_formulario'] = "banco/banco_formulario.twig";
            $data['title'] = "bancos";
            echo $template->render($data);
        }
    }

    public function getBancoAjax() {
        echo json_encode($this->banco->getBancoAjax($this->input->get()));
    }
    
    public function borrar($ban_id){
        $this->banco->borrar($ban_id);
        redirect("admin-banco");
    }

}
