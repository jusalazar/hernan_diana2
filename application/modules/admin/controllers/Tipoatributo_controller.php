<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Tipoatributo_model $tipoatributo
 */
class Tipoatributo_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("tipoatributo/tipoatributo_model", "tipoatributo");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->tipoatributo->insertar($this->post());
            redirect("admin-tipoatributo");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["tipoatributo"] = $this->tipoatributo->getById($args);
            $data['nombrePagina'] = "Gestión de tipoatributos";
            $data['nombre_formulario'] = "tipoatributo/tipoatributo_formulario.twig";
            $data['title'] = "tipoatributos";
            echo $template->render($data);            
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de tipoatributos";
            $data['nombre_formulario'] = "tipoatributo/tipoatributo_formulario.twig";
            $data['title'] = "tipoatributos";
            echo $template->render($data);
        }
    }

    public function getTipoatributoAjax() {
        echo json_encode($this->tipoatributo->getTipoatributoAjax($this->input->get()));
    }
    
    public function borrar($tip_id){
        $this->tipoatributo->borrar($tip_id);
        redirect("admin-tipoatributo");
    }

}
