<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Feriado_model $feriado
 */
class Feriado_controller extends A_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("admin/feriado_model", "feriado");
    }

    /**
     *  get all feriados
     */
    function index()
    {
        json_out($this->feriado->getAll());
    }

    /**
     * show form for a feriado
     */
    function create()
    {

    }

    /**
     * save data for a feriado
     */
    function store()
    {
        json_out($this->feriado->store($this->post()));
    }

    /**
     * show one feriado by id
     */
    function show()
    {
        json_out($this->feriado->getFeriadoById($this->post("id")));
    }

    /**
     * show form for edit a feriado
     */
    function edit()
    {
    }

    /**
     * updates a feriado
     */
    function update()
    {
        json_out(($this->feriado->edit($this->post())));
    }

    /**
     * deletes a feriado
     */
    function destroy()
    {
        json_out($this->feriado->destroy($this->post("id")));
    }

    function save() {
        if($this->post("id") == 0) {
            return $this->store();
        } else {
            return $this->update();
        }
    }

}