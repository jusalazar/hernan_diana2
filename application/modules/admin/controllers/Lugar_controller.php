<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Lugar_model $lugar
 */
class Lugar_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("lugar/lugar_model", "lugar");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->lugar->insertar($this->post());
            redirect("admin-lugar");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["lugar"] = $this->lugar->getById($args);
            $data['nombrePagina'] = "Gestión de lugars";
            $data['nombre_formulario'] = "lugar/lugar_formulario.twig";
            $data['title'] = "lugars";
            echo $template->render($data);            
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de lugars";
            $data['nombre_formulario'] = "lugar/lugar_formulario.twig";
            $data['title'] = "lugars";
            echo $template->render($data);
        }
    }

    public function getLugarAjax() {
        echo json_encode($this->lugar->getLugarAjax($this->input->get()));
    }
    
    public function borrar($lug_id){
        $this->lugar->borrar($lug_id);
        redirect("admin-lugar");
    }

}
