<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Compra_model $compra
 * @property Evento_model $evento
 */
class Compra_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("compra/compra_model", "compra");
        $this->load->model("evento/evento_model", "evento");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->compra->insertar($this->post());
            redirect("admin-compras");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["compra"] = $this->compra->getById($args);
            $data['nombre_formulario'] = "compra/compra_formulario.twig";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombre_formulario'] = "compra/compra_formulario.twig";
            $data["eventos"] = $this->evento->getAllEventos();
            echo $template->render($data);
        }
    }

    public function getCompraAjax() {
        echo json_encode($this->compra->getComprasAjax($this->input->get()));
    }

    public function compra_preparando($com_id) {
        $this->compra->cambiarEstado($com_id, COMPRA_PROCESANDO);
        if (Auth::esAdmin()) {
            redirect("admin-compras");
        } else {
            redirect("admin-proveedor-mis-compras");
        }
    }

    public function compra_entregada($com_id) {
        $this->compra->cambiarEstado($com_id, COMPRA_ENTREGADA);
        if (Auth::esAdmin()) {
            redirect("admin-compras");
        } else {
            redirect("admin-proveedor-mis-compras");
        }
    }

    function ver_compra($com_id) {
        $template = $this->twig->load('maker_default.twig');
        $data['nombre_formulario'] = "compra/compra_ver.twig";
        $data['compra'] = $this->compra->getCompraById($com_id);
        echo $template->render($data);
    }

    function delegar($com_id = false) {
        if ($this->post()) {
            $this->compra->delegar($this->post());
            redirect("admin-compra/delegar/" . $this->post("com_id"));
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombre_formulario'] = "compra/compra_delegar.twig";
            $data['compra'] = $this->compra->getCompraById($com_id);
            $data['proveedores'] = $this->compra->delegacionesPosibles($data['compra']);
            echo $template->render($data);
        }
    }

    function proveedor_delegar($com_id) {
        $this->compra->proveedorDelegar($com_id);
        redirect("admin-proveedor-mis-compras");
    }

    function borrar($com_id){
        $this->compra->borrarPedido($com_id);
        redirect("admin-compras");
    }
}
