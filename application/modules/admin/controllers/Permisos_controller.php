<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Roles_controller
 * @property Permisos_model $permisos
 */
class Permisos_controller extends A_Controller
{

    /**
     * [_construct description]
     * @return [type] [description]
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("permisos_model", "permisos");
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        Auth::can("administrar permisos");
        $template = $this->twig->load('permisos.twig');
        $data['nombrePagina'] = "Panel de permisos";
        $data['title'] = "Roles";
        echo $template->render($data);
    }

    public function show($id = false)
    {
        json_out($this->permisos->getPermisos($id));
    }

    public function save()
    {
        if (Auth::check("administrar permisos")) :
            if ($this->post("id") == 0) {
                echo json_encode($this->store());
            } else {
                echo json_encode($this->update($this->post('id')));
            }
        endif;
    }

    public function create()
    {

    }

    public function store()
    {
        return $this->permisos->store($this->post());
    }

    public function edit($id)
    {

    }

    public function update($id)
    {
        return $this->permisos->update($id, $this->post());
    }

    public function destroy($id)
    {
        Auth::can("borrar permisos");
        json_out($this->permisos->destroy($id));
    }
}
/* End of file admin_controller.php */
/* Location: ./application/controllers/admin_controller.php */
