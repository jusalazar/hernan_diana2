<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Admin_model $admin
 */
class Admin_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("admin_model", "admin");
    }

    public function index() {
        $template = $this->twig->load('roles.twig');
        $data['nombrePagina'] = "Admin Panel";
        $data['title'] = "Roles";
        echo $template->render($data);
    }

    public function dashboard() {
        $template = $this->twig->load('maker_default.twig');
        $data['nombre_formulario'] = "admin/dashboard/admin_dashboard.twig";
        $data['title'] = "Mi panel";
        $data["dashboard"] = $this->admin->getDashboard();
        echo $template->render($data);
    }

    function getRenglonesDemorados(){
        //comentarios
        echo json_encode($this->admin->getRenglonesDemorados($this->input->get()));
    }


}

