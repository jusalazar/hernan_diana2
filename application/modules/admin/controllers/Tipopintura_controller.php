<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Tipopintura_model $tipopintura
 */
class Tipopintura_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("tipopintura/tipopintura_model", "tipopintura");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->tipopintura->insertar($this->post());
            redirect("admin-tipopintura");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["tipopintura"] = $this->tipopintura->getById($args);
            $data['nombrePagina'] = "Gestión de tipopinturas";
            $data['nombre_formulario'] = "tipopintura/tipopintura_formulario.twig";
            $data['title'] = "tipopinturas";
            echo $template->render($data);            
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de tipopinturas";
            $data['nombre_formulario'] = "tipopintura/tipopintura_formulario.twig";
            $data['title'] = "tipopinturas";
            echo $template->render($data);
        }
    }

    public function getTipopinturaAjax() {
        echo json_encode($this->tipopintura->getTipopinturaAjax($this->input->get()));
    }
    
    public function borrar($tip_id){
        $this->tipopintura->borrar($tip_id);
        redirect("admin-tipopintura");
    }

}
