<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Cliente_model $cliente
 */
class Cliente_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("cliente/cliente_model", "cliente");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->cliente->insertar($this->post());
            redirect("admin-cliente");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["cliente"] = $this->cliente->getById($args);
            $data['nombrePagina'] = "Gestión de clientes";
            $data['nombre_formulario'] = "cliente/cliente_formulario.twig";
            $data['title'] = "clientes";
            echo $template->render($data);            
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de clientes";
            $data['nombre_formulario'] = "cliente/cliente_formulario.twig";
            $data['title'] = "clientes";
            echo $template->render($data);
        }
    }

    public function getClienteAjax() {
        echo json_encode($this->cliente->getClienteAjax($this->input->get()));
    }
    
    public function borrar($cli_id){
        $this->cliente->borrar($cli_id);
        redirect("admin-cliente");
    }

}
