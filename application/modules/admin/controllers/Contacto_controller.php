<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Contacto_model $contacto
 */
class Contacto_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("contacto/contacto_model", "contacto");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->contacto->insertar($this->post());
            redirect("admin-contacto");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["contacto"] = $this->contacto->getById($args);
            $data['nombrePagina'] = "Gestión de contactos";
            $data['nombre_formulario'] = "contacto/contacto_formulario.twig";
            $data['title'] = "contactos";
            echo $template->render($data);            
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de contactos";
            $data['nombre_formulario'] = "contacto/contacto_formulario.twig";
            $data['title'] = "contactos";
            echo $template->render($data);
        }
    }

    public function getContactoAjax() {
        echo json_encode($this->contacto->getContactoAjax($this->input->get()));
    }
    
    public function borrar($con_id){
        $this->contacto->borrar($con_id);
        redirect("admin-contacto");
    }

}
