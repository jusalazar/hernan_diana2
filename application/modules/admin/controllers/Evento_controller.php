<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Evento_model $evento
 * @property Pintura_model $pintura
 * @property Empresa_model $empresa
 * @property Asistente_model $asistente
 * @property Lugar_model $lugar
 * @property Artista_model $artista
 */
class Evento_controller extends A_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("evento/evento_model", "evento");
        $this->load->model("pintura/pintura_model", "pintura");
        $this->load->model("asistente/asistente_model", "asistente");
        $this->load->model("artista/artista_model", "artista");
        $this->load->model("lugar/lugar_model", "lugar");
        $this->load->model("empresa/empresa_model", "empresa");
    }

    public function index($args = false) {
        if ($this->post()) {
            $this->evento->insertar($this->post());
            redirect("admin-evento");
        } elseif ($args) {
            $template = $this->twig->load('maker_default.twig');
            $data["evento"] = $this->evento->getById($args);
            $data['nombrePagina'] = "Gestión de eventos";
            $data['nombre_formulario'] = "evento/evento_formulario.twig";
            $data["pinturas"] = $this->pintura->getPinturasActivas();
            $data["asistentes"] = $this->asistente->getAsistentesActivos();
            $data["artistas"] = $this->artista->getArtistasActivos();
            $data["empresas"] = $this->empresa->getEmpresasActivas();
            $data["lugares"] = $this->lugar->getLugaresActivos();
            $data['title'] = "eventos";
            echo $template->render($data);
        } else {
            $template = $this->twig->load('maker_default.twig');
            $data['nombrePagina'] = "Gestión de eventos";
            $data['nombre_formulario'] = "evento/evento_formulario.twig";
            $data["pinturas"] = $this->pintura->getPinturasActivas();
            $data["asistentes"] = $this->asistente->getAsistentesActivos();
            $data["artistas"] = $this->artista->getArtistasActivos();
            $data["empresas"] = $this->empresa->getEmpresasActivas();
            $data["lugares"] = $this->lugar->getLugaresActivos();
            $data['title'] = "eventos";
            echo $template->render($data);
        }
    }

    public function getEventoAjax() {
        echo json_encode($this->evento->getEventoAjax($this->input->get()));
    }

    public function borrar($eve_id) {
        $this->evento->borrar($eve_id);
        redirect("admin-evento");
    }

}
