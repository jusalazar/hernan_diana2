<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cuentabanco
 * 
 * @Table(name="cuentabanco") 
 * @Entity 
 */
class Cuentabanco {

    /**
     * @var integer $id
     *
     * @Column(name="cue_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="cue_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="cue_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="cue_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var integer $cbu
     *
     * @Column(name="cue_cbu", type="integer",nullable=true)
     * 
     */
    protected $cbu;

    /**
     * @var string $cbu_alias
     *
     * @Column(name="cue_cbu_alias", type="string",nullable=true)
     * 
     */
    protected $cbu_alias;

    /**
     * @var integer $numero
     *
     * @Column(name="cue_numero", type="integer",nullable=true)
     * 
     */
    protected $numero;

    /**
     * @var integer $cuit_titular
     *
     * @Column(name="cue_cuit_titular", type="integer",nullable=true)
     * 
     */
    protected $cuit_titular;

    /**
     * @var string $nombre_titular
     *
     * @Column(name="cue_nombre_titular", type="string",nullable=true)
     * 
     */
    protected $nombre_titular;

    /**
     * @var string $tipo_cuenta
     *
     * @Column(name="cue_tipo_cuenta", type="string",nullable=true)
     * 
     */
    protected $tipo_cuenta;

    /**
     * @var Admin\Banco $banco
     * @OneToOne(targetEntity="Admin\Banco")
     * @JoinColumn(name="cue_banco", referencedColumnName="ban_id")
     */
    private $banco;

    /**
     * @var integer $preferente
     *
     * @Column(name="cue_preferente", type="integer",nullable=true)
     * 
     */
    protected $preferente;

    /**
     * @var string $benefio_nro
     *
     * @Column(name="cue_beneficio_nro", type="string",nullable=true)
     * 
     */
    protected $benefio_nro;

    /**
     * @var Admin\Artista $artista
     * @OneToOne(targetEntity="Admin\Artista")
     * @JoinColumn(name="cue_artista", referencedColumnName="art_id")
     */
    private $artista;

    function getNombre(){
        return $this->getBanco()->getNombre();
    }
    
    function __construct($artista, $preferente, $cbu, $cbu_alias, $nro_beneficio, $cuit, $nro_cuenta, $tipo_cuenta, $banco, $nombre_titular) {
        $this->setCreated_at(new \DateTime());
        $this->setActivo(true);
        $this->actualizar($artista, $preferente, $cbu, $cbu_alias, $nro_beneficio, $cuit, $nro_cuenta, $tipo_cuenta, $banco, $nombre_titular);
    }

    function actualizar($artista, $preferente, $cbu, $cbu_alias, $nro_beneficio, $cuit, $nro_cuenta, $tipo_cuenta, $banco, $nombre_titular) {
        $this->setArtista($artista);
        $this->setPreferente($preferente);
        $this->setCbu(substr(str_replace("-", "", $cbu), 0, 21));
        $this->setCbu_alias($cbu_alias);
        $this->setBenefio_nro($nro_beneficio);
        $this->setCuit_titular(str_replace("-", "", $cuit));
        $this->setNumero($nro_cuenta);
        $this->setTipo_cuenta($tipo_cuenta);
        $this->setBanco($banco);
        $this->setModified_at(new \DateTime());
        $this->setNombre_titular($nombre_titular);
    }

    function getArtista() {
        return $this->artista;
    }

    function setArtista($artista) {
        $this->artista = $artista;
    }

    function getBenefio_nro() {
        return $this->benefio_nro;
    }

    function setBenefio_nro($benefio_nro) {
        $this->benefio_nro = $benefio_nro;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getCbu() {
        return $this->cbu;
    }

    function setCbu($cbu) {
        $this->cbu = $cbu;
    }

    function getCbu_alias() {
        return $this->cbu_alias;
    }

    function setCbu_alias($cbu_alias) {
        $this->cbu_alias = $cbu_alias;
    }

    function getNumero() {
        return $this->numero;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function getCuit_titular() {
        return $this->cuit_titular;
    }

    function setCuit_titular($cuit_titular) {
        $this->cuit_titular = $cuit_titular;
    }

    function getNombre_titular() {
        return $this->nombre_titular;
    }

    function setNombre_titular($nombre_titular) {
        $this->nombre_titular = $nombre_titular;
    }

    function getTipo_cuenta() {
        return $this->tipo_cuenta;
    }

    function setTipo_cuenta($tipo_cuenta) {
        $this->tipo_cuenta = $tipo_cuenta;
    }

    function getBanco() {
        return $this->banco;
    }

    function setBanco($banco) {
        $this->banco = $banco;
    }

    function getPreferente() {
        return $this->preferente;
    }

    function setPreferente($preferente) {
        $this->preferente = $preferente;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "cbu" => $this->getCbu(),
            "cbu_alias" => $this->getCbu_alias(),
            "numero" => $this->getNumero(),
            "cuit_titular" => $this->getCuit_titular(),
            "nombre_titular" => $this->getNombre_titular(),
            "tipo_cuenta" => $this->getTipo_cuenta(),
            "banco" => $this->getBanco(),
            "preferente" => $this->getPreferente(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-cuentabanco") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-cuentabanco/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

    function getCbuBloque1() {
        return substr($this->getCbu(), 0, 7);
    }

    function getCbuBloque2() {
        return substr($this->getCbu(), 8);
    }

}
