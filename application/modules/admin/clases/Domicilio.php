<?php

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Domicilio
 * 
 * @Table(name="domicilio") 
 * @Entity 
 */
class Domicilio {

    /**
     * @var integer $id
     *
     * @Column(name="dom_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="dom_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="dom_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="dom_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var string $calle
     *
     * @Column(name="dom_calle", type="string",nullable=true)
     * 
     */
    protected $calle;

    /**
     * @var string $altura
     *
     * @Column(name="dom_altura", type="string",nullable=true)
     * 
     */
    protected $altura;

    /**
     * @var string $piso
     *
     * @Column(name="dom_piso", type="string",nullable=true)
     * 
     */
    protected $piso;

    /**
     * @var string $dpto
     *
     * @Column(name="dom_dpto", type="string",nullable=true)
     * 
     */
    protected $dpto;

    /**
     * @var string $barrio
     *
     * @Column(name="dom_barrio", type="string",nullable=true)
     * 
     */
    protected $barrio;

    /**
     * @var string $longitud
     *
     * @Column(name="dom_longitud", type="string",nullable=true)
     * 
     */
    protected $longitud;

    /**
     * @var string $latitud
     *
     * @Column(name="dom_latitud", type="string",nullable=true)
     * 
     */
    protected $latitud;

    /**
     * @var string $codigo_postal
     *
     * @Column(name="dom_codigo_postal", type="string",nullable=true)
     * 
     */
    protected $codigo_postal;

    /**
     * @var string $ciudad
     *
     * @Column(name="dom_ciudad", type="string",nullable=true)
     * 
     */
    protected $ciudad;

    /**
     * @var string $pais
     *
     * @Column(name="dom_pais", type="string",nullable=true)
     * 
     */
    protected $pais;

    function __construct($altura, $barrio, $calle, $latitud, $longitud, $ciudad, $piso, $cod_postal, $pais, $activo) {
        $this->actualizar($altura, $barrio, $calle, $latitud, $longitud, $ciudad, $piso, $cod_postal, $pais, $activo);
    }

    function actualizar($altura, $barrio, $calle, $latitud, $longitud, $ciudad, $piso, $cod_postal, $pais, $activo) {
        $this->setAltura($altura);
        $this->setBarrio($barrio);
        $this->setCalle($calle);
        $this->setLatitud($latitud);
        $this->setLongitud($longitud);
        $this->setCiudad($ciudad);
        $this->setPiso($piso);
        $this->setCodigo_postal($cod_postal);
        $this->setPais($pais);
        $this->setActivo($activo);
    }

    function getDireccion_completa() {
        $direccion = $this->getCalle() . ' ' . $this->getAltura();
        $piso = "";
        $dpto = "";
        $localidad = "";
        if ($this->getDpto()) {
            $dpto = " Dpto:" . $this->getDpto();
        }
        if ($this->getPiso()) {
            $piso = " Piso:" . $this->getPiso();
        }
        $localidad = $this->getBarrio() . ' ' . $this->getCiudad();
        $direccion .= $piso . $dpto . $localidad;
        return $direccion;
    }

    function getDireccionInput() {
        return $this->getCalle() . ' ' . $this->getAltura() . ' ,' . $this->getBarrio() . ' ' . $this->getCiudad();
    }

    function getDirececionResumida() {
        $dir = $this->getCalle() . ' ' . $this->getAltura() . " Piso:" . $this->getPiso() . " Dpto:" . $this->getDpto();
        return strlen($dir) > 8 ? $dir : $this->getDireccion_completa();
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getCalle() {
        return $this->calle;
    }

    function setCalle($calle) {
        $this->calle = $calle;
    }

    function getAltura() {
        return $this->altura;
    }

    function setAltura($altura) {
        $this->altura = $altura;
    }

    function getPiso() {
        return $this->piso;
    }

    function setPiso($piso) {
        $this->piso = $piso;
    }

    function getDpto() {
        return $this->dpto;
    }

    function setDpto($dpto) {
        $this->dpto = $dpto;
    }

    function getBarrio() {
        return $this->barrio;
    }

    function setBarrio($barrio) {
        $this->barrio = $barrio;
    }

    function getLongitud() {
        return $this->longitud;
    }

    function setLongitud($longitud) {
        $this->longitud = $longitud;
    }

    function getLatitud() {
        return $this->latitud;
    }

    function setLatitud($latitud) {
        $this->latitud = $latitud;
    }

    function getCodigo_postal() {
        return $this->codigo_postal;
    }

    function setCodigo_postal($codigo_postal) {
        $this->codigo_postal = $codigo_postal;
    }

    function getCiudad() {
        return $this->ciudad;
    }

    function setCiudad($ciudad) {
        $this->ciudad = $ciudad;
    }

    function getPais() {
        return $this->pais;
    }

    function setPais($pais) {
        $this->pais = $pais;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "calle" => ($this->getCalle()),
            "altura" => $this->getAltura(),
            "piso" => $this->getPiso(),
            "dpto" => $this->getDpto(),
            "barrio" => $this->getBarrio(),
            "longitud" => $this->getLongitud(),
            "latitud" => $this->getLatitud(),
            "preferente" => $this->getPreferente(),
            "codigo_postal" => $this->getCodigo_postal(),
            "ciudad" => $this->getCiudad(),
            "pais" => $this->getPais(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-domicilio") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-domicilio/borrar/" . $this->getId()) . '" title="' . (!$this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . (!$this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

}
