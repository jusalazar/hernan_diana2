<?php

namespace Admin;
defined("BASEPATH") OR exit("No direct script access allowed");

use Tools\GenericClass;
use DateTime;
/**
 * Feriado
 *
 * @Table(name="feriados")
 * @Entity
 */
class Feriado extends GenericClass
{
    /**
     * @var string $descripcion
     * @Column(type="string", length=255, nullable=false, name="descripcion")
     */
    private $descripcion;
    /**
     * @var DateTime $fecha
     * @Column(type="datetime",nullable=false, name="fecha")
     */
    private $fecha;
    /**
     * @var boolean $movible
     * @Column(type="boolean" , name="movible")
     */
    private $movible;

    /**
     * Feriado constructor.
     * @param string $descripcion
     * @param DateTime $fecha
     * @param bool $movible
     */
    public function setear($descripcion, DateTime $fecha, $movible)
    {
        $this->descripcion = $descripcion;
        $this->fecha = $fecha;
        $this->movible = $movible;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param DateTime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return bool
     */
    public function isMovible()
    {
        return $this->movible;
    }

    /**
     * @param bool $movible
     */
    public function setMovible($movible)
    {
        $this->movible = $movible;
    }

    public function jsonSerialize()
    {
        return array(
            'title' => mb_strtoupper($this->descripcion),
            'start' => $this->fecha->format("Y-m-d"),
            'id' => $this->getId(),
            'movible' => $this->movible
        );
    }

}