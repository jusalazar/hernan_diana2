<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Lugar
 * 
 * @Table(name="lugar") 
 * @Entity 
 */
class Lugar {

    /**
     * @var integer $id
     *
     * @Column(name="lug_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="lug_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="lug_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="lug_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var string $nombre
     *
     * @Column(name="lug_nombre", type="string",nullable=true)
     * 
     */
    protected $nombre;

    /**
     * @var string $contacto_nombre
     *
     * @Column(name="lug_contacto_nombre", type="string",nullable=true)
     * 
     */
    protected $contacto_nombre;

    /**
     * @var string $contacto_email
     *
     * @Column(name="lug_contacto_email", type="string",nullable=true)
     * 
     */
    protected $contacto_email;

    /**
     * @var string $contacto_telefono
     *
     * @Column(name="lug_contacto_telefono", type="string",nullable=true)
     * 
     */
    protected $contacto_telefono;

    /**
     * @var integer $capacidad_lugar
     *
     * @Column(name="lug_capacidad_lugar", type="integer",nullable=true)
     * 
     */
    protected $capacidad_lugar;

    /**
     * @var Domicilio $domicilio
     * @OneToOne(targetEntity="\Domicilio")
     * @JoinColumn(name="lug_domicilio", referencedColumnName="dom_id")
     */
    private $domicilio;

    /**
     * @var boolean $privado
     *
     * @Column(name="lug_privado", type="boolean",nullable=true)
     * 
     */
    protected $privado;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function getContacto_nombre() {
        return $this->contacto_nombre;
    }

    function setContacto_nombre($contacto_nombre) {
        $this->contacto_nombre = $contacto_nombre;
    }

    function getContacto_email() {
        return $this->contacto_email;
    }

    function setContacto_email($contacto_email) {
        $this->contacto_email = $contacto_email;
    }

    function getContacto_telefono() {
        return $this->contacto_telefono;
    }

    function setContacto_telefono($contacto_telefono) {
        $this->contacto_telefono = $contacto_telefono;
    }

    function getCapacidad_lugar() {
        return $this->capacidad_lugar;
    }

    function setCapacidad_lugar($capacidad_lugar) {
        $this->capacidad_lugar = $capacidad_lugar;
    }

    function getDomicilio() {
        return $this->domicilio;
    }

    function setDomicilio($domicilio) {
        $this->domicilio = $domicilio;
    }

    function getPrivado() {
        return $this->privado;
    }

    function setPrivado($privado) {
        $this->privado = $privado;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "nombre" => $this->getNombre(),
            "contacto_nombre" => $this->getContacto_nombre(),
            "contacto_email" => $this->getContacto_email(),
            "contacto_telefono" => $this->getContacto_telefono(),
            "capacidad_lugar" => $this->getCapacidad_lugar(),
            "domicilio" => $this->getDomicilio()->getDireccion_completa(),
            "privado" => $this->getPrivado(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-lugar") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-lugar/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

    function getLugar() {
        return "#";
        $artista = str_replace(" ", "-", ($this->getNombre()));
        return base_url("lugar/" . $artista . "/" . $this->getId());
    }

}
