<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Compra
 *
 * @Table(name="compra")
 * @Entity
 */
class Compra {

    /**
     * @var integer $id
     *
     * @Column(name="com_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DateTime $fechaIn
     *
     * @Column(name="com_fecha_in", type="datetime")
     *
     */
    protected $fechaIn;

    /**
     * @var boolean $pagada
     *
     * @Column(name="com_pagado",type="boolean")
     *
     */
    protected $pagada;

    /**
     * @var DateTime $fecha_pago
     *
     * @Column(name="com_fecha_pago", type="datetime")
     *
     */
    protected $fecha_pago;

    /**
     * @var string $medio_de_pago
     *
     * @Column(name="com_medio_pago",type="string")
     *
     */
    protected $medio_de_pago;

    /**
     * @var boolean $eliminada
     *
     * @Column(name="com_eliminada",type="boolean")
     *
     */
    protected $eliminada;

    /**
     * @var Admin\Evento $evento
     * @OneToOne(targetEntity="Admin\Evento")
     * @JoinColumn(name="com_evento", referencedColumnName="eve_id")
     */
    private $evento;

    /**
     * @var Admin\Descuento $descuento
     * @OneToOne(targetEntity="Admin\Descuento")
     * @JoinColumn(name="com_descuento", referencedColumnName="des_id")
     */
    private $descuento;

    /**
     * @var \Compra $tarjeta_regalo
     * @OneToOne(targetEntity="\Compra")
     * @JoinColumn(name="tarjeta_regalo", referencedColumnName="com_id")
     */
    private $tarjeta_regalo;

    /**
     * @var string $comentarios
     *
     * @Column(name="com_comentarios",type="string")
     *
     */
    protected $comentarios;

    /**
     * @var string $comprador_nombre
     *
     * @Column(name="com_comprador_nombre",type="string")
     *
     */
    protected $comprador_nombre;

    /**
     * @var string $comprador_apellido
     *
     * @Column(name="com_comprador_apellido",type="string")
     *
     */
    protected $comprador_apellido;

    /**
     * @var string $comprador_email
     *
     * @Column(name="com_comprador_email",type="string")
     *
     */
    protected $comprador_email;

    /**
     * @var string $comprador_fb
     *
     * @Column(name="com_comprador_fb",type="string")
     *
     */
    protected $comprador_fb;

    /**
     * @var integer $cantidad
     *
     * @Column(name="com_cantidad",type="integer")
     *
     */
    protected $cantidad;

    /**
     * @var float $precio_unitario
     *
     * @Column(name="com_precio_unitario",type="float")
     *
     */
    protected $precio_unitario;

    /**
     * @var integer $es_tarjeta_regalo
     *
     * @Column(name="es_tarjeta_regalo",type="integer")
     *
     */
    protected $es_tarjeta_regalo;

    /**
     * @var integer $mp_payment_id
     *
     * @Column(name="com_mp_id",type="integer")
     *
     */
    protected $mp_payment_id;

    function getMp_payment_id() {
        return $this->mp_payment_id;
    }

    function setMp_payment_id($mp_payment_id) {
        $this->mp_payment_id = $mp_payment_id;
    }

    function getCodigo() {
        return PREFIX_COMPRA . $this->getId();
    }

    function getTarjeta_regalo() {
        return $this->tarjeta_regalo;
    }

    function setTarjeta_regalo($tarjeta_regalo) {
        $this->tarjeta_regalo = $tarjeta_regalo;
    }

    function getEs_tarjeta_regalo() {
        return $this->es_tarjeta_regalo;
    }

    function setEs_tarjeta_regalo($es_tarjeta_regalo) {
        $this->es_tarjeta_regalo = $es_tarjeta_regalo;
    }

    function getComprador() {
        return $this->getComprador_nombre() . ' ' . $this->getComprador_apellido() . ' ' . $this->getComprador_email();
    }

    function getId() {
        return $this->id;
    }

    function getFechaIn() {
        return $this->fechaIn;
    }

    function getPagada() {
        return $this->pagada;
    }

    function getFecha_pago() {
        return $this->fecha_pago;
    }

    function getMedio_de_pago() {
        return $this->medio_de_pago;
    }

    function getEliminada() {
        return $this->eliminada;
    }

    function getEvento() {
        return $this->evento;
    }

    function getDescuento() {
        return $this->descuento;
    }

    function getComentarios() {
        return $this->comentarios;
    }

    function getComprador_nombre() {
        return $this->comprador_nombre;
    }

    function getComprador_apellido() {
        return $this->comprador_apellido;
    }

    function getComprador_email() {
        return $this->comprador_email;
    }

    function getComprador_fb() {
        return $this->comprador_fb;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getPrecio_unitario() {
        return $this->precio_unitario;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFechaIn($fechaIn) {
        $this->fechaIn = $fechaIn;
    }

    function setPagada($pagada) {
        $this->pagada = $pagada;
    }

    function setFecha_pago($fecha_pago) {
        $this->fecha_pago = $fecha_pago;
    }

    function setMedio_de_pago($medio_de_pago) {
        $this->medio_de_pago = $medio_de_pago;
    }

    function setEliminada($eliminada) {
        $this->eliminada = $eliminada;
    }

    function setEvento($evento) {
        $this->evento = $evento;
    }

    function setDescuento($descuento) {
        $this->descuento = $descuento;
    }

    function setComentarios($comentarios) {
        $this->comentarios = $comentarios;
    }

    function setComprador_nombre($comprador_nombre) {
        $this->comprador_nombre = $comprador_nombre;
    }

    function setComprador_apellido($comprador_apellido) {
        $this->comprador_apellido = $comprador_apellido;
    }

    function setComprador_email($comprador_email) {
        $this->comprador_email = $comprador_email;
    }

    function setComprador_fb($comprador_fb) {
        $this->comprador_fb = $comprador_fb;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setPrecio_unitario($precio_unitario) {
        $this->precio_unitario = $precio_unitario;
    }

    function getTotalCompra() {
        $total_compra = $this->getCantidad() * $this->getPrecio_unitario();
        return $total_compra - $this->getTotalDescuento() - $this->getTotalTarjeta();
    }

    function getSubTotal() {
        return $this->getCantidad() * $this->getPrecio_unitario();
    }

    function getDatosArray() {
        $array = array(
            "com_id" => $this->getId(),
            "evento" => $this->getEvento() ? $this->getEvento()->getTitulo() : "Tarjeta de regalo",
            "total" => $this->getTotalCompra(),
            "comprador" => $this->getComprador_nombre() . ' ' . $this->getComprador_apellido() . ' ' . $this->getComprador_email(),
            "cantidad" => $this->getCantidad(),
            "estado" => $this->getPagada() ? "Pagada" : "Falta pagar",
            "fecha" => $this->getFechaIn()->format("d/m/Y h:i:s"),
            "com_acciones" => $this->getAcciones()
        );
        return $array;
    }

    function getAcciones() {
        $response = '<a class="edit ml10" href="' . base_url("admin-compra") . '/' . $this->getId() . '" title="Ver compra">
            <i class="fa fa-search"></i>';
        $response .= '<a class="borrar ml10" href="' . base_url("admin-compra") . '/borrar/' . $this->getId() . '" title="Borrar compra">
            <i class="fa fa-trash"></i>';
        return $response;
    }

    function getTotalDescuento() {
        if (!$this->getDescuento()) {
            return 0;
        }
        $total_compra = $this->getCantidad() * $this->getPrecio_unitario();
        $valor_descuento = $this->getDescuento()->getDescuento() ? $this->getDescuento()->getDescuento() / 100 : 0;
        $valor_importe = $this->getDescuento()->getImporte() ? $this->getDescuento()->getImporte() : 0;
        $total_compra = $valor_descuento ? ($total_compra * $valor_descuento) : $valor_importe;
        return $total_compra;
    }

    function getTotalTarjeta() {
        if (!$this->getTarjeta_regalo()) {
            return 0;
        }
        $total_compra = $this->getCantidad() * $this->getPrecio_unitario();
        $valor_descuento = $this->getTarjeta_regalo()->getDescuento() ? $this->getTarjeta_regalo()->getDescuento() / 100 : 0;
        $valor_importe = $this->getTarjeta_regalo()->getImporte() ? $this->getTarjeta_regalo()->getImporte() : 0;
        $total_compra = $valor_descuento ? ($total_compra * $valor_descuento) : $valor_importe;
        return $total_compra;
    }

}
