<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Evento
 * 
 * @Table(name="evento") 
 * @Entity 
 */
class Evento {

    /**
     * @var integer $id
     *
     * @Column(name="eve_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="eve_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="eve_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="eve_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var string $titulo
     *
     * @Column(name="eve_titulo", type="string",nullable=true)
     * 
     */
    protected $titulo;

    /**
     * @var datetime $fecha_evento
     *
     * @Column(name="eve_fecha_evento", type="datetime",nullable=true)
     * 
     */
    protected $fecha_evento;

    /**
     * @var integer $capacidad
     *
     * @Column(name="eve_capacidad", type="integer",nullable=true)
     * 
     */
    protected $capacidad;

    /**
     * @var string $descripcion
     *
     * @Column(name="eve_descripcion", type="string",nullable=true)
     * 
     */
    protected $descripcion;

    /**
     * @var string $notas
     *
     * @Column(name="eve_notas", type="string",nullable=true)
     * 
     */
    protected $notas;

    /**
     * @var float $importe
     *
     * @Column(name="eve_importe", type="float",nullable=true)
     * 
     */
    protected $importe;

    /**
     * @var float $fee
     *
     * @Column(name="eve_fee", type="float",nullable=true)
     * 
     */
    protected $fee;

    /**
     * @var string $recaudador_nombre
     *
     * @Column(name="eve_recaudador_nombre", type="string",nullable=true)
     * 
     */
    protected $recaudador_nombre;

    /**
     * @var string $recaudador_email
     *
     * @Column(name="eve_recaudador_email", type="string",nullable=true)
     * 
     */
    protected $recaudador_email;

    /**
     * @var string $recaudador_codigo
     *
     * @Column(name="eve_recaudador_codigo", type="string",nullable=true)
     * 
     */
    protected $recaudador_codigo;

    /**
     * @var float $recaudador_porcentaje
     *
     * @Column(name="eve_recaudador_porcentaje", type="float",nullable=true)
     * 
     */
    protected $recaudador_porcentaje;

    /**
     * @var Admin\Empresa $empresa
     * @OneToOne(targetEntity="Admin\Empresa")
     * @JoinColumn(name="eve_empresa", referencedColumnName="emp_id")
     */
    private $empresa;

    /**
     * @var Admin\Asistente $asistente
     * @OneToOne(targetEntity="Admin\Asistente")
     * @JoinColumn(name="eve_asistente", referencedColumnName="asi_id")
     */
    private $asistente;

    /**
     * @var Admin\Lugar $lugar
     * @OneToOne(targetEntity="Admin\Lugar")
     * @JoinColumn(name="eve_lugar", referencedColumnName="lug_id")
     */
    private $lugar;

    /**
     * @var \Admin\Pintura $pintura
     * @OneToOne(targetEntity="Admin\Pintura")
     * @JoinColumn(name="eve_pintura", referencedColumnName="pin_id")
     */
    private $pintura;

    /**
     * @var \Admin\Artista $artista
     * @OneToOne(targetEntity="Admin\Artista")
     * @JoinColumn(name="eve_artista", referencedColumnName="art_id")
     */
    private $artista;

    /**
     * @var string $forma_pago
     * @Column(name="eve_forma_pago", type="string",nullable=true)
     */
    protected $forma_pago;

    /**
     * @var string $moneda
     *
     * @Column(name="eve_moneda", type="string",nullable=true)
     * 
     */
    protected $moneda;

    /**
     * @var integer $recaudador_caridad
     *
     * @Column(name="eve_recaudador_caridad", type="integer",nullable=true)
     * 
     */
    protected $recaudador_caridad;

    /**
     * @var \Compra[] $compras
     * @OneToMany(targetEntity="\Compra", mappedBy="evento")
     * @OrderBy({"id" = "DESC"})
     */
    private $compras;

    function getArtista() {
        return $this->artista;
    }

    function setArtista($artista) {
        $this->artista = $artista;
    }

    function __construct() {
        $this->compras = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function getCompras() {
        return $this->compras;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function getFecha_evento() {
        return $this->fecha_evento;
    }

    function setFecha_evento($fecha_evento) {
        $this->fecha_evento = $fecha_evento;
    }

    function getCapacidad() {
        return $this->capacidad;
    }

    function setCapacidad($capacidad) {
        $this->capacidad = $capacidad;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function getNotas() {
        return $this->notas;
    }

    function setNotas($notas) {
        $this->notas = $notas;
    }

    function getImporte() {
        return $this->importe;
    }

    function setImporte($importe) {
        $this->importe = $importe;
    }

    function getFee() {
        return $this->fee;
    }

    function setFee($fee) {
        $this->fee = $fee;
    }

    function getRecaudador_nombre() {
        return $this->recaudador_nombre;
    }

    function setRecaudador_nombre($recaudador_nombre) {
        $this->recaudador_nombre = $recaudador_nombre;
    }

    function getRecaudador_email() {
        return $this->recaudador_email;
    }

    function setRecaudador_email($recaudador_email) {
        $this->recaudador_email = $recaudador_email;
    }

    function getRecaudador_codigo() {
        return $this->recaudador_codigo;
    }

    function setRecaudador_codigo($recaudador_codigo) {
        $this->recaudador_codigo = $recaudador_codigo;
    }

    function getRecaudador_porcentaje() {
        return $this->recaudador_porcentaje;
    }

    function setRecaudador_porcentaje($recaudador_porcentaje) {
        $this->recaudador_porcentaje = $recaudador_porcentaje;
    }

    function getEmpresa() {
        return $this->empresa;
    }

    function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }

    function getAsistente() {
        return $this->asistente;
    }

    function setAsistente($asistente) {
        $this->asistente = $asistente;
    }

    function getLugar() {
        return $this->lugar;
    }

    function setLugar($lugar) {
        $this->lugar = $lugar;
    }

    function getPintura() {
        return $this->pintura;
    }

    function setPintura($pintura) {
        $this->pintura = $pintura;
    }

    function getForma_pago() {
        return $this->forma_pago;
    }

    function setForma_pago($forma_pago) {
        $this->forma_pago = $forma_pago;
    }

    function getMoneda() {
        return $this->moneda;
    }

    function setMoneda($moneda) {
        $this->moneda = $moneda;
    }

    function getRecaudador_caridad() {
        return $this->recaudador_caridad;
    }

    function setRecaudador_caridad($recaudador_caridad) {
        $this->recaudador_caridad = $recaudador_caridad;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "titulo" => $this->getTitulo(),
            "fecha_evento" => $this->getFecha_evento()->format('Y-m-d'),
            "capacidad" => $this->getCapacidad(),
            "descripcion" => $this->getDescripcion(),
            "notas" => $this->getNotas(),
            "domicilio" => $this->getLugar()->getDomicilio()->getDireccion_completa(),
            "importe" => $this->getImporte(),
            "fee" => $this->getFee(),
            "recaudador_nombre" => $this->getRecaudador_nombre(),
            "recaudador_email" => $this->getRecaudador_email(),
            "recaudador_codigo" => $this->getRecaudador_codigo(),
            "recaudador_porcentaje" => $this->getRecaudador_porcentaje(),
            "empresa" => $this->getEmpresa()->getNombre(),
            "asistente" => $this->getAsistente()->getNombre(),
            "lugar" => $this->getLugar()->getNombre(),
            "pintura" => $this->getPintura()->getNombre(),
            "forma_pago" => $this->getForma_pago(),
            "moneda" => $this->getMoneda(),
            "recaudador_caridad" => $this->getRecaudador_caridad(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-evento") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-evento/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

    function getEntradasDisponibles($cantidad = false) {
        $capacidad = $this->getCapacidad();
        $comprado = 0;
        foreach ($this->getCompras() as $cada_compra) {
            if (!$cada_compra->getEliminada() && $cada_compra->getPagada()) {
                $comprado += $cada_compra->getCantidad();
            }
        }
        if ($cantidad) {
            $comprado += $cantidad;
        }
        if ($capacidad >= $comprado) {
            return $capacidad - $comprado;
        }
        return false;
    }

    function formaTituloParaUrl() {
        $titulo = str_replace(" ", "-", $this->getTitulo());
        $titulo = str_replace("/", "-", $titulo);
        return $titulo;
    }

    function getLinkComprar() {
        $titulo = $this->formaTituloParaUrl();
        return base_url("comprar/" . $titulo . "/" . $this->getId());
    }

    function getLink() {
        $titulo = $this->formaTituloParaUrl();
        return base_url("evento/" . $titulo . "/" . $this->getId());
    }

}
