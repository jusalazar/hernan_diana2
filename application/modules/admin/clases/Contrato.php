<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Contrato
 * 
 * @Table(name="contrato") 
 * @Entity 
 */
class Contrato {

    /**
     * @var integer $id
     *
     * @Column(name="con_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="con_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="con_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="con_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var \Domicilio $domicilio
     * @OneToOne(targetEntity="\Domicilio")
     * @JoinColumn(name="con_domicilio", referencedColumnName="dom_id")
     */
    private $domicilio;

    /**
     * @var float $m2
     *
     * @Column(name="con_m2", type="float",nullable=true)
     * 
     */
    protected $m2;

    /**
     * @var Admin\Atributo $estado_inmueble
     * @OneToOne(targetEntity="Admin\Atributo")
     * @JoinColumn(name="con_atr_estado_inmueble", referencedColumnName="atr_id")
     */
    private $estado_inmueble;

    /**
     * @var Admin\Atributo $atr_tipo_propiedad
     * @OneToOne(targetEntity="Admin\Atributo")
     * @JoinColumn(name="con_atr_tipo_propiedad", referencedColumnName="atr_id")
     */
    private $atr_tipo_propiedad;

    /**
     * @var Admin\Atributo $atr_tipo_garantia
     * @OneToOne(targetEntity="Admin\Atributo")
     * @JoinColumn(name="con_atr_tipo_garantia", referencedColumnName="atr_id")
     */
    private $atr_tipo_garantia;

    /**
     * @var string $descripcion
     *
     * @Column(name="con_descripcion", type="string",nullable=true)
     * 
     */
    protected $descripcion;

    /**
     * @var string $email
     *
     * @Column(name="con_email", type="string",nullable=true)
     * 
     */
    protected $email;

    /**
     * @var string $locador_direccion
     *
     * @Column(name="locador_direccion", type="string",nullable=true)
     * 
     */
    protected $locador_direccion;

    /**
     * @var string $locador_nombre
     *
     * @Column(name="locador_nombre", type="string",nullable=true)
     * 
     */
    protected $locador_nombre;

    /**
     * @var string $locador_apellido
     *
     * @Column(name="locador_apellido", type="string",nullable=true)
     * 
     */
    protected $locador_apellido;

    /**
     * @var string $locador_dni
     *
     * @Column(name="locador_dni", type="string",nullable=true)
     * 
     */
    protected $locador_dni;

    /**
     * @var string $locador_telefono
     *
     * @Column(name="locador_telefono", type="string",nullable=true)
     * 
     */
    protected $locador_telefono;

    /**
     * @var string $locatario_direccion
     *
     * @Column(name="locatario_direccion", type="string",nullable=true)
     * 
     */
    protected $locatario_direccion;

    /**
     * @var string $tipoAumento
     *
     * @Column(name="tipo_aumento", type="string",nullable=true)
     * 
     */
    protected $tipoAumento;

    /**
     * @var float $porcentajeAumento
     *
     * @Column(name="porcentaje_aumento", type="float",nullable=true)
     * 
     */
    protected $porcentajeAumento;

    /**
     * @var string $locatario_nombre
     *
     * @Column(name="locatario_nombre", type="string",nullable=true)
     * 
     */
    protected $locatario_nombre;

    /**
     * @var string $locatario_apellido
     *
     * @Column(name="locatario_apellido", type="string",nullable=true)
     * 
     */
    protected $locatario_apellido;

    /**
     * @var string $locatario_dni
     *
     * @Column(name="locatario_dni", type="string",nullable=true)
     * 
     */
    protected $locatario_dni;

    /**
     * @var string $fiador_dni
     *
     * @Column(name="fiador_dni", type="string",nullable=true)
     * 
     */
    protected $fiador_dni;

    /**
     * @var string $fiador_apellido
     *
     * @Column(name="fiador_apellido", type="string",nullable=true)
     * 
     */
    protected $fiador_apellido;

    /**
     * @var string $fiador_nombre
     *
     * @Column(name="fiador_nombre", type="string",nullable=true)
     * 
     */
    protected $fiador_nombre;

    /**
     * @var string $fiador_direccion
     *
     * @Column(name="fiador_direccion", type="string",nullable=true)
     * 
     */
    protected $fiador_direccion;

    /**
     * @var string $fiador_sexo
     *
     * @Column(name="fiador_sexo", type="string",nullable=true)
     * 
     */
    protected $fiador_sexo;

    /**
     * @var string $fiador_sexo
     *
     * @Column(name="fiador_sexo", type="string",nullable=true)
     * 
     */
    protected $fiador_tipo_garantia;

    /**
     * @var string $alquiler_tipo
     *
     * @Column(name="alquiler_tipo", type="string",nullable=true)
     * 
     */
    protected $alquiler_tipo;

    /**
     * @var float $alquiler_valor
     *
     * @Column(name="alquiler_valor", type="float",nullable=true)
     * 
     */
    protected $alquiler_valor;

    /**
     * @var integer $pago_primer_dia
     *
     * @Column(name="locacion_pago_intervalo_del", type="integer",nullable=true)
     * 
     */
    protected $pago_primer_dia;

    /**
     * @var integer $pago_ultimo_dia
     *
     * @Column(name="locacion_pago_intervalo_al", type="integer",nullable=true)
     * 
     */
    protected $pago_ultimo_dia;

    /**
     * @var datetime $fecha_inicio_contrato
     *
     * @Column(name="locacion_fecha_inicio", type="datetime",nullable=true)
     */
    private $fecha_inicio_contrato;

    /**
     * @var integer $duracion_contrato
     *
     * @Column(name="locacion_duracion_meses", type="integer",nullable=true)
     * 
     */
    protected $duracion_contrato;

    /**
     * @var string $destino_locacion
     *
     * @Column(name="locacion_destino_locacion", type="string",nullable=true)
     * 
     */
    protected $destino_locacion;

    /**
     * @var string $locatario_sexo
     *
     * @Column(name="locatario_sexo", type="string",nullable=true)
     * 
     */
    protected $locatario_sexo;

    /**
     * @var string $locador_sexo
     *
     * @Column(name="locador_sexo", type="string",nullable=true)
     * 
     */
    protected $locador_sexo;

    /**
     * @var string $paymentId
     *
     * @Column(name="payment_id", type="string",nullable=true)
     * 
     */
    protected $paymentId;

    /**
     * @var datetime $fechaPago
     *
     * @Column(name="fecha_pago", type="datetime",nullable=true)
     * 
     */
    protected $fechaPago;

    /**
     * @var Admin\Contratoatributo[] $atributos
     * @OneToMany(targetEntity="Admin\Contratoatributo", mappedBy="contrato")
     * @OrderBy({"id" = "DESC"})
     */
    private $atributos;

    function getPaymentId() {
        return $this->paymentId;
    }

    function getFechaPago() {
        return $this->fechaPago;
    }

    function setPaymentId($paymentId) {
        $this->paymentId = $paymentId;
    }

    function setFechaPago($fechaPago) {
        $this->fechaPago = $fechaPago;
    }

    function setPorcentajeAumento($porcentajeAumento) {
        $this->porcentajeAumento = $porcentajeAumento;
    }

    function setTipoAumento($tipoAumento) {
        $this->tipoAumento = $tipoAumento;
    }

    function getLocador_telefono() {
        return $this->locador_telefono;
    }

    function setLocador_telefono($locador_telefono) {
        $this->locador_telefono = $locador_telefono;
    }

    function __construct() {
        $this->atributos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function getAtributos() {
        return $this->atributos;
    }

    function setAtributos(array $atributos) {
        $this->atributos = $atributos;
    }

    function getPago_primer_dia() {
        return $this->pago_primer_dia;
    }

    function getPago_ultimo_dia() {
        return $this->pago_ultimo_dia;
    }

    function getFecha_inicio_contrato() {
        return $this->fecha_inicio_contrato;
    }

    function getDuracion_contrato() {
        return $this->duracion_contrato;
    }

    function getDestino_locacion() {
        return $this->destino_locacion;
    }

    function getLocatario_sexo() {
        return $this->locatario_sexo;
    }

    function getLocador_sexo() {
        return $this->locador_sexo;
    }

    function setPago_primer_dia($pago_primer_dia) {
        $this->pago_primer_dia = $pago_primer_dia;
    }

    function setPago_ultimo_dia($pago_ultimo_dia) {
        $this->pago_ultimo_dia = $pago_ultimo_dia;
    }

    function setFecha_inicio_contrato($fecha_inicio_contrato) {
        $this->fecha_inicio_contrato = $fecha_inicio_contrato;
    }

    function setDuracion_contrato($duracion_contrato) {
        $this->duracion_contrato = $duracion_contrato;
    }

    function setDestino_locacion($destino_locacion) {
        $this->destino_locacion = $destino_locacion;
    }

    function setLocatario_sexo($locatario_sexo) {
        $this->locatario_sexo = $locatario_sexo;
    }

    function setLocador_sexo($locador_sexo) {
        $this->locador_sexo = $locador_sexo;
    }

    function getAlquiler_tipo() {
        return $this->alquiler_tipo;
    }

    function getAlquiler_valor() {
        return $this->alquiler_valor;
    }

    function setAlquiler_tipo($alquiler_tipo) {
        $this->alquiler_tipo = $alquiler_tipo;
    }

    function setAlquiler_valor($alquiler_valor) {
        $this->alquiler_valor = $alquiler_valor;
    }

    function getLocador_direccion() {
        return $this->locador_direccion;
    }

    function getLocador_nombre() {
        return $this->locador_nombre;
    }

    function getLocador_apellido() {
        return $this->locador_apellido;
    }

    function getLocador_dni() {
        return $this->locador_dni;
    }

    function getLocatario_direccion() {
        return $this->locatario_direccion;
    }

    function getLocatario_nombre() {
        return $this->locatario_nombre;
    }

    function getLocatario_apellido() {
        return $this->locatario_apellido;
    }

    function getLocatario_dni() {
        return $this->locatario_dni;
    }

    function getFiador_dni() {
        return $this->fiador_dni;
    }

    function getFiador_apellido() {
        return $this->fiador_apellido;
    }

    function getFiador_nombre() {
        return $this->fiador_nombre;
    }

    function getFiador_direccion() {
        return $this->fiador_direccion;
    }

    function getFiador_sexo() {
        return $this->fiador_sexo;
    }

    function getFiador_tipo_garantia() {
        return $this->fiador_tipo_garantia;
    }

    function setLocador_direccion($locador_direccion) {
        $this->locador_direccion = $locador_direccion;
    }

    function setLocador_nombre($locador_nombre) {
        $this->locador_nombre = $locador_nombre;
    }

    function setLocador_apellido($locador_apellido) {
        $this->locador_apellido = $locador_apellido;
    }

    function setLocador_dni($locador_dni) {
        $this->locador_dni = $locador_dni;
    }

    function setLocatario_direccion($locatario_direccion) {
        $this->locatario_direccion = $locatario_direccion;
    }

    function setLocatario_nombre($locatario_nombre) {
        $this->locatario_nombre = $locatario_nombre;
    }

    function setLocatario_apellido($locatario_apellido) {
        $this->locatario_apellido = $locatario_apellido;
    }

    function setLocatario_dni($locatario_dni) {
        $this->locatario_dni = $locatario_dni;
    }

    function setFiador_dni($fiador_dni) {
        $this->fiador_dni = $fiador_dni;
    }

    function setFiador_nombre($fiador_nombre) {
        $this->fiador_nombre = $fiador_nombre;
    }

    function setFiador_apellido($fiador_apellido) {
        $this->fiador_apellido = $fiador_apellido;
    }

    function setFiador_direccion($fiador_direccion) {
        $this->fiador_direccion = $fiador_direccion;
    }

    function setFiador_sexo($fiador_sexo) {
        $this->fiador_sexo = $fiador_sexo;
    }

    function setFiador_tipo_garantia($fiador_tipo_garantia) {
        $this->fiador_tipo_garantia = $fiador_tipo_garantia;
    }


    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getDomicilio() {
        return $this->domicilio;
    }

    function setDomicilio($domicilio) {
        $this->domicilio = $domicilio;
    }

    function getM2() {
        return $this->m2;
    }

    function setM2($m2) {
        $this->m2 = $m2;
    }

    function getEstado_inmueble() {
        return $this->estado_inmueble;
    }

    function setEstado_inmueble($estado_inmueble) {
        $this->estado_inmueble = $estado_inmueble;
    }

    function getAtr_tipo_propiedad() {
        return $this->atr_tipo_propiedad;
    }

    function getAtr_tipo_garantia() {
        return $this->atr_tipo_garantia;
    }

    function setAtr_tipo_propiedad($atr_tipo_propiedad) {
        $this->atr_tipo_propiedad = $atr_tipo_propiedad;
    }

    function setAtr_tipo_garantia($atr_tipo_garantia) {
        $this->atr_tipo_garantia = $atr_tipo_garantia;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "domicilio" => $this->getDomicilio()?$this->getDomicilio()->getDireccion_completa():"sin domicilio",
            "m2" => $this->getM2(),
            "estado_inmueble" => $this->getEstado_inmueble()->getNombre(),
            "atr_tipo_propiedad" => $this->getAtr_tipo_propiedad()->getNombre(),
            "atr_tipo_garantia" => $this->getAtr_tipo_garantia()->getNombre(),
            "descripcion" => $this->getDescripcion(),
            "email" => $this->getEmail(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-contrato") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i></a> &nbsp;&nbsp;
            <a class="edit ml10" target="_BLANK" href="' . base_url("admin-contrato") . '/reporte/' . $this->getId() . '" title="Reporte">
            <i class="fa fa-book"></i></a> &nbsp;&nbsp;
            <a class="edit ml10" href="' . base_url("admin-contrato/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

    function getMontoTexto($importe, $sin_moneda = false) {
        $number = new \NumberToLetterConverter();
        if ($sin_moneda) {
            return $number->convertNumber($importe, null, 0) . " (" . $importe . ")";
        } else {
            return $number->convertNumber($importe, null, 0) . " ($" . $importe . ")";
        }
    }

    function mostrarAtributosPDF() {
        $out = "";
        foreach ($this->getAtributos() as $k => $cada_atributo) {
            $out.=($k > 0 ? "," : "") . $cada_atributo->getAtributo()->getNombre();
        }
        return $out;
    }

    function getPeriodoDuracionContrato() {
        $number = new \NumberToLetterConverter();
        return str_replace(" ", "", $number->convertNumber($this->getDuracion_contrato(), null, 0)) . " (" . $this->getDuracion_contrato() . ") meses";
    }

    function getFecha_fin_contrato() {
        $inicio = clone $this->getFecha_inicio_contrato();
        $inicio->add(new \DateInterval("P" . $this->getDuracion_contrato() . "M"));
        return $inicio;
    }

    function getTotal() {
        return $this->getAlquiler_valor() * $this->getDuracion_contrato();
    }

    function getTotalALetras() {
        return $this->getMontoTexto($this->getTotal());
    }

    function getDeposito() {
        return $this->getAlquiler_valor();
    }

    function getTipoAumento() {
        return $this->tipoAumento;
    }

    function getPorcentajeAumento() {
        return $this->porcentajeAumento;
    }

    function getFormaPagoPDF() {
        $divisor = $this->getDuracion_contrato();
        if ($this->getTipoAumento() == "anual") {
            $divisor = 12;
        } elseif ($this->getTipoAumento() == "semestral") {
            $divisor = 6;
        } elseif ($this->getTipoAumento() == "cuatrimestral") {
            $divisor = 4;
        } elseif ($this->getTipoAumento() == "trimestral") {
            $divisor = 3;
        } else {
            $divisor = $this->getDuracion_contrato();
        }
        $iteraciones = ($this->getDuracion_contrato() / $divisor) - 1;
        $cuota_anterior = $this->getAlquiler_valor();
        $cuotas = array();
        $cuotas[0] = $cuota_anterior;
        $texto = " por los primeros " . $this->tipoAumentoALetras($this->getTipoAumento()) . " se abonará la suma de " . $this->getMontoTexto($cuota_anterior);
        for ($x = 0; $x < $iteraciones; $x++) {
            $cuota_anterior = ROUND($cuota_anterior + ($cuota_anterior * ($this->getPorcentajeAumento() / 100)));
            $cuotas[$x + 1] = $cuota_anterior;
            $texto.= ", los siguientes " . $this->tipoAumentoALetras($this->getTipoAumento()) . " se abonará la suma de " . $this->getMontoTexto($cuota_anterior);
        }
        return $texto;
    }

    function tipoAumentoALetras($tipo_aumento) {
        if ($tipo_aumento == "anual") {
            $texto = "(12) meses";
        } elseif ($tipo_aumento == "semestral") {
            $texto = "(6) meses";
        } elseif ($tipo_aumento == "cuatrimestral") {
            $texto = "(4) meses";
        } elseif ($tipo_aumento == "trimestral") {
            $texto = "(3) meses";
        } else {
            die('sin periodo definido 564 line contrato');
        }
        return $texto;
    }

    function getFechadePago() {
        return "del " . $this->getMontoTexto($this->getPago_primer_dia(), true) . " al " . $this->getMontoTexto($this->pago_ultimo_dia, true) . " de cada mes";
    }

    function getLugarDePago() {
        return $this->getLocador_direccion();
    }

    function getAnticipo() {
        return $this->getAlquiler_valor();
    }

    function getPrimerMes() {
        return $this->getFecha_inicio_contrato()->format("M");
    }

}
