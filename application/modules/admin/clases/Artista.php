<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Artista
 * 
 * @Table(name="artista") 
 * @Entity 
 */
class Artista {

    /**
     * @var integer $id
     *
     * @Column(name="art_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="art_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="art_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="art_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var string $nombre
     *
     * @Column(name="art_nombre", type="string",nullable=true)
     * 
     */
    protected $nombre;

    /**
     * @var string $foto_perfil
     *
     * @Column(name="art_imagen_perfil", type="string",nullable=true)
     * 
     */
    protected $foto_perfil;

    /**
     * @var string $apellido
     *
     * @Column(name="art_apellido", type="string",nullable=true)
     * 
     */
    protected $apellido;

    /**
     * @var string $telefono
     *
     * @Column(name="art_telefono", type="string",nullable=true)
     * 
     */
    protected $telefono;

    /**
     * @var string $email
     *
     * @Column(name="art_email", type="string",nullable=true)
     * 
     */
    protected $email;

    /**
     * @var string $descripcion
     *
     * @Column(name="art_descripcion", type="string",nullable=true)
     * 
     */
    protected $descripcion;

    /**
     * @var string $notas
     *
     * @Column(name="art_notas", type="string",nullable=true)
     * 
     */
    protected $notas;

    /**
     * @var Admin\Licencia $licencia
     * @OneToOne(targetEntity="Admin\Licencia")
     * @JoinColumn(name="art_licencia", referencedColumnName="lic_id")
     */
    private $licencia;

    /**
     * @var \Domicilio $domicilio
     * @OneToOne(targetEntity="\Domicilio")
     * @JoinColumn(name="art_domicilio", referencedColumnName="dom_id")
     */
    private $domicilio;

    /**
     * @var integer $disponible
     *
     * @Column(name="art_disponible", type="integer",nullable=true)
     * 
     */
    protected $disponible;

    /**
     * @var Admin\Cuentabanco[] $bancos
     * @OneToMany(targetEntity="Admin\Cuentabanco", mappedBy="artista")
     * @OrderBy({"id" = "DESC"})
     */
    private $bancos;

    function getFoto_perfil() {
        return $this->foto_perfil;
    }

    function getLinkFoto() {
        return base_url(FOTOS_ARTISTAS) . $this->getFoto_perfil();
    }

    function setFoto_perfil($foto_perfil) {
        $this->foto_perfil = $foto_perfil;
    }

    function __construct() {
        $this->bancos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function getBancos() {
        return $this->bancos;
    }

    function setBancos(array $bancos) {
        $this->bancos = $bancos;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getNombreCompleto(){
        return $this->getNombre().' '.$this->getApellido();
    }
    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function getNotas() {
        return $this->notas;
    }

    function setNotas($notas) {
        $this->notas = $notas;
    }

    function getLicencia() {
        return $this->licencia;
    }

    function setLicencia($licencia) {
        $this->licencia = $licencia;
    }

    function getDomicilio() {
        return $this->domicilio;
    }

    function setDomicilio($domicilio) {
        $this->domicilio = $domicilio;
    }

    function getBanco() {
        return $this->getBancos() ? $this->getBancos()[0] : null;
    }

    function getDisponible() {
        return $this->disponible;
    }

    function setDisponible($disponible) {
        $this->disponible = $disponible;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "nombre" => $this->getNombre(),
            "apellido" => $this->getApellido(),
            "telefono" => $this->getTelefono(),
            "email" => $this->getEmail(),
            "descripcion" => $this->getDescripcion(),
            "notas" => $this->getNotas(),
            "licencia" => $this->getLicencia()->getNombre(),
            "domicilio" => $this->getDomicilio()->getDireccion_completa(),
            "banco" => $this->getBanco()->getNombre(),
            "disponible" => $this->getDisponible(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-artista") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-artista/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

    function getLink() {
        return "#";
        $artista = str_replace(" ", "-", ($this->getNombre() . '-' . $this->getApellido()));
        return base_url("artista/" . $artista . "/" . $this->getId());
    }

}
