<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Compra_movimientos
 *
 * @Table(name="compra_movimientos")
 * @Entity
 */
class Compra_movimientos {

    /**
     * @var integer $id
     *
     * @Column(name="cmm_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Comprarenglon $compra_renglon
     * @ManyToOne(targetEntity="Comprarenglon", inversedBy="renglon")
     * @JoinColumn(name="cmm_cor_id", referencedColumnName="cor_id")
     * */
    protected $compra_renglon;

    /**
     * @var string $evento
     *
     * @Column(name="cmm_evento", type="string")
     *
     */
    protected $evento;

    /**
     * @var DateTime $fechaIn
     *
     * @Column(name="cmm_fec_in", type="datetime")
     *
     */
    protected $fechaIn;

    /**
     * Compra_movimientos constructor.
     * @param int $id
     * @param Compra $compra
     * @param string $evento
     * @param DateTime $fechaIn
     */
    public function __construct() {
        
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    function getCompra_renglon() {
        return $this->compra_renglon;
    }

    function setCompra_renglon(Comprarenglon $compra_renglon) {
        $this->compra_renglon = $compra_renglon;
    }

    /**
     * @return string
     */
    public function getEvento() {
        return $this->evento;
    }

    /**
     * @param string $evento
     */
    public function setEvento($evento) {
        $this->evento = $evento;
    }

    /**
     * @return DateTime
     */
    public function getFechaIn() {
        return $this->fechaIn;
    }

    /**
     * @param DateTime $fechaIn
     */
    public function setFechaIn($fechaIn) {
        $this->fechaIn = $fechaIn;
    }

    function getEventoMostrar() {
        return "Estado: " . mb_strtoupper($this->getEvento(), "utf8");
    }

}
