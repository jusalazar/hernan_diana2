<?php
namespace Admin;
defined("BASEPATH") OR exit("No direct script access allowed");


use Doctrine\Common\Collections\ArrayCollection;

/**
 * Contacto
 * 
 * @Table(name="contacto") 
 * @Entity 
 */
class Contacto {

    /**
     * @var integer $id
     *
     * @Column(name="con_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var boolean $activo
     *
     * @Column(name="con_activo", type="boolean")
     */
    private $activo;
    
    /**
     * @var datetime $created_at
     *
     * @Column(name="con_created_at", type="datetime",nullable=false)
     */
    private $created_at;
    
    /**
     * @var datetime $modified_at
     *
     * @Column(name="con_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

     /**
    * @var string $nombre
    *
    * @Column(name="con_nombre", type="string",nullable=true)
    * 
    */
    protected $nombre;
 /**
    * @var string $email
    *
    * @Column(name="con_email", type="string",nullable=true)
    * 
    */
    protected $email;
 /**
    * @var datetime $fecha_pedida
    *
    * @Column(name="con_fecha_pedida", type="datetime",nullable=true)
    * 
    */
    protected $fecha_pedida;
 /**
    * @var integer $cantidad_invitados
    *
    * @Column(name="con_cantidad_invitados", type="integer",nullable=true)
    * 
    */
    protected $cantidad_invitados;
 /**
    * @var string $ciudad
    *
    * @Column(name="con_ciudad", type="string",nullable=true)
    * 
    */
    protected $ciudad;
 /**
    * @var string $empresa
    *
    * @Column(name="con_empresa", type="string",nullable=true)
    * 
    */
    protected $empresa;
 /**
    * @var string $tipo
    *
    * @Column(name="con_tipo", type="string",nullable=true)
    * 
    */
    protected $tipo;

    
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setActivo($activo) {
        return $this->activo = $activo;
    }
    
    function getActivo() {
        return $this->activo;
    }
    
    function setCreated_at($created_at){
        $this->created_at = $created_at;
    }
    
    function getCreated_at() {
        return $this->created_at;
    }
    
    function setModified_at($modified_at){
        $this->modified_at = $modified_at;
    }
    
    function getModified_at() {
        return $this->modified_at;
    }

    function getNombre() {
    return $this->nombre;
}

function setNombre($nombre) {
    $this->nombre = $nombre;
}
function getEmail() {
    return $this->email;
}

function setEmail($email) {
    $this->email = $email;
}
function getFecha_pedida() {
    return $this->fecha_pedida;
}

function setFecha_pedida($fecha_pedida) {
    $this->fecha_pedida = $fecha_pedida;
}
function getCantidad_invitados() {
    return $this->cantidad_invitados;
}

function setCantidad_invitados($cantidad_invitados) {
    $this->cantidad_invitados = $cantidad_invitados;
}
function getCiudad() {
    return $this->ciudad;
}

function setCiudad($ciudad) {
    $this->ciudad = $ciudad;
}
function getEmpresa() {
    return $this->empresa;
}

function setEmpresa($empresa) {
    $this->empresa = $empresa;
}
function getTipo() {
    return $this->tipo;
}

function setTipo($tipo) {
    $this->tipo = $tipo;
}


    public function getDatosArray(){
                $array = array("id" => $this->getId(),
"nombre" => $this->getNombre(), 
"email" => $this->getEmail(), 
"fecha_pedida" => $this->getFecha_pedida()->format('Y-m-d'), 
"cantidad_invitados" => $this->getCantidad_invitados(), 
"ciudad" => $this->getCiudad(), 
"empresa" => $this->getEmpresa(), 
"tipo" => $this->getTipo(), 
"acciones" => $this->getAcciones());
                return $array;
                }
    
    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-contacto") .'/'. $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-contacto/borrar/" . $this->getId()) . '" title="'.($this->getActivo()?"Eliminar":"Recuperar").'">
            <i class="glyphicon glyphicon-'.($this->getActivo()?"trash":"check").'"></i>
            </a> ';
    }

}

