<?php
namespace Admin;
defined("BASEPATH") OR exit("No direct script access allowed");


use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cliente
 * 
 * @Table(name="cliente") 
 * @Entity 
 */
class Cliente {

    /**
     * @var integer $id
     *
     * @Column(name="cli_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var boolean $activo
     *
     * @Column(name="cli_activo", type="boolean")
     */
    private $activo;
    
    /**
     * @var datetime $created_at
     *
     * @Column(name="cli_created_at", type="datetime",nullable=false)
     */
    private $created_at;
    
    /**
     * @var datetime $modified_at
     *
     * @Column(name="cli_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

     /**
    * @var string $nombre
    *
    * @Column(name="cli_nombre", type="string",nullable=true)
    * 
    */
    protected $nombre;
 /**
    * @var string $telefono
    *
    * @Column(name="cli_telefono", type="string",nullable=true)
    * 
    */
    protected $telefono;
 /**
    * @var string $email
    *
    * @Column(name="cli_email", type="string",nullable=true)
    * 
    */
    protected $email;

    
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setActivo($activo) {
        return $this->activo = $activo;
    }
    
    function getActivo() {
        return $this->activo;
    }
    
    function setCreated_at($created_at){
        $this->created_at = $created_at;
    }
    
    function getCreated_at() {
        return $this->created_at;
    }
    
    function setModified_at($modified_at){
        $this->modified_at = $modified_at;
    }
    
    function getModified_at() {
        return $this->modified_at;
    }

    function getNombre() {
    return $this->nombre;
}

function setNombre($nombre) {
    $this->nombre = $nombre;
}
function getTelefono() {
    return $this->telefono;
}

function setTelefono($telefono) {
    $this->telefono = $telefono;
}
function getEmail() {
    return $this->email;
}

function setEmail($email) {
    $this->email = $email;
}


    public function getDatosArray(){
                $array = array("id" => $this->getId(),
"nombre" => $this->getNombre(), 
"telefono" => $this->getTelefono(), 
"email" => $this->getEmail(), 
"acciones" => $this->getAcciones());
                return $array;
                }
    
    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-cliente") .'/'. $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-cliente/borrar/" . $this->getId()) . '" title="'.($this->getActivo()?"Eliminar":"Recuperar").'">
            <i class="glyphicon glyphicon-'.($this->getActivo()?"trash":"check").'"></i>
            </a> ';
    }

}

