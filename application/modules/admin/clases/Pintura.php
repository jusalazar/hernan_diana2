<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Pintura
 * 
 * @Table(name="pintura") 
 * @Entity 
 */
class Pintura {

    /**
     * @var integer $id
     *
     * @Column(name="pin_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="pin_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="pin_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="pin_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var string $nombre
     *
     * @Column(name="pin_nombre", type="string",nullable=true)
     * 
     */
    protected $nombre;

    /**
     * @var string $imagen
     *
     * @Column(name="pin_imagen", type="string",nullable=true)
     * 
     */
    protected $imagen;

    /**
     * @var string $tiempo
     *
     * @Column(name="pin_tiempo", type="string",nullable=true)
     * 
     */
    protected $tiempo;

    /**
     * @var integer $pago
     *
     * @Column(name="pin_pago", type="integer",nullable=true)
     * 
     */
    protected $pago;

    /**
     * @var float $costo
     *
     * @Column(name="pin_costo", type="float",nullable=true)
     * 
     */
    protected $costo;

    /**
     * @var \Admin\Artista $artista
     * @OneToOne(targetEntity="Admin\Artista")
     * @JoinColumn(name="pin_artista", referencedColumnName="art_id")
     */
    private $artista;

    /**
     * @var Admin\Tipopintura $tipopintura
     * @OneToOne(targetEntity="Admin\Tipopintura")
     * @JoinColumn(name="pin_tipopintura", referencedColumnName="tip_id")
     */
    private $tipopintura;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getNombreCompleto() {
        return $this->getArtista()->getNombre() . ' - ' . $this->getNombre();
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function getImagen() {
        return $this->imagen;
    }

    function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    function getTiempo() {
        return $this->tiempo;
    }

    function setTiempo($tiempo) {
        $this->tiempo = $tiempo;
    }

    function getPago() {
        return $this->pago;
    }

    function setPago($pago) {
        $this->pago = $pago;
    }

    function getCosto() {
        return $this->costo;
    }

    function setCosto($costo) {
        $this->costo = $costo;
    }

    function getArtista() {
        return $this->artista;
    }

    function setArtista($artista) {
        $this->artista = $artista;
    }

    function getTipopintura() {
        return $this->tipopintura;
    }

    function setTipopintura($tipopintura) {
        $this->tipopintura = $tipopintura;
    }
    
    function getLinkImagen(){
        return base_url(FOTOS_PINTURAS).$this->getImagen();
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "nombre" => $this->getNombre(),
            "imagen" => '<img src="'.$this->getLinkImagen().'" width="60px"/>',
            "tiempo" => $this->getTiempo(),
            "pago" => $this->getPago(),
            "costo" => $this->getCosto(),
            "artista" => $this->getArtista()->getNombre(),
            "tipopintura" => $this->getTipopintura()->getNombre(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-pintura") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-pintura/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

}
