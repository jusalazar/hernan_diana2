<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Descuento
 * 
 * @Table(name="descuento") 
 * @Entity 
 */
class Descuento {

    /**
     * @var integer $id
     *
     * @Column(name="des_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="des_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="des_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="des_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var string $tipo
     *
     * @Column(name="des_tipo", type="string",nullable=true)
     * 
     */
    protected $tipo;

    /**
     * @var string $codigo
     *
     * @Column(name="des_codigo", type="string",nullable=true)
     * 
     */
    protected $codigo;

    /**
     * @var integer $descuento
     *
     * @Column(name="des_descuento", type="integer",nullable=true)
     * 
     */
    protected $descuento;

    /**
     * @var datetime $usado
     *
     * @Column(name="des_usado", type="datetime",nullable=true)
     * 
     */
    protected $usado;

    /**
     * @var float $importe
     *
     * @Column(name="des_importe", type="float",nullable=true)
     * 
     */
    protected $importe;

    function getCodigo() {
        return $this->codigo;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function getImporte() {
        return $this->importe;
    }

    function setImporte($importe) {
        $this->importe = $importe;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function getDescuento() {
        return $this->descuento;
    }

    function setDescuento($descuento) {
        $this->descuento = $descuento;
    }

    function getUsado() {
        return $this->usado;
    }

    function setUsado($usado) {
        $this->usado = $usado;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "tipo" => $this->getTipo(),
            "descuento" => $this->getDescuento(),
            "importe" => $this->getImporte(),
            "codigo" => $this->getCodigo(),
            "usado" => $this->getUsado() ? $this->getUsado()->format('Y-m-d') : "sin usar",
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-descuento") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-descuento/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

}
