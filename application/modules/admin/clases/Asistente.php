<?php

namespace Admin;

defined("BASEPATH") OR exit("No direct script access allowed");

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Asistente
 * 
 * @Table(name="asistente") 
 * @Entity 
 */
class Asistente {

    /**
     * @var integer $id
     *
     * @Column(name="asi_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean $activo
     *
     * @Column(name="asi_activo", type="boolean")
     */
    private $activo;

    /**
     * @var datetime $created_at
     *
     * @Column(name="asi_created_at", type="datetime",nullable=false)
     */
    private $created_at;

    /**
     * @var datetime $modified_at
     *
     * @Column(name="asi_modified_at", type="datetime",nullable=true)
     */
    private $modified_at;

    /**
     * @var string $nombre
     *
     * @Column(name="asi_nombre", type="string",nullable=true)
     * 
     */
    protected $nombre;

    /**
     * @var string $apellido
     *
     * @Column(name="asi_apellido", type="string",nullable=true)
     * 
     */
    protected $apellido;

    /**
     * @var string $telefono
     *
     * @Column(name="asi_telefono", type="string",nullable=true)
     * 
     */
    protected $telefono;

    /**
     * @var Admin\Empresa $empresa
     * @OneToOne(targetEntity="Admin\Empresa")
     * @JoinColumn(name="asi_empresa", referencedColumnName="emp_id")
     */
    private $empresa;

    /**
     * @var Admin\Licencia $licencia
     * @OneToOne(targetEntity="Admin\Licencia")
     * @JoinColumn(name="asi_licencia", referencedColumnName="lic_id")
     */
    private $licencia;

    /**
     * @var \Domicilio $domicilio
     * @OneToOne(targetEntity="\Domicilio")
     * @JoinColumn(name="asi_domicilio", referencedColumnName="dom_id")
     */
    private $domicilio;

    /**
     * @var integer $lunes
     *
     * @Column(name="asi_lunes", type="integer",nullable=true)
     * 
     */
    protected $lunes;

    /**
     * @var integer $martes
     *
     * @Column(name="asi_martes", type="integer",nullable=true)
     * 
     */
    protected $martes;

    /**
     * @var integer $miercoles
     *
     * @Column(name="asi_miercoles", type="integer",nullable=true)
     * 
     */
    protected $miercoles;

    /**
     * @var integer $jueves
     *
     * @Column(name="asi_jueves", type="integer",nullable=true)
     * 
     */
    protected $jueves;

    /**
     * @var integer $viernes
     *
     * @Column(name="asi_viernes", type="integer",nullable=true)
     * 
     */
    protected $viernes;

    /**
     * @var integer $sabado
     *
     * @Column(name="asi_sabado", type="integer",nullable=true)
     * 
     */
    protected $sabado;

    /**
     * @var integer $domingo
     *
     * @Column(name="asi_domingo", type="integer",nullable=true)
     * 
     */
    protected $domingo;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActivo($activo) {
        return $this->activo = $activo;
    }

    function getActivo() {
        return $this->activo;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function setModified_at($modified_at) {
        $this->modified_at = $modified_at;
    }

    function getModified_at() {
        return $this->modified_at;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function getEmpresa() {
        return $this->empresa;
    }

    function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }

    function getLicencia() {
        return $this->licencia;
    }

    function setLicencia($licencia) {
        $this->licencia = $licencia;
    }

    function getDomicilio() {
        return $this->domicilio;
    }

    function setDomicilio($domicilio) {
        $this->domicilio = $domicilio;
    }

    function getLunes() {
        return $this->lunes;
    }

    function setLunes($lunes) {
        $this->lunes = $lunes;
    }

    function getMartes() {
        return $this->martes;
    }

    function setMartes($martes) {
        $this->martes = $martes;
    }

    function getMiercoles() {
        return $this->miercoles;
    }

    function setMiercoles($miercoles) {
        $this->miercoles = $miercoles;
    }

    function getJueves() {
        return $this->jueves;
    }

    function setJueves($jueves) {
        $this->jueves = $jueves;
    }

    function getViernes() {
        return $this->viernes;
    }

    function setViernes($viernes) {
        $this->viernes = $viernes;
    }

    function getSabado() {
        return $this->sabado;
    }

    function setSabado($sabado) {
        $this->sabado = $sabado;
    }

    function getDomingo() {
        return $this->domingo;
    }

    function setDomingo($domingo) {
        $this->domingo = $domingo;
    }

    public function getDatosArray() {
        $array = array("id" => $this->getId(),
            "nombre" => $this->getNombre(),
            "apellido" => $this->getApellido(),
            "telefono" => $this->getTelefono(),
            "empresa" => $this->getEmpresa(),
            "licencia" => $this->getLicencia(),
            "domicilio" => $this->getDomicilio(),
            "lunes" => $this->getLunes(),
            "martes" => $this->getMartes(),
            "miercoles" => $this->getMiercoles(),
            "jueves" => $this->getJueves(),
            "viernes" => $this->getViernes(),
            "sabado" => $this->getSabado(),
            "domingo" => $this->getDomingo(),
            "acciones" => $this->getAcciones());
        return $array;
    }

    function getAcciones() {
        return '<a class="edit ml10" href="' . base_url("admin-asistente") . '/' . $this->getId() . '" title="Editar">
            <i class="fa fa-search"></i>
            </a> &nbsp;&nbsp;<a class="edit ml10" href="' . base_url("admin-asistente/borrar/" . $this->getId()) . '" title="' . ($this->getActivo() ? "Eliminar" : "Recuperar") . '">
            <i class="glyphicon glyphicon-' . ($this->getActivo() ? "trash" : "check") . '"></i>
            </a> ';
    }

}
