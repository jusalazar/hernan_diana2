<?php
namespace Admin;
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Contratoatributo
 *
 * @Table(name="contrato_atributo")
 * @Entity
 */
class Contratoatributo {

    /**
     * @var integer $id
     *
     * @Column(name="cna_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Admin\Contrato $contrato
     * @OneToOne(targetEntity="Admin\Contrato")
     * @JoinColumn(name="cna_contrato", referencedColumnName="con_id")
     */
    private $contrato;

    /**
     * @var Admin\Atributo $atributo
     * @OneToOne(targetEntity="Admin\Atributo")
     * @JoinColumn(name="cna_atributo", referencedColumnName="atr_id")
     */
    private $atributo;

    function getId() {
        return $this->id;
    }

    function getContrato() {
        return $this->contrato;
    }

    function getAtributo() {
        return $this->atributo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setContrato($contrato) {
        $this->contrato = $contrato;
    }

    function setAtributo($atributo) {
        $this->atributo = $atributo;
    }

}
