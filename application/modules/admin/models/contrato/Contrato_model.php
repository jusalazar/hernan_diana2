<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Contrato_model extends A_Model {
    /* @var $contrato Admin\Contrato */

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Contrato", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Contrato", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function getContratoAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.domicilio like :busqueda OR c.m2 like :busqueda OR c.estado_inmueble like :busqueda OR c.atr_tipo_propiedad like :busqueda OR c.descripcion like :busqueda OR c.email like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Contrato c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Contrato c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Contrato c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Contrato c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $contratos = $query->getResult();
        foreach ($contratos as $contrato) {
            $out [] = $contrato->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function insertarContratoAlquiler($information) {
        //alta de contrato
        if (isset($information["con_id"])) {
            $contrato = $this->getCollectionByid("Admin\Contrato", $information["con_id"]);
        } else {
            $contrato = new \Admin\Contrato();
            $contrato->setActivo(true);
            $contrato->setCreated_at(new \DateTime());
            $this->orm->persist($contrato);
        }

        $contrato->setEstado_inmueble($this->orm->getReference("\Admin\Atributo", $information["vivienda_estado"]));
        $contrato->setAtr_tipo_propiedad($this->orm->getReference("\Admin\Atributo", $information["vivienda_tipo"]));
        $contrato->setAtr_tipo_garantia($this->orm->getReference("\Admin\Atributo", $information["garantia_tipo"]));
        $contrato->setDescripcion($information["con_descripcion"]);
        $contrato->setM2($information["con_m2"]);

        //alta domicilio contrato vivienda
        if ($contrato->getDomicilio()) {
            $domicilio = $contrato->getDomicilio();
            $domicilio->actualizar($information["altura"], $information["barrio"], $information["calle"], $information["cityLat"]
                    , $information["cityLng"], $information["city2"], "", $information["cod_postal"], $information["pais"], true);
        } else {
            $domicilio = new Domicilio($information["altura"], $information["barrio"], $information["calle"], $information["cityLat"]
                    , $information["cityLng"], $information["city2"], "", $information["cod_postal"], $information["pais"], true);
            $this->orm->persist($domicilio);
        }
        $contrato->setDomicilio($domicilio);
        $this->orm->flush();
        $contrato->setAlquiler_tipo($information["con_tipo_pago"]);
        $contrato->setTipoAumento($information["con_tipo_pago"]);
        $contrato->setPorcentajeAumento($information["con_porcentaje_aumento"]);
        $contrato->setAlquiler_valor($information["con_monto"]);
        $contrato->setLocador_direccion($information["con_locador_direccion"]);
        $contrato->setLocador_telefono($information["con_locador_telefono"]);
        $contrato->setLocador_apellido($information["con_locador_apellido"]);
        $contrato->setLocador_nombre($information["con_locador_nombre"]);
        $contrato->setLocador_dni($information["con_locador_dni"]);
        $contrato->setLocatario_apellido($information["con_locatario_apellido"]);
        $contrato->setLocatario_nombre($information["con_locatario_nombre"]);
        $contrato->setLocatario_dni($information["con_locatario_dni"]);
        $contrato->setFiadordni($information["con_fiador_dni"]);
        $contrato->setFiadorApellido($information["con_fiador_apellido"]);
        $contrato->setFiadorNombre($information["con_fiador_nombre"]);
        $contrato->setFiadorSexo($information["con_fiador_sexo"]);
        $contrato->setFiadorDireccion($information["con_fiador_direccion"]);
        $contrato->setLocatario_direccion($information["con_locatario_direccion"]);
        $contrato->setFecha_inicio_contrato(new \DateTime(str_replace("/", "-", $information["con_fecha_inicio"])));
        $contrato->setDuracion_contrato($information["con_locacion_duracion"]);
        $contrato->setLocatario_sexo($information["con_locatario_sexo"]);
        $contrato->setLocador_sexo($information["con_locador_sexo"]);
        $contrato->setDestino_locacion($information["con_destino_locacion"]);
        $contrato->setPago_primer_dia($information["con_locacion_pago_intervalo_del"]);
        $contrato->setEmail($information["con_email"]);
        $contrato->setPago_ultimo_dia($information["con_locacion_pago_intervalo_al"]);
        $this->cargaAtributos($contrato, $information);
        $this->orm->flush();
        return $contrato;
    }

    function cargaAtributos(&$contrato, $information) {
        if ($contrato->getAtributos()) {
            foreach ($contrato->getAtributos() as $cada_atributo) {
                $this->orm->remove($cada_atributo);
            }
            $this->orm->flush();
        }
        if (isset($information["vivienda_atributos"])) {
            foreach ($information["vivienda_atributos"] as $atr_id => $cada_id_atributo) {
                $contrato_atributo = new Admin\Contratoatributo();
                $contrato_atributo->setAtributo($this->orm->getReference("Admin\Atributo", $atr_id));
                $contrato_atributo->setContrato($contrato);
                $this->orm->persist($contrato_atributo);
                $contrato->getAtributos()->add($contrato_atributo);
            }
            $this->orm->flush();
        }
    }

}
