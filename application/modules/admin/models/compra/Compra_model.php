<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Class Compra_model
 * @property Proveedor_model $proveedor
 * @property Usuario_model $usuario
 * @property Producto_model $producto
 * @property Alerta $alerta
 */
class Compra_model extends A_Model {
    /* @var $comprador Tools\Usuario */
    /* @var $compra Compra */

    public function __construct() {
        parent::__construct();
        $this->load->model("tools/usuario_model", "usuario");
    }

    function getProveedorProducto($var_id, $pro_id) {
        $str_sql = "SELECT p FROM Admin\Proveedorproducto p JOIN p.proveedor_producto pro JOIN p.producto_variacion pv WHERE pv.id=:var_id AND pro.id=:pro_id AND p.estado=1";
        $proveedor_producto_variacion = $this->orm->createQuery($str_sql)->setParameters(array("pro_id" => $pro_id, "var_id" => $var_id))->getResult();
        if ($proveedor_producto_variacion) {
            return $proveedor_producto_variacion[0];
        } else {
            return false;
        }
    }

    function insertar($information) {
        /* @var $evento Admin\Evento */
        $evento = $this->getCollectionByid("Admin\Evento", $information["eve_id"]);
        if (!$evento->getEntradasDisponibles($information["num-tickets"])) {
            die('no tiene ' . $information["cantidad"] . ' disponibles');
            return false;
        }
        $compra = new Compra();
        $compra->setFechaIn(new \DateTime());
        $compra->setCantidad($information["num-tickets"]);
        $compra->setComentarios(isset($information["comentarios"]) ? $information["comentarios"] : "");
        $compra->setComprador_nombre($information["nombre"]);
        $compra->setComprador_apellido($information["apellido"]);
        $compra->setComprador_email($information["email"]);
        $compra->setComprador_fb($information["fb"]);
        $compra->setPagada(false);
        $compra->setEliminada(false);
        $compra->setMedio_de_pago("mercado pago");
        $compra->setEvento($evento);
        $compra->setEs_tarjeta_regalo(0);
        if ($information["des_id"]) {
            $descuento = $this->getCollectionByid("Admin\Descuento", $information["des_id"]);
            $compra->setDescuento($descuento);
            $descuento->setUsado(new \DateTime());
        }
        /*if ($information["tar_id"]) {
            $tarjeta = $this->getCollectionByid("Admin\Descuento", $information["tar_id"]);
            $compra->setTarjeta_regalo($tarjeta);
            $tarjeta->setUsado(new \DateTime());
        }*/
        $compra->setPrecio_unitario($evento->getImporte());
        $this->orm->persist($compra);
        $this->orm->flush();
        return $compra->getId();
    }

    function compraTarjetaRegalo($information) {
        $tarjeta = $this->getTarjetaRegalo();
        $compra = new Compra();
        $compra->setFechaIn(new \DateTime());
        $compra->setCantidad($information["num-tickets"]);
        $compra->setComentarios(isset($information["comentarios"]) ? $information["comentarios"] : "");
        $compra->setComprador_nombre($information["nombre"]);
        $compra->setComprador_apellido($information["apellido"]);
        $compra->setComprador_email($information["email"]);
        $compra->setComprador_fb($information["fb"]);
        $compra->setPagada(false);
        $compra->setEliminada(false);
        $compra->setMedio_de_pago("mercado pago");
        $compra->setEs_tarjeta_regalo(1);
        $compra->setPrecio_unitario($tarjeta->getImporte());
        $this->orm->persist($compra);
        $this->orm->flush();
        return $compra->getId();
    }

    function avisaCompra(&$compra) {
        //$texto = "Hemos recibido su compra de " . $compra->getCantidad() . " tickets por un total de $" . $compra->getTotalCompra();
        $contenido = utf8_decode(file_get_contents(MAIL_TEMPLATE_PATH . "compra.html"));
        $contenido = str_replace('{SUBJECT}', "Nueva compra #" . $compra->getId(), $contenido);
        $contenido = str_replace('{PINTURA_SRC}', $compra->getEvento()->getPintura()->getLinkImagen(), $contenido);
        $contenido = str_replace('{TITULO_PINTURA}', $compra->getEvento()->getPintura()->getNombre(), $contenido);
        $contenido = str_replace('{CODIGO_COMPRA}', $compra->getCodigo(), $contenido);
        $contenido = str_replace('{DESCRIPCION_PINTURA}', $compra->getEvento()->getPintura()->getArtista()->getNombreCompleto(), $contenido);
        $contenido = str_replace('{DIRECCION}', $compra->getEvento()->getLugar()->getDomicilio()->getDireccion_completa(), $contenido);
        $contenido = str_replace('{TOTAL}', $compra->getTotalCompra(), $contenido);
        $contenido = str_replace('{FECHA}', $compra->getEvento()->getFecha_evento()->format("d/m/Y H:i"), $contenido);
        $this->mandaMail($compra->getComprador_email(), "Gracias por su compra", $contenido);
    }

    function avisaCompraTarjetaRegalo(&$compra) {
        $contenido = utf8_decode(file_get_contents(MAIL_TEMPLATE_PATH . "tarjeta_regalo.html"));
        $contenido = str_replace('{SUBJECT}', "Nueva compra #" . $compra->getId(), $contenido);
        $contenido = str_replace('{CODIGO_COMPRA}', $compra->getCodigo(), $contenido);
        $contenido = str_replace('{TOTAL}', $compra->getTotalCompra(), $contenido);
        $contenido = str_replace('{FECHA}', $compra->getFechaIn()->format("d/m/Y H:i"), $contenido);
        $this->mandaMail($compra->getComprador_email(), "Gracias por su compra", $contenido);
    }

    function reenviaEmailCompra($information) {
        $compra = $this->orm->createQuery("SELECT c FROM \Compra c WHERE c.comprador_email=:email")->setParameter("email", $information["email"])->setMaxResults(1)->getResult();
        if ($compra && $compra[0]->getFecha_pago()) {
            $this->avisaCompra($compra[0]);
            return true;
        }
        return false;
    }

    public function getComprasAjax($information) {
        $out = array();
        if (isset($information["limit"])) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if (isset($information["offset"])) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        if (isset($information["order"])) {
            switch ($information["order"]) {
                case "asc" :
                    $orden = "ASC";
                    break;
                case "desc" :
                    $orden = "DESC";
                    break;
            }
        }

        $sort = "c.id";

        if (isset($information["sort"])) {
            switch ($information["order"]) {
                case 'com_estado' :
                    $sort = "c.estado";
                    break;
                case 'com_cliente' :
                    $sort = "c.comprador";
                    break;
                case 'com_id' :
                    $sort = "c.id";
                    break;
                case 'com_medio_pago' :
                    $sort = "c.tipo_pago";
                    break;
                case 'com_fecha_entrega' :
                    $sort = "r.fecha_pedida";
                    break;
            }
        }


        $where = "WHERE c.eliminada=0";
        if (isset($information["pagado"])) {
            $where .= " AND c.pagada=" . (int) $information["pagado"];
        }
        if (isset($information["evento"]) && $information["evento"]) {
            $where .= " AND c.evento=" . (int) $information["evento"];
        }
        $joins = "";
        if (isset($information["search"])) {
            $where .= " AND pf.nombre like :busqueda OR pf.apellido like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Compra c $joins $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Compra c $joins $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Compra c $joins $where ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Compra c $joins $where")->setMaxResults(1)->getSingleScalarResult();
        }
        $compras = $query->getResult();
        foreach ($compras as $compra) {
            $out [] = $compra->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function borrarPedido($com_id) {
        /* @var $compra Compra */
        try {
            $compra = $this->getCollectionByid("Compra", $com_id);
            $compra->setEliminada(true);
            $this->orm->flush();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    function getCompraById($com_id) {
        return $this->getCollectionByid("Compra", $com_id);
    }

    function mpObject($sandbox = false) {
        $mp = new MP(CLIENT_ID, CLIENT_TOKEN);
        $mp->sandbox_mode($sandbox);
        return $mp;
    }

    function getMpPreferences(&$compra) {
        $sandbox = false;
        $mp = $this->mpObject($sandbox);
        $out = array();
        $url_compra = base_url("compra-pagada/" . $compra->getId());
        $array_renglon["title"] = $compra->getEvento() ? $compra->getEvento()->getTitulo() : "Tarjeta de regalo";
        $array_renglon["quantity"] = $compra->getCantidad();
        $array_renglon["currency_id"] = "ARS";
        $array_renglon["unit_price"] = $compra->getPrecio_unitario();
        $out["items"][] = $array_renglon;
        $out["back_urls"] = array("success" => $url_compra . '?e=' . MP_ESTADO_SUCCESS, "failure" => $url_compra . '?e=' . MP_ESTADO_REJECTED, "pending" => $url_compra . '?e=' . MP_ESTADO_PENDING);
        $out["auto_return"] = "approved";
        $out["merchant_order_id"] = $compra->getId();
        $preference = $mp->create_preference($out);
        return $preference;
    }

    function evaluaPagoMp(&$compra) {
        $estado = false;
        if ($compra->getFecha_pago()) {
            $estado = true;
        } else {
            if ($this->input->get("e") == MP_ESTADO_SUCCESS) {
                $this->mpMarcaCompraPaga($compra, $this->input->get("collection_id"), MP_ESTADO_SUCCESS);
                if (!$compra->getEs_tarjeta_regalo()) {
                    $this->avisaCompra($compra);
                } else {
                    $this->avisaCompraTarjetaRegalo($compra);
                }
                $estado = true;
            }
        }
        return $estado;
    }

    function mpMarcaCompraPaga(&$compra, $payment_id, $status) {
        if ($status == "approved") {
            $compra->setFecha_pago(new \DateTime());
            $compra->setPagada(true);
            $compra->setMp_payment_id($payment_id);
            $this->orm->flush();
        }
    }

    function getCompraWebById($com_id) {
        return $this->orm->createQuery("SELECT c FROM \Compra c WHERE c.id=:com_id")->setParameter("com_id", $com_id)->getSingleResult();
    }

    function getTarjetaRegalo() {
        return $this->orm->createQuery("SELECT d FROM Admin\Descuento d WHERE d.tipo='tarjeta regalo'")->getSingleResult();
    }

}
