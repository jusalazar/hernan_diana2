<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Descuento_model extends A_Model {
    /* @var $descuento Admin\Descuento */

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Descuento", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Descuento", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertarTarjetaRegalo($information) {
        if ($information["des_id"]) {
            $descuento = $this->getCollectionByid("Admin\Descuento", $information["des_id"]);
            if($descuento->getUsado()){
                return false;
            }
        } else {
            $descuento = new Admin\Descuento();
            $descuento->setCreated_at(new \DateTime());
            $descuento->setActivo(1);
            $this->orm->persist($descuento);
        }
        $descuento->setTipo("tarjeta regalo");
        $descuento->setDescuento(0);
        $descuento->setCodigo($information["des_codigo"]);
        $descuento->setImporte($information["des_importe"]);
        $descuento->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["des_id"]) {
            $descuento = $this->getCollectionByid("Admin\Descuento", $information["des_id"]);
            if($descuento->getUsado()){
                return false;
            }
        } else {
            $descuento = new Admin\Descuento();
            $descuento->setCreated_at(new \DateTime());
            $descuento->setActivo(1);
            $this->orm->persist($descuento);
        }
        $descuento->setTipo("descuento");
        $descuento->setCodigo($information["des_codigo"]);
        $descuento->setDescuento($information["des_descuento"]);
        $descuento->setImporte($information["des_importe"]);
        $descuento->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getDescuentoAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }
        $where = " WHERE c.tipo <> 'tarjeta regalo'";
        if (isset($information["search"])) {
            $where = " AND c.tipo like :busqueda OR c.descuento like :busqueda OR c.usado like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Descuento c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Descuento c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Descuento c $where ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Descuento c $where")->setMaxResults(1)->getSingleScalarResult();
        }
        $descuentos = $query->getResult();
        foreach ($descuentos as $descuento) {
            $out [] = $descuento->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function checkDescuentoWeb($information) {
        
               
        $descuento = $this->orm->createQuery("SELECT d FROM Admin\Descuento d WHERE d.tipo <> 'tarjeta regalo' AND d.codigo=:des_id")->setParameter("des_id", $information["descuento"])->getResult();
        if ($descuento && !$descuento[0]->getUsado()) {
            $response["des_id"] = $descuento[0]->getId();
            $response["descuento"] = $descuento[0]->getDescuento();
            $response["importe"] = $descuento[0]->getImporte();
            return $response;
        }
        return 0;
    }
    
    function checkTarjetaWeb($information){
        $descuento = $this->orm->createQuery("SELECT d FROM Admin\Descuento d WHERE d.tipo='tarjeta regalo' AND d.codigo=:des_id")->setParameter("des_id", $information["tarjeta"])->getResult();
        if ($descuento && !$descuento[0]->getUsado()) {
            $response["des_id"] = $descuento[0]->getId();
            $response["descuento"] = $descuento[0]->getDescuento();
            $response["importe"] = $descuento[0]->getImporte();
            return $response;
        }
        return 0;
    }

}
