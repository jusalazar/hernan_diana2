<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Asistente_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Asistente", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Asistente", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["asi_id"]) {
            $asistente = $this->getCollectionByid("Admin\Asistente", $information["asi_id"]);
            $domicilio = $asistente->getDomicilio();
            $domicilio->actualizar($this->post("altura"), $this->post("barrio"), $this->post("calle"), $this->post("cityLat"), $this->post("cityLng"), $this->post("ciudad"), $this->post("piso"), $this->post("depto"), $this->post("pais"), 1);
        } else {
            $asistente = new Admin\Asistente();
            $asistente->setCreated_at(new \DateTime());
            $this->orm->persist($asistente);
            $domicilio = new Domicilio($this->post("altura"), $this->post("barrio"), $this->post("calle"), $this->post("cityLat"), $this->post("cityLng"), $this->post("ciudad"), $this->post("piso"), $this->post("depto"), $this->post("pais"), 1);
            $this->orm->persist($domicilio);
        }
        $asistente->setNombre($information["asi_nombre"]);
        $asistente->setApellido($information["asi_apellido"]);
        $asistente->setTelefono($information["asi_telefono"]);
        $asistente->setEmpresa($this->orm->getReference("Admin\Empresa", $information["asi_empresa"]));
        $asistente->setLicencia($this->orm->getReference("Admin\Licencia", $information["asi_licencia"]));
        $asistente->setDomicilio($domicilio);
        $asistente->setLunes(isset($information["asi_lunes"]) ? 1 : 0);
        $asistente->setMartes(isset($information["asi_martes"]) ? 1 : 0);
        $asistente->setMiercoles(isset($information["asi_miercoles"]) ? 1 : 0);
        $asistente->setJueves(isset($information["asi_jueves"]) ? 1 : 0);
        $asistente->setViernes(isset($information["asi_viernes"]) ? 1 : 0);
        $asistente->setSabado(isset($information["asi_sabado"]) ? 1 : 0);
        $asistente->setDomingo(isset($information["asi_domingo"]) ? 1 : 0);
        $asistente->setActivo(isset($information["asi_activo"]) && !$information["asi_activo"] ? 0 : 1);
        $asistente->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getAsistenteAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda OR c.apellido like :busqueda OR c.telefono like :busqueda OR c.empresa like :busqueda OR c.licencia like :busqueda OR c.domicilio like :busqueda OR c.lunes like :busqueda OR c.martes like :busqueda OR c.miercoles like :busqueda OR c.jueves like :busqueda OR c.viernes like :busqueda OR c.sabado like :busqueda OR c.domingo like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Asistente c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Asistente c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Asistente c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Asistente c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $asistentes = $query->getResult();
        foreach ($asistentes as $asistente) {
            $out [] = $asistente->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getAsistentesActivos() {
        return $this->orm->createQuery("SELECT a FROM Admin\Asistente a WHERE a.activo=1")->getResult();
    }

}
