<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Contacto_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Contacto", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Contacto", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["con_id"]) {
            $contacto = $this->getCollectionByid("Admin\Contacto", $information["con_id"]);
        } else {
            $contacto = new Admin\Contacto();
            $contacto->setCreated_at(new \DateTime());
            $contacto->setActivo(1);
            $this->orm->persist($contacto);
        }
        $contacto->setNombre($information["con_nombre"]);
        $contacto->setEmail($information["con_email"]);
        $contacto->setFecha_pedida(new \DateTime($information["con_fecha_pedida"]));
        $contacto->setCantidad_invitados($information["con_cantidad_invitados"]);
        $contacto->setCiudad($information["con_ciudad"]);
        $contacto->setEmpresa($information["con_empresa"]);
        $contacto->setTipo($information["con_tipo"]);
        $contacto->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getContactoAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda OR c.email like :busqueda OR c.fecha_pedida like :busqueda OR c.cantidad_invitados like :busqueda OR c.ciudad like :busqueda OR c.empresa like :busqueda OR c.tipo like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Contacto c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Contacto c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Contacto c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Contacto c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $contactos = $query->getResult();
        foreach ($contactos as $contacto) {
            $out [] = $contacto->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function insertaWeb($information, $tipo) {
        try {
            $contacto = new Admin\Contacto();
            $contacto->setCreated_at(new \DateTime());
            $contacto->setActivo(1);

            $contacto->setNombre($information["name"]);
            $contacto->setEmail($information["email"]);
            $fecha_pedida = new \DateTime(str_replace("/", "-", $information["event_date"]));
            $contacto->setFecha_pedida($fecha_pedida);
            $contacto->setCantidad_invitados($information["capacity"]);
            $contacto->setCiudad($information["city"]);
            if ($tipo == "empresas") {
                $contacto->setEmpresa($information["company"]);
            }
            $contacto->setTipo($tipo);
            $contacto->setModified_at(new \DateTime());
            $this->orm->persist($contacto);
            $this->orm->flush();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

}
