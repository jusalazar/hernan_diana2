<?php

use Carbon\Carbon;
use Tools\Mensaje;
use Tools\Permiso;

/**
 * Description of General_model
 * @author mato
 *
 *
 */
class Permisos_model extends A_model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     * @return Permiso
     */
    function getPermiso($id = false)
    {
        if ($id === FALSE) {
            $dql = "SELECT p FROM Tools\Permiso p";
            $permisos = $this->orm->createQuery($dql)->getResult();
            return $permisos;
        }
        $permiso = $this->orm->find("Tools\Permiso", $id);
        return $permiso;
    }

    /**
     * @param bool $id
     * @return Permiso
     */
    function getPermisos($id = false)
    {
        return $this->getPermiso($id);
    }

    public function store($args)
    {
        $permiso = new Permiso();
        try {
            $permiso->setear(null, $args['nombre'], $args['descripcion'], Carbon::now());
            $this->persistir($permiso);
            return Mensaje::getExito("El Permiso fue creado");
        } catch (Exception $e) {
            return Mensaje::getError("El Permiso no pudo ser creado");
        }
    }

    public function update($id, $args)
    {
        try {
            $permiso = $this->getPermiso($id);
            $permiso->setNombre($args['nombre']);
            $permiso->setDescripcion($args['descripcion']);
            $this->persistir($permiso);
            return Mensaje::getExito("Permiso editado con éxito");
        } catch (Exception $e) {
            return Mensaje::getError("Permiso pudo ser editadxxo", $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $permiso = $this->orm->getReference("Tools\Permiso", $id);
            $this->orm->remove($permiso);
            $this->orm->flush();
            return Mensaje::getExito("El Permiso borrado");
        } catch (Exception $e) {
            return Mensaje::getError("El Permiso no pudo ser borrado" ,$e->getMessage());
        }
    }
}