<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Tipopintura_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Tipopintura", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Tipopintura", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["tip_id"]) {
            $tipopintura = $this->getCollectionByid("Admin\Tipopintura", $information["tip_id"]);
        } else {
            $tipopintura = new Admin\Tipopintura();
            $tipopintura->setCreated_at(new \DateTime());
            $tipopintura->setActivo(true);
            $this->orm->persist($tipopintura);
        }
        $tipopintura->setNombre($information["tip_nombre"]);

        $tipopintura->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getTipopinturaAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Tipopintura c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Tipopintura c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Tipopintura c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Tipopintura c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $tipopinturas = $query->getResult();
        foreach ($tipopinturas as $tipopintura) {
            $out [] = $tipopintura->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getTiposActivos() {
        return $this->orm->createQuery("SELECT t FROM Admin\Tipopintura t WHERE t.activo=1")->getResult();
    }

}
