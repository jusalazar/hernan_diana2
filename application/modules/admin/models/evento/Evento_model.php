<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Evento_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Evento", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Evento", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["eve_id"]) {
            $evento = $this->getCollectionByid("Admin\Evento", $information["eve_id"]);
        } else {
            $evento = new Admin\Evento();
            $evento->setCreated_at(new \DateTime());
            $evento->setActivo(1);
            $this->orm->persist($evento);
        }
        $evento->setTitulo($information["eve_titulo"]);
        $evento->setFecha_evento(new \DateTime($information["eve_fecha_evento"]));
        $evento->setCapacidad($information["eve_capacidad"]);
        $evento->setDescripcion($information["eve_descripcion"]);
        $evento->setNotas($information["eve_notas"]);
        $evento->setImporte((double) $information["eve_importe"]);
        $evento->setFee((double) $information["eve_fee"]);
        $evento->setRecaudador_nombre($information["eve_recaudador_nombre"]);
        $evento->setRecaudador_email($information["eve_recaudador_email"]);
        $evento->setRecaudador_codigo($information["eve_recaudador_codigo"]);
        $evento->setRecaudador_porcentaje((double) $information["eve_recaudador_porcentaje"]);
        $evento->setEmpresa($this->orm->getReference("Admin\Empresa", $information["eve_empresa"]));
        $evento->setAsistente($this->orm->getReference("Admin\Asistente", $information["eve_asistente"]));
        $evento->setLugar($this->orm->getReference("Admin\Lugar", $information["eve_lugar"]));
        $evento->setPintura($this->orm->getReference("Admin\Pintura", $information["eve_pintura"]));
        $evento->setArtista($this->orm->getReference("Admin\Artista", $information["eve_artista"]));
        $evento->setForma_pago($information["eve_forma_pago"]);
        $evento->setMoneda($information["eve_moneda"]);
        $evento->setRecaudador_caridad(isset($information["eve_recaudador_caridad"]) ? 1 : 0);

        $evento->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getEventoAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.titulo like :busqueda OR c.fecha_evento like :busqueda OR c.capacidad like :busqueda OR c.descripcion like :busqueda OR c.notas like :busqueda OR c.domicilio like :busqueda OR c.importe like :busqueda OR c.fee like :busqueda OR c.recaudador_nombre like :busqueda OR c.recaudador_email like :busqueda OR c.recaudador_codigo like :busqueda OR c.recaudador_porcentaje like :busqueda OR c.empresa like :busqueda OR c.asistente like :busqueda OR c.lugar like :busqueda OR c.artista like :busqueda OR c.pintura like :busqueda OR c.forma_pago like :busqueda OR c.moneda like :busqueda OR c.recaudador_caridad like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Evento c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Evento c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Evento c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Evento c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $eventos = $query->getResult();
        foreach ($eventos as $evento) {
            $out [] = $evento->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getEventosActivos($pagina = 0) {
        $offset = $pagina * MAX_RESULT_BUSQUEDA;
        $where = "WHERE e.activo=1 AND e.fecha_evento>=CURRENT_TIMESTAMP()";
        if ($this->input->get("from") && $this->input->get("to")) {
            $params["desde"] = new \DateTime((str_replace("/", "-", $this->input->get("from"))));
            $params["hasta"] = new \DateTime((str_replace("/", "-", $this->input->get("to"))));
            $and = " AND e.fecha_evento BETWEEN :desde AND :hasta";
            $response["resultados"] = $this->orm->createQuery("SELECT e FROM Admin\Evento e $where $and ORDER BY e.titulo ASC")
                            ->setParameters($params)->setFirstResult($offset)->setMaxResults(MAX_RESULT_BUSQUEDA)->getResult();
            $response["total"] = $this->orm->createQuery("SELECT count(e) cant FROM Admin\Evento e $where $and ORDER BY e.titulo ASC")
                            ->setParameters($params)->getSingleResult();
        } else {
            $response["resultados"] = $this->orm->createQuery("SELECT e FROM Admin\Evento e $where ORDER BY e.titulo ASC")->setFirstResult($offset)->setMaxResults(MAX_RESULT_BUSQUEDA)->getResult();
            $response["total"] = $this->orm->createQuery("SELECT count(e) cant FROM Admin\Evento e $where ORDER BY e.titulo ASC")->getSingleResult();
        }

        return $response;
    }

    public function getCiudadesSelect2($args) {
        /* @var $lugar Admin\Lugar */
        $nombre = strtolower(trim($args['q']['term'], 'utf-8'));
        $query = $this->orm->createQuery("SELECT l FROM Admin\Lugar l JOIN l.domicilio d WHERE d.ciudad like :busqueda ORDER BY d.ciudad ASC");
        $ciudades = $query->setParameter("busqueda", "%" . $nombre . "%")->getResult();

        $data = array();
        if ($ciudades) {
            foreach ($ciudades as $lugar) {
                $data[] = array(
                    "domicilio" => $lugar->getDomicilio()->getCiudad(),
                    "id" => $lugar->getId());
            }
        } else {
            $data[] = array("id" => "0", "text" => "No se encontraron resultados..");
        }
        $ret['results'] = $data;
        return $ret;
    }

    function getCiudadesDisponibles() {
        return $this->orm->createQuery("SELECT l FROM Admin\Lugar l JOIN l.domicilio d ORDER BY d.ciudad ASC")->getResult();
    }

    function getAllEventos() {
        return $this->orm->createQuery("SELECT e FROM Admin\Evento e")->getResult();
    }

}
