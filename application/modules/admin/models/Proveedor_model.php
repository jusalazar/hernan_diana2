<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Proveedor_model extends A_Model {
    /* @var $proveedor Proveedor */
    /* @var $domicilio Domicilio */
    /* @var $punto_venta Punto_venta */

    public function __construct() {
        parent::__construct();
    }

    function getPuntoVenta($puv_id) {
        return $this->orm->createQuery("SELECT p FROM \Punto_venta p WHERE p.id=:puv_id")->setParameter("puv_id", $puv_id)->getSingleResult();
    }

    function getPuntosVentaActivos() {
        return $this->orm->createQuery("SELECT p FROM \Punto_venta p WHERE p.activo<>0")->getResult();
    }

    function misDatos($information) {
        if ($information["puv_id"]) {
            $punto_venta = $this->getCollectionByid("\Punto_venta", $information["puv_id"]);
        } else {
            $punto_venta = new \Punto_venta();
            $punto_venta->setFechaIn(new \DateTime());
            $punto_venta->setActivo(true);
            $this->orm->persist($punto_venta);
        }
        $punto_venta->setNombre($information["nombre"]);
        $punto_venta->setTelefono($information["telefono"]);
        $punto_venta->setEmail($information["email"]);
        $this->orm->flush();
    }

    function getById($pro_id) {
        $proveedor = $this->orm->createQuery("SELECT p FROM Proveedor p JOIN p.usuario u WHERE u.id=:busqueda")->setParameter("busqueda", $pro_id)->getSingleResult();
        return $proveedor;
    }

    function getDashboard() {
        $response["mas_ingresos"] = $this->getProductosConMayorIngreso();
        $response["mas_vendidos"] = $this->getProductosMasVendidos();
        return $response;
    }

    function getProductosConMayorIngreso() {
        $query = "select CONCAT(producto.pro_nombre,' ',var_ume,' ',var_cantidad) nombre,pro_foto imagen,MAX(prv_precio_real) precio,SUM(cor_precio*cor_cantidad) total_vendido
            from compra
           inner join compra_renglon on cor_compra=com_id
           inner join proveedor_producto_variacion on cor_proveedor_variacion=ppv_id
           inner join proveedor on pro_id=ppv_proveedor
           inner join usuarios on id=pro_usu_id
           inner join producto_variacion on prv_id=ppv_producto_variacion
           inner join variacion on var_id=prv_variacion
           inner join producto producto on producto.pro_id=prv_producto
           where id=" . get_user_id() . "
           group by producto.pro_nombre,pro_foto,var_ume,var_cantidad
           order by total_vendido desc
           limit 10";
        return $this->db->query($query)->result();
    }

    function getProductosMasVendidos() {
        $query = "select CONCAT(producto.pro_nombre,' ',var_ume,' ',var_cantidad) nombre,pro_foto imagen,MAX(prv_precio_real) precio,SUM(cor_cantidad) unidades_totales,SUM(cor_precio*cor_cantidad) total_vendido
            from compra
           inner join compra_renglon on cor_compra=com_id
           inner join proveedor_producto_variacion on cor_proveedor_variacion=ppv_id
           inner join proveedor on pro_id=ppv_proveedor
           inner join usuarios on id=pro_usu_id
           inner join producto_variacion on prv_id=ppv_producto_variacion
           inner join variacion on var_id=prv_variacion
           inner join producto producto on producto.pro_id=prv_producto
           where id=" . get_user_id() . "
           group by producto.pro_nombre,pro_foto,var_ume,var_cantidad
           order by unidades_totales desc
           limit 10";
        return $this->db->query($query)->result();
    }

    function getProveedoresAdmin($information) {
        $limit = $information["limit"] ? $information["limit"] : 20;
        $offset = $information["offset"] ? $information["offset"] : 0;

        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "p.id";
        if (isset($information["sort"])) {
            switch ($information["order"]) {
                
            }
        }

        $where = "WHERE 1=1";

        if (isset($information["search"])) {
            $where .= " AND p.nombre like :busqueda OR p.telefono like :busqueda";
            $query = $this->orm->createQuery("SELECT p FROM Proveedor p $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(p) FROM Proveedor p")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT p FROM Proveedor p $where ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(p) FROM Proveedor p $where")->setMaxResults(1)->getSingleScalarResult();
        }
        $proveedores = $query->getResult();
        $out = array();
        foreach ($proveedores as $proveedor) {
            $out [] = $proveedor->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }
            
    function eliminarProveedor($information) {
        $proveedor = $this->getCollectionByid("Proveedor", $information["pro_id"]);
        $proveedor->setActivo($proveedor->getActivo() ? false : true);
        $this->orm->flush();
    }

    function enviaContacto($information) {
        $rol = $this->orm->getReference("Tools\Rol", ROL_VENDEDOR_ID);
        $usuario = new \Tools\Usuario();
        $pass = crearPassword();
        $usuario->setPassword(password_hash($pass, PASSWORD_DEFAULT));
        $roles[] = $rol;
        $usuario->setRoles($roles);
        $usuario->setEmail($information["contacto_email"]);
        $usuario->setFuerza_cambio_clave(true);
        $this->persistir($usuario);

        //alta domiciilio si viene
        if ($this->post("contacto_direccion")) {
            $domicilio = new Domicilio();
            $domicilio->setAltura($this->post("contacto_altura"));
            $domicilio->setBarrio($this->post("contacto_barrio"));
            $domicilio->setCalle($this->post("contacto_calle"));
            $domicilio->setLatitud($this->post("contacto_latitud"));
            $domicilio->setLongitud($this->post("contacto_longitud"));
            $domicilio->setCiudad($this->post("contacto_ciudad"));
            $domicilio->setPiso("");
            $domicilio->setPais($this->post("contacto_pais"));
            $domicilio->setActivo(true);
            $domicilio->setPreferente(true);
            $domicilio->setDireccion_completa($this->post("contacto_direccion"));
            $domicilio->setUsuario($usuario);
            $this->orm->persist($domicilio);
            $this->orm->flush();
        }
        $proveedor = new Proveedor();
        $proveedor->setUsuario($usuario);
        $proveedor->setFechaIn(new \DateTime());
        $proveedor->setAutorizacion(false);
        $proveedor->setActivo(false);
        $proveedor->setNombre($this->post("contacto_nombre"));
        $proveedor->setEmail($usuario->getEmail());
        $this->persistir($proveedor);

        $punto_venta = new Punto_venta();
        $this->orm->persist($punto_venta);
        $punto_venta->setDomicilio($domicilio);
        $punto_venta->setProveedor($proveedor);
        $punto_venta->setHace_delivery(1);
        $punto_venta->setCosto_envio(0.00);
        $punto_venta->setRadio_entrega(0);
        $punto_venta->setTiempo_entrega_estimado(TIEMPO_ENTREGA_TOLERANCIA);
        $punto_venta->setNombre($this->post("contacto_nombre"));
        $punto_venta->setTelefono($this->post("contacto_telefono"));
        $punto_venta->setActivo(true);
        $punto_venta->setEmail($usuario->getEmail());
        $this->orm->flush();

        $perfil = new Tools\Perfil();
        $perfil->setUsuario($usuario);
        $perfil->setNombre($this->post("contacto_nombre"));
        $perfil->setApellido($this->post("contacto_apellido"));
        $perfil->setTelefono($this->post("contacto_telefono"));
        $perfil->setEmail($usuario->getEmail());
        $this->persistir($perfil);
        $this->orm->flush();
    }

    function getProveedoresActivos() {
        return $this->orm->createQuery("SELECT p FROM \Proveedor p WHERE p.activo=1")->getResult();
    }

}
