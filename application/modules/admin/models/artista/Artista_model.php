<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Artista_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Artista", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Artista", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["art_id"]) {
            $artista = $this->getCollectionByid("Admin\Artista", $information["art_id"]);
            $domicilio = $artista->getDomicilio();
            $domicilio->actualizar($this->post("altura"), $this->post("barrio"), $this->post("calle"), $this->post("cityLat"), $this->post("cityLng"), $this->post("ciudad"), $this->post("piso"), $this->post("depto"), $this->post("pais"), 1);
        } else {
            $artista = new Admin\Artista();
            $artista->setCreated_at(new \DateTime());
            $artista->setActivo(true);
            $this->orm->persist($artista);
            $domicilio = new Domicilio($this->post("altura"), $this->post("barrio"), $this->post("calle"), $this->post("cityLat"), $this->post("cityLng"), $this->post("ciudad"), $this->post("piso"), $this->post("depto"), $this->post("pais"), 1);
            $this->orm->persist($domicilio);
        }
        $artista->setNombre($information["art_nombre"]);
        $artista->setApellido($information["art_apellido"]);
        $artista->setTelefono($information["art_telefono"]);
        $artista->setEmail($information["art_email"]);
        $artista->setDescripcion($information["art_descripcion"]);
        $artista->setNotas($information["art_notas"]);
        $artista->setLicencia($this->orm->getReference("Admin\Licencia", $information["art_licencia"]));
        $artista->setDisponible(isset($information["art_disponible"]) ? 1 : 0);
        $artista->setDomicilio($domicilio);
        $this->cargaFotoPerfil($artista);
        $banco = $this->orm->getReference("Admin\Banco", $information["cue_banco"]);
        $cuenta_banco = new Admin\Cuentabanco($artista, true, $information["cue_cbu"], $information["cue_cbu_alias"], $information["benefio_nro"], $information["cue_cuit_titular"], $information["cue_numero"], $information["cue_tipo_cuenta"], $banco, $information["cue_nombre_titular"]);
        $this->orm->persist($cuenta_banco);
        $artista->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function cargaFotoPerfil(&$artista, $array_borrar = false) {

        if (isset($_FILES['foto']) && $_FILES['foto']['name'][0]) {
            if ($array_borrar) {
                $artista->setFoto_perfil(NULL);
            }

            $files = reArrayFiles($_FILES['foto']);
            foreach ($files as $k => $foto) {
                if ($foto['name']) {
                    $nombre_foto = guardarFoto($foto, FOTOS_ARTISTAS, $artista->getId(), false, FOTO_PRODUCTO_WIDTH, FOTO_PRODUCTO_HEIGHT, true);
                    $artista->setFoto_perfil($nombre_foto);
                }
            }
        }
    }

    public function getArtistaAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda OR c.apellido like :busqueda OR c.telefono like :busqueda OR c.email like :busqueda OR c.descripcion like :busqueda OR c.notas like :busqueda OR c.licencia like :busqueda OR c.domicilio like :busqueda OR c.banco like :busqueda OR c.disponible like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Artista c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Artista c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Artista c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Artista c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $artistas = $query->getResult();
        foreach ($artistas as $artista) {
            $out [] = $artista->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getArtistasActivos() {
        return $this->orm->createQuery("SELECT a FROM Admin\Artista a WHERE a.activo=1")->getResult();
    }

}
