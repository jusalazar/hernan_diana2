<?php

use Carbon\Carbon;
use Tools\Mensaje;
use Tools\Rol;

/**
 * Description of General_model
 * @author mato
 *
 *
 */
class Roles_model extends A_model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     * @return Rol
     */
    function getRol($id = false)
    {
        if ($id === FALSE) {
            $dql = "SELECT r FROM Tools\Rol r";
            $roles = ($this->orm->createQuery($dql)->getResult());
            return $roles;
        }
        $rol = $this->orm->find("Tools\Rol", $id);
        return $rol;
    }

    function getRoles($id = false)
    {
        return $this->toJson($this->getRol($id));
    }

    public function store($args)
    {
        $rol = new Rol();
        try {
            $rol->setear(null, $args['nombre'], $args['panel'], $args['peso'], Carbon::now());
            $this->persistir($rol);
            return Mensaje::getExito("Rol creado");
        } catch (Exception $e) {
            return Mensaje::getError("Rol no pudo ser creado");
        }
    }

    public function update($id, $args)
    {
        try {
            $rol = $this->getRol($id);
            $rol->setNombre($args['nombre']);
            $rol->setPanel($args['panel']);
            $rol->setPeso($args['peso']);
            $this->persistir($rol);
            return Mensaje::getExito("Rol editado con éxito");
        } catch (Exception $e) {
            return Mensaje::getError("Rol no pudo ser editadxxo", $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $rol = $this->orm->getReference("Tools\Rol", $id);
            $this->orm->remove($rol);
            $this->orm->flush();
            return Mensaje::getExito("Rol borrado");
        } catch (Exception $e) {
            return Mensaje::getError("Rol no pudo ser borrado" ,$e->getMessage());
        }
    }
}