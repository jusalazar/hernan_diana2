<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Newsletter_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Newsletter", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Newsletter", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($this->post("new_id")) {
            $newsletter = $this->getCollectionByid("Admin\Newsletter", $information["new_id"]);
        } else {
            $newsletter = new Admin\Newsletter();
            $newsletter->setCreated_at(new \DateTime());
            $this->orm->persist($newsletter);
        }
        $newsletter->setEmail($information["new_email"]);
        $newsletter->setInteresa_perro($this->post("new_interesa_perro") ? true :false);
        $newsletter->setInteresa_gato($this->post("new_interesa_gato") ? true: false);
        $newsletter->setActivo(true);
        $newsletter->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getNewsletterAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.email like :busqueda OR c.interesa_perro like :busqueda OR c.interesa_gato like :busqueda OR c.fecha_baja like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Newsletter c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Newsletter c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Newsletter c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Newsletter c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $newsletters = $query->getResult();
        foreach ($newsletters as $newsletter) {
            $out [] = $newsletter->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

}
