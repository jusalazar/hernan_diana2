<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Empresa_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Empresa", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Empresa", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["emp_id"]) {
            $empresa = $this->getCollectionByid("Admin\Empresa", $information["emp_id"]);
        } else {
            $empresa = new Admin\Empresa();
            $empresa->setCreated_at(new \DateTime());
            $this->orm->persist($empresa);
        }
        $empresa->setNombre($information["emp_nombre"]);
        $empresa->setActivo(isset($information["emp_activo"]) && !$information["emp_activo"] ? 0 : 1);
        $empresa->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getEmpresaAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Empresa c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Empresa c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Empresa c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Empresa c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $empresas = $query->getResult();
        foreach ($empresas as $empresa) {
            $out [] = $empresa->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getEmpresasActivas() {
        return $this->orm->createQuery("SELECT e FROM Admin\Empresa e WHERE e.activo=1")->getResult();
    }

}
