<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Cliente_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Cliente", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Cliente", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["cli_id"]) {
            $cliente = $this->getCollectionByid("Admin\Cliente", $information["cli_id"]);
        } else {
            $cliente = new Admin\Cliente();
            $cliente->setCreated_at(new \DateTime());
            $this->orm->persist($cliente);
        }
        $cliente->setNombre($information["cli_nombre"]);
        $cliente->setTelefono($information["cli_telefono"]);
        $cliente->setEmail($information["cli_email"]);
        $cliente->setActivo(isset($information["cli_activo"]) && !$information["cli_activo"] ? 0 : 1);
        $cliente->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getClienteAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda OR c.telefono like :busqueda OR c.email like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Cliente c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Cliente c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Cliente c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Cliente c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $clientes = $query->getResult();
        foreach ($clientes as $cliente) {
            $out [] = $cliente->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }
    
    function getClientesActivos(){
        return $this->orm->createQuery("SELECT c FROM Admin\Cliente c WHERE c.activo=1")->getResult();
    }

}
