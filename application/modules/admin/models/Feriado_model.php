<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Admin\Feriado;
use Tools\Mensaje;
use Carbon\Carbon;

/**
 * Class Formulario_model
 */
class Feriado_model extends A_Model
{

    function getAll()
    {
        return $this->getAllCollection("Admin\Feriado");
    }

    function store($params)
    {
        try {
            $feriado = new Feriado();
            $this->save($feriado, $params);
            return Mensaje::getExito("Feriado Grabado");
        } catch (Exception $e) {
            return Mensaje::getError("Error al grabar feriado", $e->getMessage());
        }

    }

    function edit($params)
    {
        try {
            $feriado = $this->getFeriadoById($params['id']);
            $this->save($feriado, $params);
            return Mensaje::getExito("Feriado Grabado");
        } catch (Exception $e) {
            return Mensaje::getError("Error al grabar feriado", $e->getMessage());
        }

    }

    function save(Feriado $feriado, $params)
    {
        $fecha = DateTime::createFromFormat("d/m/Y", $params['fecha']);
        $feriado->setear($params['descripcion'], $fecha, $params['movible']);
        $this->persistir($feriado);
    }

    function destroy($id)
    {
        try {
            $feriado = $this->orm->getReference("Admin\Feriado", $id);
            $this->orm->remove($feriado);
            return Mensaje::getExito("Feriado eliminado");
        } catch (Exception $e) {
            return Mensaje::getError("Error al eliminar feriado", $e->getMessage());
        }
    }

    /**
     * @param $id
     * @return Feriado
     */
    function getFeriadoById($id)
    {
        return $this->orm->find("Admin\Feriado", $id);
    }
}