<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Lugar_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Lugar", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Lugar", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["lug_id"]) {
            $lugar = $this->getCollectionByid("Admin\Lugar", $information["lug_id"]);
            $domicilio = $lugar->getDomicilio();
            $domicilio->actualizar($this->post("altura"), $this->post("barrio"), $this->post("calle"), $this->post("cityLat"), $this->post("cityLng"), $this->post("ciudad"), $this->post("piso"), $this->post("depto"), $this->post("pais"), 1);
        } else {
            $lugar = new Admin\Lugar();
            $lugar->setCreated_at(new \DateTime());
            $this->orm->persist($lugar);
            $domicilio = new Domicilio($this->post("altura"), $this->post("barrio"), $this->post("calle"), $this->post("cityLat"), $this->post("cityLng"), $this->post("ciudad"), $this->post("piso"), $this->post("depto"), $this->post("pais"), 1);
            $this->orm->persist($domicilio);
        }
        $lugar->setNombre($information["lug_nombre"]);
        $lugar->setContacto_nombre($information["lug_contacto_nombre"]);
        $lugar->setContacto_email($information["lug_contacto_email"]);
        $lugar->setContacto_telefono($information["lug_contacto_telefono"]);
        $lugar->setCapacidad_lugar($information["lug_capacidad_lugar"]);
        $lugar->setDomicilio($domicilio);
        $lugar->setPrivado(isset($information["lug_privado"]) ? true : false);
        $lugar->setActivo(isset($information["lug_activo"]) && !$information["lug_activo"] ? false : true);
        $lugar->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getLugarAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda OR c.contacto_nombre like :busqueda OR c.contacto_email like :busqueda OR c.contacto_telefono like :busqueda OR c.capacidad_lugar like :busqueda OR c.domicilio like :busqueda OR c.privado like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Lugar c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Lugar c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Lugar c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Lugar c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $lugars = $query->getResult();
        foreach ($lugars as $lugar) {
            $out [] = $lugar->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getLugaresActivos() {
        return $this->orm->createQuery("SELECT l FROM Admin\Lugar l WHERE l.activo=1")->getResult();
    }

}
