<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Puntoventa_model extends A_Model {
    /* @var $puntoventa Admin\Puntoventa */

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Puntoventa", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Puntoventa", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    private function cargaDomicilio(&$puntoventa, $information) {
        if ($puntoventa->getDomicilio()) {
            $domicilio = $puntoventa->getDomicilio();
        } else {
            $domicilio = new Domicilio();
            $this->orm->persist($domicilio);
        }
        $domicilio->setAltura($information["altura"]);
        $domicilio->setBarrio($information["barrio"]);
        $domicilio->setCalle($information["calle"]);
        $domicilio->setLatitud($information["cityLat"]);
        $domicilio->setLongitud($information["cityLng"]);
        $domicilio->setCiudad($information["ciudad"]);
        $domicilio->setPiso($information["piso"]);
        $domicilio->setDpto($information["depto"]);
        $domicilio->setPais($information["pais"]);
        $domicilio->setCodigo_postal($information["cod_postal"]);
        $domicilio->setActivo(true);
        $puntoventa->setDomicilio($domicilio);
    }

    public function insertar($information) {
        if ($information["pun_id"]) {
            $puntoventa = $this->getCollectionByid("Admin\Puntoventa", $information["pun_id"]);
        } else {
            $puntoventa = new Admin\Puntoventa();
            $puntoventa->setCreated_at(new \DateTime());
            $puntoventa->setActivo(true);
            $this->orm->persist($puntoventa);
        }
        $puntoventa->setNombre($information["pun_nombre"]);
        $puntoventa->setCliente($this->orm->getReference("Admin\Cliente", $information["pun_cliente"]));
        $this->cargaDomicilio($puntoventa, $information);
        $puntoventa->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getPuntoventaAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda OR c.cliente like :busqueda OR c.domicilio like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Puntoventa c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Puntoventa c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Puntoventa c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Puntoventa c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $puntoventas = $query->getResult();
        foreach ($puntoventas as $puntoventa) {
            $out [] = $puntoventa->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getPuntosActivos() {
        return $this->orm->createQuery("SELECT p FROM Admin\Puntoventa p WHERE p.activo=1")->getResult();
    }

}
