<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Licencia_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Licencia", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Licencia", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["lic_id"]) {
            $licencia = $this->getCollectionByid("Admin\Licencia", $information["lic_id"]);
        } else {
            $licencia = new Admin\Licencia();
            $licencia->setCreated_at(new \DateTime());
            $this->orm->persist($licencia);
        }
        $licencia->setNombre($information["lic_nombre"]);
        $licencia->setActivo(isset($information["lic_activo"]) && !$information["lic_activo"] ? 0 : 1);
        $licencia->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getLicenciaAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Licencia c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Licencia c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Licencia c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Licencia c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $licencias = $query->getResult();
        foreach ($licencias as $licencia) {
            $out [] = $licencia->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getLicenciasActivas() {
        return $this->orm->createQuery("SELECT l FROM Admin\Licencia l WHERE l.activo=1")->getResult();
    }

}
