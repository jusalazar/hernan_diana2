<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Admin_model extends A_Model {
    /* @var $proveedor Proveedor */
    /* @var $domicilio Domicilio */
    /* @var $punto_venta Punto_venta */

    public function __construct() {
        parent::__construct();
    }

    function getDashboard() {
        $response["cant_contratos_alquiler_pendiente"] = 0;
        $response["cant_consultas_pendientes"] = 0;
        return $response;
    }

    function getProductosConMayorIngreso() {
        $query = "select CONCAT(producto.pro_nombre,' ',var_ume,' ',var_cantidad) nombre,pro_foto imagen,MAX(prv_precio_real) precio,SUM(cor_precio*cor_cantidad) total_vendido
            from compra
           inner join compra_renglon on cor_compra=com_id
           inner join proveedor_producto_variacion on cor_proveedor_variacion=ppv_id
           inner join proveedor on pro_id=ppv_proveedor
           inner join usuarios on id=pro_usu_id
           inner join producto_variacion on prv_id=ppv_producto_variacion
           inner join variacion on var_id=prv_variacion
           inner join producto producto on producto.pro_id=prv_producto
           group by producto.pro_nombre,pro_foto,var_ume,var_cantidad
           order by total_vendido desc
           limit 10";
        return $this->db->query($query)->result();
    }

    function getProductosMasVendidos() {
        $query = "select CONCAT(producto.pro_nombre,' ',var_ume,' ',var_cantidad) nombre,pro_foto imagen,MAX(prv_precio_real) precio,SUM(cor_cantidad) unidades_totales,SUM(cor_precio*cor_cantidad) total_vendido
            from compra
           inner join compra_renglon on cor_compra=com_id
           inner join proveedor_producto_variacion on cor_proveedor_variacion=ppv_id
           inner join proveedor on pro_id=ppv_proveedor
           inner join usuarios on id=pro_usu_id
           inner join producto_variacion on prv_id=ppv_producto_variacion
           inner join variacion on var_id=prv_variacion
           inner join producto producto on producto.pro_id=prv_producto
           group by producto.pro_nombre,pro_foto,var_ume,var_cantidad
           order by unidades_totales desc
           limit 10";
        return $this->db->query($query)->result();
    }

    function getRenglonesDemorados($information) {
        $out = array();
        if (isset($information["limit"])) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if (isset($information["offset"])) {
            $offset = $information["offset"];
        }
        $orden = "ASC";
        if (isset($information["order"])) {
            switch ($information["order"]) {
                case "asc" :
                    $orden = "ASC";
                    break;
                case "desc" :
                    $orden = "DESC";
                    break;
            }
        }


        $joins = "JOIN c.horario_preferido h";
        /* @var $cada_renglon \Comprarenglon */
        $renglones = $this->orm->createQuery("SELECT c FROM \Comprarenglon c $joins WHERE c.fecha_pedida < CURRENT_TIMESTAMP() AND (c.estado=" . COMPRA_PROCESANDO . " OR c.estado=" . COMPRA_PEDIDA . ") ORDER BY c.id ASC")->setFirstResult($offset)->setMaxResults($limit)->getResult();
        $response["total"] = $this->orm->createQuery("SELECT COUNT(c) cant FROM \Comprarenglon c $joins WHERE c.fecha_pedida < CURRENT_TIMESTAMP() AND (c.estado=" . COMPRA_PROCESANDO . " OR c.estado=" . COMPRA_PEDIDA . ") ")->setMaxResults(1)->getSingleScalarResult();

        foreach ($renglones as $cada_renglon) {
            $out[] = $cada_renglon->getDatosArray();
        }
        $response["rows"] = $out;
        return $response;
    }

}
