<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Pintura_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Pintura", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Pintura", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["pin_id"]) {
            $pintura = $this->getCollectionByid("Admin\Pintura", $information["pin_id"]);
        } else {
            $pintura = new Admin\Pintura();
            $pintura->setCreated_at(new \DateTime());
            $pintura->setActivo(true);
            $this->orm->persist($pintura);
        }
        $pintura->setNombre($information["pin_nombre"]);
        $pintura->setTiempo($information["pin_tiempo"]);
        $pintura->setPago($information["pin_pago"]);
        $pintura->setCosto($information["pin_costo"]);
        $pintura->setArtista($this->orm->getReference("Admin\Artista", $information["pin_artista"]));
        $pintura->setTipopintura($this->orm->getReference("Admin\Tipopintura", $information["pin_tipopintura"]));
        $pintura->setModified_at(new \DateTime());
        $this->cargaFotoPintura($pintura);
        $this->orm->flush();
    }

    public function cargaFotoPintura(&$pintura, $array_borrar = false) {
        
        if (isset($_FILES['foto']) && $_FILES['foto']['name'][0]) {
            if ($array_borrar) {
                $pintura->setFoto(NULL);
            }

            $files = reArrayFiles($_FILES['foto']);
            foreach ($files as $k => $foto) {
                if ($foto['name']) {
                    $nombre_foto = guardarFoto($foto, FOTOS_PINTURAS, $pintura->getId(), false, FOTO_PRODUCTO_WIDTH, FOTO_PRODUCTO_HEIGHT, true);
                    $pintura->setImagen($nombre_foto);
                }
            }
        }
    }

    public function getPinturaAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda OR c.imagen like :busqueda OR c.tiempo like :busqueda OR c.pago like :busqueda OR c.costo like :busqueda OR c.artista like :busqueda OR c.tipopintura like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Pintura c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Pintura c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Pintura c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Pintura c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $pinturas = $query->getResult();
        foreach ($pinturas as $pintura) {
            $out [] = $pintura->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getPinturasActivas() {
        return $this->orm->createQuery("SELECT p FROM Admin\Pintura p WHERE p.activo=1")->getResult();
    }

}
