<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Banco_model extends A_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getById($id) {
        return $this->getCollectionByid("Admin\Banco", $id);
    }

    public function borrar($id) {
        $objeto = $this->getCollectionByid("Admin\Banco", $id);
        if ($objeto->getActivo()) {
            $objeto->setActivo(0);
        } else {
            $objeto->setActivo(1);
        }
        $this->orm->flush();
    }

    public function insertar($information) {
        if ($information["ban_id"]) {
            $banco = $this->getCollectionByid("Admin\Banco", $information["ban_id"]);
        } else {
            $banco = new Admin\Banco();
            $banco->setCreated_at(new \DateTime());
            $banco->setActivo(1);
            $this->orm->persist($banco);
        }
        $banco->setNombre($information["ban_nombre"]);
        $banco->setModified_at(new \DateTime());
        $this->orm->flush();
    }

    public function getBancoAjax($information) {
        $out = array();
        if ($information["limit"]) {
            $limit = $information["limit"];
        } else {
            $limit = 20;
        }
        $offset = 0;
        if ($information["offset"]) {
            $offset = $information["offset"];
        }
        $orden = "DESC";
        switch ($information["order"]) {
            case "asc" :
                $orden = "ASC";
                break;
            case "desc" :
                $orden = "DESC";
                break;
        }

        $sort = "c.id";
        if (isset($information["sort"])) {
            $sort = "c.{$information["sort"]}";
        }

        if (isset($information["search"])) {
            $where = "WHERE c.nombre like :busqueda";
            $query = $this->orm->createQuery("SELECT c FROM Admin\Banco c $where ORDER BY $sort $orden")->setParameter("busqueda", "%" . $information["search"] . "%")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Banco c $where")->setParameter("busqueda", "%" . $information["search"] . "%")->setMaxResults(1)->getSingleScalarResult();
        } else {
            $query = $this->orm->createQuery("SELECT c FROM Admin\Banco c  ORDER BY $sort $orden")->setFirstResult($offset)->setMaxResults($limit);
            $data ["total"] = $this->orm->createQuery("SELECT count(c) FROM Admin\Banco c ")->setMaxResults(1)->getSingleScalarResult();
        }
        $bancos = $query->getResult();
        foreach ($bancos as $banco) {
            $out [] = $banco->getDatosArray();
        }
        $data ["rows"] = $out;
        return $data;
    }

    function getBancosActivos() {
        return $this->orm->createQuery("SELECT b FROM Admin\Banco b WHERE b.activo=1")->getResult();
    }

}
