<?php
$this->load->view("general/header");
?>
<style>
    body {
        background-image: url(<?= base_url("public/img/back_vendedores.jpg") ?>);
        background-size: cover;
    }

    .login-logo {
        color: white;
        font-size: 38px;
    }
</style>

<div class="login-box">
    <div class="login-logo">
        <b>&nbsp;</b><span class="texto-blanco">&nbsp;</span>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Ingrese sus credenciales</p>
        <p style="color:red;text-align: center;"><?=$error?></p>
        <?php
        $url = "validar_acceso";
        $atributos = array("id" => "formLogin");
        echo form_open($url, $atributos); ?>
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Usuario" id="user" name="user" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <?php if($intentos > 2):?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center;">
                <div class="row">
                    <?= $captcha['image'] ?>
                </div>
                <br>
                <?php
                $data = array(
                    "id" => "captcha",
                    "name" => "captcha",
                    "class" => "form-control",
                    "placeholder" => "Escriba aquí lo que ve en la imagen"
                );
                echo form_input($data);
                ?>
            </div>
            <?php endif;?>
            <!-- /.col -->
            <div class="col-xs-offset-8 col-xs-4">
                <br>
                <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
            </div>
            <!-- /.col -->
        </div>
        <?= form_close(); ?>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<?php
$this->load->view("general/footer_common");

?>

<script>
    formLogin = $("#formLogin");
    formLogin.validate();
</script>
</body>
</html>
