<?php

use Tools\Usuario;

/**
 * Description of Usuario_model
 * @author mato
 */
class Usuario_model extends A_model {

    function __construct() {
        parent::__construct();
    }

    /**
     *
     * @return \Acceso\Usuario
     */
    function getUsuarios() {
        return $this->orm->createQuery("SELECT u FROM Tools\Usuario u ORDER BY u.id DESC")->getResult();
    }

    function getById($usu_id) {
        if (Auth::esAdmin()) {
            return $this->orm->createQuery("SELECT u FROM Tools\Usuario u WHERE u.id=:user_id")->setParameter("user_id", $usu_id)->getSingleResult();
        }return false;
    }

    function getusuariosCombo() {
        $usuarios = $this->getUsuarios();
        $datos = array();
        foreach ($usuarios as $item) {
            $datos[$item['id']] = $item['nombre'];
        }
        return $datos;
    }

    /**
     * @return \Acceso\Usuario[] $vendedores
     */
    function getVendedores() {
        $qb = $this->getQueryBuilder();
        $qb->select("u")->from("Tools\Usuario", "u")->where("u.rol = 5");
        return $qb->getQuery()->getResult();
    }

    function getVendedoresCombo() {
        $registros = $this->getVendedores();
        $vendedores = array();
        foreach ($registros as $item) {
            $vendedores[$item->getId()] = mb_strtoupper($item->getNombrePersona(), "utf8");
        }
        return $vendedores;
    }

    function getRoles() {
        return $this->getAllCollection("Tools\Rol");
    }

    function getRolesCombo() {
        $qb = $this->getQueryBuilder();
        $qb->select("r")->from("Tools\Rol", "r");
        $roles = $qb->getQuery()->getResult();
        foreach ($roles as $rol) {
            $datos[$rol->getId()] = $rol->getNombre();
        }
        return $datos;
    }

    /**
     *
     * @param int $id
     * @return \Acceso\Usuario
     */
    function getUsuario($id) {
        return $this->getObjeto("Tools\Usuario", $id);
    }

    function insertar() {
        try {
            $rol = $this->orm->getReference("Tools\Rol", $this->post("rol"));
            if ($this->post("usu_id")) {
                $usuario = $this->getUsuario($this->post("usu_id"));
            } else {
                $usuario = new \Tools\Usuario();
                $usuario->setPassword(password_hash($this->post("clave"), PASSWORD_DEFAULT));
            }
            $roles[] = $rol;
            $usuario->setRoles($roles);
            $usuario->setEmail($this->post("nombre_usuario"));
            if ($this->post("fecha_baja")) {
                $usuario->setFecha_baja(new \DateTime());
            } else {
                $usuario->setFecha_baja(NULL);
            }
            if ($this->post("cambiar_clave") == 1) {
                $usuario->setPassword(password_hash($this->post("clave"), PASSWORD_DEFAULT));
            }
            if ($this->post("forzar_cambio_clave") == 1) {
                $usuario->setFuerza_cambio_clave(true);
            }
            $this->persistir($usuario);

            //alta domiciilio si viene
            if ($this->post("direccion")) {
                if ($this->post("cld_id")) {
                    $domicilio = $this->getCollectionByid("Domicilio", $this->post("cld_id"));
                } else {
                    $domicilio = new Domicilio();
                }
                $domicilio->setAltura($this->post("altura"));
                $domicilio->setBarrio($this->post("barrio"));
                $domicilio->setCalle($this->post("calle"));
                $domicilio->setLatitud($this->post("cityLat"));
                $domicilio->setLongitud($this->post("cityLng"));
                $domicilio->setCiudad($this->post("ciudad"));
                $domicilio->setPiso($this->post("piso"));
                $domicilio->setPais($this->post("pais"));
                $domicilio->setActivo(true);
                if ($usuario->getDomicilios()) {
                    foreach ($usuario->getDomicilios() as $cada_domicilio) {
                        $cada_domicilio->setPreferente(false);
                    }
                }
                $domicilio->setPreferente(true);
                $domicilio->setDireccion_completa($this->post("direccion"));
                $domicilio->setUsuario($usuario);
                $this->orm->persist($domicilio);
                $this->orm->flush();
            }

            if ($rol->getNombre() == "proveedor") {
                if ($this->post("usu_id")) {
                    $proveedor = $this->orm->createQuery("SELECT p FROM Proveedor p WHERE p.usuario=" . $usuario->getId())->getSingleResult();
                } else {
                    $proveedor = new Proveedor();
                }
                $proveedor->setUsuario($usuario);
                $proveedor->setFechaIn(new \DateTime());
                $proveedor->setAutorizacion(true);
                $proveedor->setNombre($this->post("perfil_nombre"));
                $this->persistir($proveedor);
                if ($proveedor->getPunto_venta()) {
                    $punto_venta = $proveedor->getPunto_venta();
                } else {
                    $punto_venta = new Punto_venta();
                    $this->orm->persist($punto_venta);
                }
                $punto_venta->setDomicilio($domicilio);
                $punto_venta->setProveedor($proveedor);
                $punto_venta->setHace_delivery(1);
                $punto_venta->setCosto_envio(0.00);
                $punto_venta->setRadio_entrega(0);
                $punto_venta->setTiempo_entrega_estimado(180);
                $punto_venta->setNombre($this->post("perfil_nombre"));
                $punto_venta->setTelefono($this->post("perfil_telefono"));
                $punto_venta->setActivo(true);
                $punto_venta->setEmail($this->post("nombre_usuario"));
                $this->orm->flush();
            }

            if ($this->post("usu_id")) {
                $perfil = $usuario->getPerfil();
            } else {
                $perfil = new Tools\Perfil();
            }
            $perfil->setUsuario($usuario);
            $perfil->setNombre($this->post("perfil_nombre"));
            $perfil->setApellido($this->post("perfil_apellido"));
            $perfil->setTelefono($this->post("perfil_telefono"));
            $this->persistir($perfil);
            $this->orm->flush();

            $respuesta['mensaje'] = "Nuevo usuario guardado";
            $respuesta['exito'] = "success";
        } catch (Exception $ex) {
            $respuesta['mensaje'] = "Problema al insertar";
            $respuesta['exito'] = "danger";
            die($ex->getMessage());
        }

        return $respuesta;
    }

    function actualizar() {
        /* @var $usuario Tools\Usuario */
        try {
            $rol = $this->orm->getReference("Tools\Rol", $this->post("rol"));
            $usuario = $this->getUsuario($this->post("id"));
            $usuario->setEmail($this->post("nombre"));
            if ($this->post("cambiar_clave") == 1) {
                $usuario->setPassword(password_hash($this->post("clave"), PASSWORD_DEFAULT));
            }
            if ($this->post("forzar_cambio_clave") == 1) {
                $usuario->setFuerza_cambio_clave(true);
            }
            $roles[] = $rol;
            $usuario->setRoles($roles);
            $this->persistir($usuario);
            $respuesta['mensaje'] = "Usuario guardado";
            $respuesta['exito'] = "success";
        } catch (Exception $ex) {
            $respuesta['mensaje'] = "Problema al actualizar";
            $respuesta['exito'] = "danger";
        }

        return $respuesta;
    }

    function borrarUsuario($id) {

        try {
            $usuario = $this->getUsuario($id);
            $this->orm->remove($usuario);
            $this->orm->flush();
            $respuesta['mensaje'] = "Usuario eliminado";
            $respuesta['exito'] = "success";
        } catch (Exception $ex) {
            $respuesta['mensaje'] = "Problema al eliminar";
            $respuesta['exito'] = "danger";
        }

        return $respuesta;
    }

    /**
     * @param string $nombre
     * @param string $password
     * @return Usuario $usuario
     */
    function getUsuarioPorNombre($nombre) {
        $qb = $this->getQueryBuilder();
        $qb->select("u")->from("Tools\Usuario", "u")->where("u.email = :nombre");
        $qb->setParameter("nombre", $nombre);
        return $qb->getQuery()->getResult()[0];
    }

    public function getClientesSelect2($args) {
        /* @var $usuario Tools\Usuario */
        $nombre = strtolower(trim($args['q']['term'], 'utf-8'));
        $query = $this->orm->createQuery("SELECT u FROM Tools\Usuario u  JOIN u.roles r JOIN u.perfil p WHERE r.id=:rol_id AND p.nombre like :busqueda ORDER BY p.nombre ASC");
        $usuarios = $query->setParameters(array("busqueda" => "%" . $nombre . "%", "rol_id" => ROL_COMPRADOR_ID))->getResult();

        $data = array();
        if ($usuarios) {
            foreach ($usuarios as $usuario) {
                $data[] = array(
                    "domicilio" => $usuario->getDomicilioPreferente() ? $usuario->getDomicilioPreferente()->getDireccion_completa() : "sin domicilio"
                    , "longitud" => $usuario->getDomicilioPreferente() ? $usuario->getDomicilioPreferente()->getLongitud() : "0"
                    , "telefono" => $usuario->getEmail()
                    , "email" => $usuario->getEmail()
                    , "latitud" => $usuario->getDomicilioPreferente() ? $usuario->getDomicilioPreferente()->getLatitud() : "0"
                    , "id" => $usuario->getId()
                    , "text" => $usuario->getPerfil()->getNombreCompleto());
            }
        } else {
            $data[] = array("id" => "0", "text" => "No se encontraron resultados..");
        }
        $ret['results'] = $data;
        return $ret;
    }

    public function crearDomicilio($information) {
        try {
            $edita = false;
            if (isset($information["dom_id"]) && $information["dom_id"]) {
                $domicilio = $this->getCollectionByid("\Domicilio", $information["dom_id"]);
                $edita = true;
            } else {
                $domicilio = new Domicilio();
            }

            if (isset($information["piso"])) {
                $domicilio->setPiso($information["piso"]);
            }
            if (isset($information["depto"])) {
                $domicilio->setDpto($information["depto"]);
            }

            $domicilio->setAltura($information["altura"]);
            $domicilio->setBarrio($information["barrio"]);
            $domicilio->setCalle($information["calle"]);
            $domicilio->setLatitud($information["cityLat"]);
            $domicilio->setLongitud($information["cityLng"]);
            $domicilio->setCiudad($information["ciudad"]);
            $domicilio->setPais($information["pais"]);
            $domicilio->setActivo(true);
            $domicilio->setPreferente(true);
            $domicilio->setDireccion_completa($information["direccion"]);
            $repite_domicilio = false;
            if (Auth::estaLogeado()) {
                $usuario = $this->getCollectionByid("Tools\Usuario", get_user_id());
                $domicilio->setUsuario($usuario);
                if ($usuario->getDomicilioPreferente()) {
                    $domicilio->setPreferente(false);
                }
                if (!$edita) {
                    foreach ($usuario->getDomicilios() as $cada_domicilio) {
                        if ($cada_domicilio->getLatitud() == $information["cityLat"] && $cada_domicilio->getLongitud() == $information["cityLng"]) {
                            $repite_domicilio = true;
                            break;
                        }
                    }
                }
            }
            if (!$repite_domicilio) {
                $this->orm->persist($domicilio);
                $this->orm->flush();
            }
            $_SESSION["domicilio_actual"] = $domicilio;
            $respuesta['mensaje'] = "Domicilio actual agregado";
            $respuesta['exito'] = "success";
        } catch (Exception $ex) {
            die($ex->getMessage());
            $respuesta['mensaje'] = "No se pudo cargar su domicilio";
            $respuesta['exito'] = "danger";
        }
        return $respuesta;
    }

    function registraConFb($information) {
        try {
            if ($information["fbid"]) {
                $usuario = $this->orm->createQuery("SELECT u FROM Tools\Usuario u WHERE u.fbid=:fb_id or u.email=:email")
                                ->setParameters(array("fb_id" => $information["fbid"], "email" => $information["email"]))->getResult();
                if ($usuario) {
                    $usuario[0]->setFbid($information["fbid"]);
                    $this->orm->flush();
                    $this->logearUsuario($usuario[0]);
                    $response["estado"] = 1;
                    if (strpos($_SERVER["HTTP_REFERER"], "comprar") !== false) {
                        $response["url"] = base_url("comprar");
                    } else {
                        $response["url"] = base_url("perfil");
                    }
                    return $response;
                }
            }
            $rol = $this->orm->getReference("Tools\Rol", ROL_COMPRADOR_ID);
            $usuario = new Tools\Usuario();
            $usuario->setCreated_at(new \DateTime());
            $usuario->setFbid($information["fbid"]);
            $usuario->setFb_image($information["fimage"]);
            $password = crearPassword();
            $usuario->setPassword(password_hash($password, PASSWORD_DEFAULT));
            $usuario->setEmail($information["email"]);
            $usuario->setFuerza_cambio_clave(true);
            $roles[] = $rol;
            $usuario->setRoles($roles);
            $this->persistir($usuario);
            if (isset($_SESSION["domicilio_actual"])) {
                $domicilio = $this->getCollectionByid("Domicilio", $_SESSION["domicilio_actual"]->getId());
                if ($domicilio->getUsuario()) {
                    $dom_clone = Clone $domicilio;
                    $dom_clone->setUsuario($usuario);
                    $dom_clone->setActivo(true);
                    $dom_clone->setPreferente(true);
                    $this->orm->persist($dom_clone);
                } else {
                    $domicilio->setActivo(true);
                    $domicilio->setPreferente(true);
                    $domicilio->setUsuario($usuario);
                }
            } elseif (isset($information["altura"])) {
                $domicilio = new Domicilio();
                $domicilio->setAltura($information["altura"]);
                $domicilio->setBarrio($information["barrio"]);
                $domicilio->setCalle($information["calle"]);
                $domicilio->setLatitud($information["latitud"]);
                $domicilio->setLongitud($information["longitud"]);
                $domicilio->setCiudad($information["ciudad"]);
                $domicilio->setPiso($information["piso"]);
                $domicilio->setDpto(isset($information["depto"]) ? $information["depto"] : "");
                $domicilio->setPais($information["pais"]);
                $domicilio->setActivo(true);
                $domicilio->setPreferente(true);
                $domicilio->setDireccion_completa($information["direccion"]);
                $domicilio->setUsuario($usuario);
                $this->persistir($domicilio);
            } else {
                $domicilio = false;
            }
            $perfil = new Tools\Perfil();
            $perfil->setUsuario($usuario);
            $perfil->setNombre($this->post("nombre"));
            $perfil->setApellido($this->post("apellido"));
            $perfil->setEmail($information["email"]);
            $this->persistir($perfil);
            $usuario->setPerfil($perfil);
            $this->orm->persist($usuario);
            $this->orm->flush();
            $this->bienvenidoUsuario($usuario, $password);
            $this->logearUsuario($usuario);
            $response["estado"] = 1;
            if (strpos($_SERVER["HTTP_REFERER"], "comprar") !== false) {
                $response["url"] = base_url("comprar");
            } else {
                $response["url"] = base_url("perfil");
            }
            return $response;
        } catch (Exception $ex) {
            $response["estado"] = 0;
            return $response;
        }
    }

    function crearDesdeWeb($information) {
        $rol = $this->orm->getReference("Tools\Rol", ROL_COMPRADOR_ID);
        $usuario = new Tools\Usuario();
        $usuario->setCreated_at(new \DateTime());
        if (!isset($information["usu_password"])) {
            $password = crearPassword();
        } else {
            $password = $information["usu_password"];
        }
        $usuario->setPassword(password_hash($password, PASSWORD_DEFAULT));
        $usuario->setEmail($information["email"]);
        $usuario->setFuerza_cambio_clave(true);
        $roles[] = $rol;
        $usuario->setRoles($roles);
        $this->persistir($usuario);


        if (isset($_SESSION["domicilio_actual"]) && $_SESSION["domicilio_actual"]) {
            $domicilio = $this->getCollectionByid("Domicilio", $_SESSION["domicilio_actual"]->getId());
        } elseif (isset($information["altura"])) {
            $domicilio = new Domicilio();
            $domicilio->setAltura($information["altura"]);
            $domicilio->setBarrio($information["barrio"]);
            $domicilio->setCalle($information["calle"]);
            $domicilio->setLatitud($information["latitud"]);
            $domicilio->setLongitud($information["longitud"]);
            $domicilio->setCiudad($information["ciudad"]);
            $domicilio->setPiso($information["piso"]);
            $domicilio->setDpto(isset($information["depto"]) ? $information["depto"] : "");
            $domicilio->setPais($information["pais"]);
            $domicilio->setActivo(true);
            $domicilio->setPreferente(true);
            $domicilio->setDireccion_completa($information["direccion"]);
            $domicilio->setUsuario($usuario);
            $this->persistir($domicilio);
        } else {
            $domicilio = false;
        }

        $perfil = new Tools\Perfil();
        $perfil->setUsuario($usuario);
        $perfil->setNombre($this->post("nombre"));
        $perfil->setApellido($this->post("apellido"));
        $perfil->setTelefono($information["telefono"]);
        $perfil->setEmail($information["email"]);
        $this->persistir($perfil);

        $usuario->setPerfil($perfil);
        $this->orm->persist($usuario);
        $this->orm->flush();
        $this->bienvenidoUsuario($usuario, $password);
        $this->logearUsuario($usuario);
        return $usuario;
    }

    public function bienvenidoUsuario(&$usuario, $password) {
        /* @var $usuario \Tools\Usuario */
        if ($usuario->getFbid()) {
            $texto = "Tu inicio de sesión al sistema es por medio del botón de facebook en MisPichos.com";
        } else {
            $texto = "Tus datos para ingresar son los siguientes:<br> Usuario:" . $usuario->getEmail() . ' <br> Password:' . $password;
        }
        $nombre_persona = $usuario->getPerfil()->getNombre();
        $contenido = utf8_decode(file_get_contents(MAIL_TEMPLATE_PATH . "bienvenido.html"));
        $contenido = str_replace('{TITULO}', "Hola " . $nombre_persona, $contenido);
        $contenido = str_replace('{SUBTITULO}', "Bienvenida a MisPichos.com", $contenido);
        $contenido = str_replace('{MENSAJE}', $texto, $contenido);
        $this->mandaMail($usuario->getEmail(), "Bienvenido a MisPichos", $contenido);
    }

    public function logearUsuario(&$usuario) {
        $data = array(
            "logged" => TRUE,
            "usu_id" => $usuario->getId(),
            "nombre" => $usuario->getEmail(),
            "rol_id" => $usuario->getRol()->getId(),
            "usu_nombre" => $usuario->getPerfil()->getNombre(),
            "rol_nombre" => $usuario->getRol()->getNombre(),
            "rol_panel" => $usuario->getRol()->getPanel(),
            "cantidad_intentos" => 0,
            "error" => ""
        );
        $this->session->set_userdata($data);
    }

    function cambiaDomicilioActual($dom_id) {
        if (Auth::estaLogeado()) {
            $usuario = $this->getPerfil();
            $this->sacaPreferenteDomicilios($usuario);
            $domicilio = $this->getCollectionByid("Domicilio", $dom_id);
            $domicilio->setPreferente(true);
            $this->orm->flush();
            $_SESSION["domicilio_actual"] = $domicilio;
        }
    }

    function sacaPreferenteDomicilios(&$usuario) {
        foreach ($usuario->getDomicilios() as $cada_domicilio) {
            $cada_domicilio->setPreferente(false);
        }
        $this->orm->flush();
    }

    public function existeEmail($email) {
        $existe = $this->orm->createQuery("SELECT u FROM Tools\Usuario u WHERE u.email=:email AND u.fecha_baja IS NULL")->setParameter("email", $email)->getResult();
        if ($existe) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getPerfil() {
        if (Auth::estaLogeado()) {
            return $this->orm->createQuery("SELECT u FROM Tools\Usuario u WHERE u.id=:id")->setParameter("id", get_user_id())->getSingleResult();
        }
        return 0;
    }

    function editaPerfilWeb($information) {
        /* @var $usuario Tools\Usuario */
        $usuario = $this->getPerfil();
        $usuario->setEmail($information["perfil_email"]);
        $usuario->getPerfil()->setNombre($information["perfil_nombre"]);
        $usuario->getPerfil()->setApellido($information["perfil_apellido"]);
        $usuario->getPerfil()->setTelefono($information["perfil_telefono"]);
        $usuario->getPerfil()->setEmail($information["perfil_email"]);
        $this->orm->flush();
    }

    function editaClaveWeb($information) {
        $usuario = $this->getPerfil();
        if (password_verify($information["perfil_pass_actual"], $usuario->getPassword())) {
            $usuario->setPassword(password_hash($information["perfil_pass_nueva"], PASSWORD_DEFAULT));
            $this->orm->flush();
            return "success";
        } else {
            return "error";
        }
    }

    function recuperaCuenta($email) {
        /* @var $usuario Tools\Usuario */
        $query = $this->orm->createQuery("SELECT u FROM Tools\Usuario u WHERE u.email=:email")->setParameter("email", $email);
        $usuario = $query->getResult();

        if ($usuario) {
            $usuario = $usuario[0];
            $pass = crearPassword();
            $usuario->setPassword(password_hash($pass, PASSWORD_DEFAULT));
            $this->orm->flush();

            $texto = "Su nueva contrase&ntilde;a para el usuario:" . $usuario->getEmail() . ' es:' . trim($pass);
            $contenido = utf8_decode(file_get_contents(MAIL_TEMPLATE_PATH . "generico.html"));
            $contenido = str_replace('{TITULO}', "Ha pedido recuperar la clave de acceso a MisPichos", $contenido);
            $contenido = str_replace('{SUBTITULO}', "", $contenido);
            $contenido = str_replace('{MENSAJE}', $texto, $contenido);
            $this->mandaMail($usuario->getEmail(), "Recupera cuenta", $contenido);
            return "success";
        } else {
            return "error";
        }
    }

    function eliminarDomicilio($dom_id) {
        /* @var $domicilio Domicilio */
        $domicilio = $this->getCollectionByid("\Domicilio", $dom_id);
        if ($domicilio->puedeBorrar() && ($domicilio->getUsuario() && ($domicilio->getUsuario()->getId() == get_user_id()))) {
            $domicilio->setActivo(false);
            $this->orm->flush();
        }
    }

    function getDomicilioById($dom_id) {
        /* @var $domicilio Domicilio */
        $domicilio = $this->getCollectionByid("\Domicilio", $dom_id);
        if (($domicilio->getUsuario() && ($domicilio->getUsuario()->getId() == get_user_id()))) {
            return $domicilio;
        }
        return false;
    }

    function getCompraById($com_id) {
        /* @var $compra Compra */
        $compra = $this->getCollectionByid("Compra", $com_id);
        if ($compra->getComprador()->getId() == get_user_id()) {
            $this->load->model("admin/compra/Compra_model", "compra");
            $this->compra->estadoCompraMercadoPago($compra);
            return $compra;
        }
    }

    function calificarPedido($information) {
        /* @var $compra Compra */
        /* @var $cada_renglon Comprarenglon */
        $compra = $this->getCollectionByid("Compra", $information["compra_id"]);
        $compra->setCalificacion_general(round($information["compra_calificacion"]));
        foreach ($compra->getRenglones() as $cada_renglon) {
            $cada_renglon->setCalificacion_comprador($information["renglon"][$cada_renglon->getId()]["llego_horario"]);
        }
        $this->orm->flush();
    }

}
