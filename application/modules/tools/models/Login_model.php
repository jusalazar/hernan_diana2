<?php

/**
 * Created by PhpStorm.
 * User: raulm
 * Date: 8/10/2016
 * Time: 7:13 PM
 */
class Login_model extends A_model {

    function __construct() {
        parent::__construct();
    }

    public function validar() {
        $usuario = $this->orm->createQuery("SELECT u FROM Tools\Usuario u WHERE u.email=:email AND u.fecha_baja IS NULL")->setParameter("email", $this->post("user"))->getResult();
        if ($this->session->cantidad_intentos > 2) {
            if ($this->input->post('captcha') != strtolower($this->session->userdata('captcha'))) {
                return FALSE;
            } else {
                $expiration = time() - 600; // Límite de 10 minutos
                $ip = $this->input->ip_address(); // ip del usuario
                $captcha = $this->input->post('captcha'); // captcha introducido por el usuario
                // eliminamos los captcha con más de 2 minutos de vida
                $this->remove_old_captcha($expiration);
                // comprobamos si es correcta la imagen introducida
                $check = $this->check($ip, $expiration, $captcha);
                /*
                 * |si el número de filas devuelto por la consulta es igual a 1
                 * |es decir, si el captcha ingresado en el campo de texto es igual
                 * |al que hay en la base de datos, junto con la ip del usuario
                 * |entonces dejamos continuar porque todo es correcto
                 */
                if ($check != 1) {
                    return FALSE;
                }
            }
        }
        if ($usuario) {
            if (password_verify($this->post("password"), $usuario[0]->getPassword())) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function validar_sin_captcha() {
        /* @var $usuario Tools\Usuario */
        $query = $this->orm->createQuery("SELECT u FROM Tools\Usuario u WHERE u.email=:email")->setParameter("email", $this->post("user"));
        $usuario = $query->getResult();
        if ($usuario) {
            if (password_verify($this->post("password"), $usuario[0]->getPassword())) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function insert_captcha($cap) {
        // insertamos el captcha en la bd
        $data = array(
            'captcha_time' => $cap['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $cap['word']
        );
        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
    }

    public function remove_old_captcha($expiration) {
        // eliminamos los registros de la base de datos cuyo
        // captcha_time sea menor a expiration
        $this->db->where('captcha_time <', $expiration);
        $this->db->delete('captcha');
    }

    public function check($ip, $expiration, $captcha) {
        // comprobamos si existe un registro con los datos
        // envíados desde el formulario
        $this->db->where('LOWER(word)', $captcha);
        $this->db->where('ip_address', $ip);
        $this->db->where('captcha_time >', $expiration);

        $query = $this->db->get('captcha');
        // devolvemos el número de filas que coinciden
        return $query->num_rows();
    }

}
