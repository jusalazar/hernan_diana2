<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Usuario_model $usuario Modelo de usuario
 */
class Usuario_controller extends A_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata["rol_nombre"] != "admin") {
            redirect(get_home());
            exit;
        }
        $this->load->model("usuario_model", "usuario");
    }

    function index($usu_id = false) {
        $template = $this->twig->load('maker_default.twig');
        $data['nombre_formulario'] = "usuario/usuario_index.twig";
        $data['title'] = "Mis usuarios";
        $data['roles'] = $this->usuario->getRoles();
        $data['usuarios'] = $this->usuario->getUsuarios();
        if ($usu_id) {
            $data['usuario'] = $this->usuario->getById($usu_id);
        }
        echo $template->render($data);
    }

    function guardar() {
        if ($this->post()) {
            $this->usuario->insertar();
        }
        redirect("usuarios");
    }

    private function validarFormulario() {
        $this->form_validation->set_rules("id", "id", "required|integer");
        $this->form_validation->set_rules("nombre", "nombre", "required");

        $this->form_validation->set_rules("nombre_apellido", "nombre apellido", "required");
        $this->form_validation->set_rules("rol", "rol", "required");

        return $this->form_validation->run();
    }

    function borrarUsuario() {
        if ($this->post("id") > 0 && is_numeric($this->post("id"))) {
            echo json_encode($this->usuario->borrarUsuario($this->post("id")));
        } else {
            $respuesta['mensaje'] = "Operación no válida";
            $respuesta['exito'] = "danger";
            echo json_encode($respuesta);
        }
    }

    function getClientesSelect2() {
        echo json_encode($this->usuario->getClientesSelect2($this->input->get()));
    }

}
