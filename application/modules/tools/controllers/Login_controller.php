<?php
use Tools\Usuario;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Login_controller
 *
 * @property Login_model $login
 * @property Usuario_model $usuario_m
 *
 */
class Login_controller extends A_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("login_model", "login");
        $this->load->model("usuario_model", "usuario_m");
        $this->rand = random_string('alnum', 6);
    }

    public function index()
    {
        $data = array();
        $data['scripts'] = array(
            "login/form"
        );
        $data['intentos'] = $this->session->cantidad_intentos;
        $data['captcha'] = $this->captcha();
        $data['error'] = $this->session->error;
        $this->session->set_userdata('captcha', $this->rand);
        $this->load->view("login/login", $data);
    }

    public function validar()
    {
        if ($this->login->validar()) {
            $usuario = $this->usuario_m->getUsuarioPorNombre($this->post("user"));
            $usuario->setLast_login(new DateTime());
            $this->login->orm->flush();
            $this->usuario_m->logearUsuario($usuario);
            redirect($usuario->getRol()->getPanel(), "refresh");
        } else {
            $this->session->logged = FALSE;
            $this->session->cantidad_intentos ++;
            $this->session->error = "Error de usuario o contraseña";
            redirect("login");
        }
    }

    public function logout()
    {
        $this->session->logged = FALSE;
        redirect("login", "refresh");
    }

    public function captcha()
    {
        $conf_captcha = array(
            'word' => $this->rand,
            'img_path' => FCPATH . 'captcha/',
            'img_url' => base_url() . 'captcha/',
            
            'font_path' => FCPATH . 'public/fonts/AlfaSlabOne-Regular.ttf',
            'img_width' => '250',
            'img_height' => '60',
            'expiration' => 600
        );
        $cap = create_captcha($conf_captcha);
        $this->login->insert_captcha($cap);
        
        return $cap;
    }
}
