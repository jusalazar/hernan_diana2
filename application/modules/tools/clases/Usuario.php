<?php

namespace Tools;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * @Entity
 * @Table(name="usuarios")
 */
class Usuario extends GenericClass {

    /**
     * @var string $email
     * @Column(type="string", length=64, unique=true, nullable=false, name="email")
     */
    private $email;

    /**
     * @var string $password
     * @Column(type="string", length=64, nullable=false, name="password")
     */
    private $password;

    /**
     * @var string $last_login
     * @Column(type="datetime",nullable=false, name="last_login")
     */
    private $last_login;

    /**
     * @var string $fecha_baja
     * @Column(type="datetime",nullable=false, name="fecha_baja")
     */
    private $fecha_baja;

    /**
     * @var boolean $fuerza_cambio_clave
     * @Column(type="boolean" , name="fuerza_cambio_clave")
     */
    private $fuerza_cambio_clave;

    /**
     * @var Tools\Rol[] $roles
     * @ManyToMany(targetEntity="Tools\Rol")
     * @JoinTable(name="usuarios_roles",
     *      joinColumns={@JoinColumn(name="usuario_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="rol_id", referencedColumnName="id")}
     *      )
     */
    private $roles;

    /**
     * @var \Tools\Perfil $perfil
     * @OneToOne(targetEntity="Tools\Perfil", mappedBy="usuario")
     */
    private $perfil;

    /**
     * @var string $fbid
     * @Column(type="string",name="fbid")
     */
    private $fbid;

    /**
     * @var string $fb_image
     * @Column(type="string",name="fb_image")
     */
    private $fb_image;

    function getFecha_baja() {
        return $this->fecha_baja;
    }

    function setFecha_baja($fecha_baja) {
        $this->fecha_baja = $fecha_baja;
    }

    function getFbid() {
        return $this->fbid;
    }

    function getFb_image() {
        return $this->fb_image;
    }

    function setFbid($fbid) {
        $this->fbid = $fbid;
    }

    function setFb_image($fb_image) {
        $this->fb_image = $fb_image;
    }

    function getFuerza_cambio_clave() {
        return $this->fuerza_cambio_clave;
    }

    function setFuerza_cambio_clave($fuerza_cambio_clave) {
        $this->fuerza_cambio_clave = $fuerza_cambio_clave;
    }

    function __construct() {
        
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return static
     */
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return static
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getLast_login() {
        return $this->last_login;
    }

    /**
     * @param string $last_login
     *
     * @return static
     */
    public function setLast_login($last_login) {
        $this->last_login = $last_login;
        return $this;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function setRoles(array $roles) {
        $this->roles = $roles;
        return $this;
    }

    public function getPerfil() {
        return $this->perfil;
    }

    public function setPerfil($perfil) {
        $this->perfil = $perfil;
        return $this;
    }

    public function getRol() {
        $aux = -1;
        $respuesta = new Rol();
        foreach ($this->getRoles() as $rol) {
            if ($rol->getPeso() > $aux) {
                $respuesta = $rol;
                $aux = $rol->getPeso();
            }
        }
        return $respuesta;
    }

    function getDatosArray() {
        return array(
            "id" => $this->getId(),
            "email" => $this->getEmail(),
            "nombre" => $this->getPerfil()->getNombre() . ' ' . $this->getPerfil()->getApellido(),
            "rol" => $this->getRol()->getNombre()
        );
    }

}
