<?php

namespace Tools;

/**
 * @Entity
 * @Table(name="permisos")
 */
class Permiso extends GenericClass
{
    /**
     * @var string $nombre
     * @Column(type="string", nullable=false, name="nombre")
     */
    private $nombre;
    /**
     * @var string $descripcion
     * @Column(type="text", nullable=false, name="descripcion")
     */
    private $descripcion;

    public function setear($id, $nombre, $descripcion, $created_at)
    {
        $this->setId($id);
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->setCreated_at($created_at);
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     *
     * @return static
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     *
     * @return static
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'nombre'=> $this->nombre,
            'descripcion'=> $this->descripcion,
            'created_at'=> $this->getCreated_at(),
            'modified_at'=> $this->getModified_at(),
        );
    }

}
