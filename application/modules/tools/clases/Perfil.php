<?php

namespace Tools;

use DateTime;
use Carbon\Carbon;

/**
 * @Entity
 * @Table(name="perfiles")
 */
class Perfil extends GenericClass {

    /**
     * @var string $nombre
     * @Column(type="string", nullable=false, name="nombre")
     */
    private $nombre;

    /**
     * @var string $apellido
     * @Column(type="string", nullable=false, name="apellido")
     */
    private $apellido;

    /**
     * @var string $dni
     * @Column(type="string", nullable=false, name="dni")
     */
    private $dni;

    /**
     * @var string $telefono
     * @Column(type="string",   name="telefono")
     */
    private $telefono;

    /**
     * @var string $email
     * @Column(type="string",   name="email")
     */
    private $email;

    /**
     * @var DateTime $fecha_nacimiento
     * @Column(type="datetime",nullable=false, name="fecha_nacimiento")
     */
    private $fecha_nacimiento;

    /**
     * One Perfil has One User.
     * @OneToOne(targetEntity="Tools\Usuario", inversedBy="perfil")
     * @JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    function getTelefono() {
        return $this->telefono;
    }

    function getEmail() {
        return $this->email;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getNombreCompleto() {
        return $this->getNombre() . ' ' . $this->getApellido();
    }
    public function __construct() {
        
    }

    /**
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     *
     * @return static
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getApellido() {
        return $this->apellido;
    }

    /**
     * @param string $apellido
     *
     * @return static
     */
    public function setApellido($apellido) {
        $this->apellido = $apellido;
        return $this;
    }

    /**
     * @return string
     */
    public function getDni() {
        return $this->dni;
    }

    /**
     * @param string $dni
     *
     * @return static
     */
    public function setDni($dni) {
        $this->dni = $dni;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFecha_nacimiento() {
        return $this->fecha_nacimiento;
    }

    /**
     * @param DateTime $fecha_nacimiento
     *
     * @return static
     */
    public function setFecha_nacimiento(DateTime $fecha_nacimiento) {
        $this->fecha_nacimiento = $fecha_nacimiento;
        return $this;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
        return $this;
    }

}
