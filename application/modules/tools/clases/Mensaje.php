<?php
/**
 * Created by PhpStorm.
 * User: raulm
 * Date: 9/7/2017
 * Time: 20:22
 */

namespace Tools;


class Mensaje
{
    public static function getExito($texto)
    {
        $respuesta['success'] = true;
        $respuesta['mensaje'] = $texto;
        $respuesta['error'] = 0;
        $respuesta['problema'] = "";
        return $respuesta;
    }

    public static function getError($texto, $problema = "")
    {
        $respuesta['success'] = false;
        $respuesta['mensaje'] = $texto;
        $respuesta['error'] = 1;
        $respuesta['problema'] = $problema;
        return $respuesta;
    }
}