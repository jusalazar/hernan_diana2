<?php

namespace Tools;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="roles")
 */
class Rol extends GenericClass
{

    /**
     *
     * @var string $nombre @Column(type="string", nullable=false, name="nombre")
     */
    private $nombre;

    /**
     *
     * @var string $panel @Column(type="string", nullable=false, name="panel")
     */
    private $panel;

    /**
     *
     * @var Permiso[] $permisos Many Roles have Many Permisos.
     * @ManyToMany(targetEntity="Permiso")
     * @JoinTable(name="roles_permisos",
     *      joinColumns={@JoinColumn(name="rol_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="permiso_id", referencedColumnName="id")}
     *      )
     */
    private $permisos;

    /**
     *
     * @var int $peso @Column(type="integer", nullable=false, name="peso")
     */
    private $peso;

    public function __construct()
    {
        $this->permisos = new ArrayCollection();
    }

    public function setear($id, $nombre, $panel, $peso, $created_at)
    {
        $this->setId($id);
        $this->nombre = $nombre;
        $this->panel = $panel;
        $this->peso = $peso;
        $this->setCreated_at($created_at);
    }

    /**
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     *
     * @param string $nombre
     *
     * @return static
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPanel()
    {
        return $this->panel;
    }

    /**
     *
     * @param string $panel
     *
     * @return static
     */
    public function setPanel($panel)
    {
        $this->panel = $panel;
        return $this;
    }

    /**
     *
     * @return Permiso[]
     */
    public function getPermisos()
    {
        return $this->permisos;
    }

    /**
     *
     * @param Permiso[] $permisos
     *
     * @return static
     */
    public function setPermisos($permisos)
    {
        $this->permisos = $permisos;
        return $this;
    }

    /**
     *
     * @return number
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     *
     * @param unknown $peso
     * @return \Tools\Rol
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
        return $this;
    }

    public function toJson()
    {
        $obj = array();
        $obj['id'] = $this->getId();
        $obj['nombre'] = $this->getNombre();
        $obj['panel'] = $this->getPanel();
        $obj['peso'] = $this->getPeso();
        return $obj;
    }
}
