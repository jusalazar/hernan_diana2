<?php

namespace Tools;

use Carbon\Carbon;
use DateTime;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 *
 */
class GenericClass implements \JsonSerializable {

    /**
     * @Id
     * @var int $id
     * @Column(type="integer",name="id")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var DateTime $modified_at
     * @Column(type="datetime",nullable=false, name="modified_at")
     */
    private $modified_at;

    /**
     * @var DateTime $created_at
     * @Column(type="datetime",nullable=false, name="created_at")
     */
    private $created_at;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return static
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModified_at() {
        return $this->modified_at;
    }

    /**
     * @param DateTime $modified_at
     *
     * @return static
     */
    public function setModified_at(DateTime $modified_at) {
        $this->modified_at = $modified_at;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated_at() {
        return $this->created_at;
    }

    /**
     * @param DateTime $created_at
     *
     * @return static
     */
    public function setCreated_at(DateTime $created_at) {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * Gets triggered only on insert
     * @PrePersist
     */
    public function onPrePersist() {
        $this->created_at = Carbon::now();
        $this->modified_at = Carbon::now();
    }

    /**
     * Gets triggered every time on update
     * @PreUpdate
     */
    public function onPreUpdate() {
        $this->modified_at = Carbon::now();
    }

    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'modified_at' => $this->modified_at,
            'created_at' => $this->created_at,
        );
    }

}
