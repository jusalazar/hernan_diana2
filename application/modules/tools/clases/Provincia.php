<?php
namespace Tools;

use DateTime;
use Carbon\Carbon;

/**
 * @Entity
 * @Table(name="provincias")
 */
class Provincia extends GenericClass
{

    /**
     *
     * @var string $nombre @Column(type="string", nullable=false, name="nombre")
     */
    private $nombre;

    /**
     *
     * @var int $peso @Column(type="text", nullable=false, name="peso")
     */
    private $peso;

    /**
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     *
     * @param unknown $nombre            
     * @return \Tools\Provincia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     *
     * @return number
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     *
     * @param integer $peso            
     * @return \Tools\Provincia
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
        return $this;
    }
}
