<?php

use Pedido\Pedido;

require_once APPPATH . "/third_party/Classes/PHPExcel.php";

/**
 * Description of Cliente_model
 * @author mato
 * @property Pedido\Pedido_model $pedido
 * @property Ordenfabricacion_model $orden
 * @property Excel $excel
 * @property Etapa_model $etapa
 */
class Argit_PDF extends FPDF {

    function Rotate($angle, $x = -1, $y = -1) {
        if ($x == -1)
            $x = $this->x;
        if ($y == -1)
            $y = $this->y;
        if ($this->angle != 0)
            $this->_out('Q');
        $this->angle = $angle;
        if ($angle != 0) {
            $angle *= M_PI / 180;
            $c = cos($angle);
            $s = sin($angle);
            $cx = $x * $this->k;
            $cy = ($this->h - $y) * $this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        }
    }

// Cabecera de página
    function Header() {
        if ($this->sub_title) {
            $this->Image($this->logo, 17, 5, 25);
            $this->SetFont('Arial', 'B', 15);
            $this->Cell(35);
            $this->Cell(30, 10, utf8_decode($this->title), 0, 0, 'L');
            $this->Ln(3);
            $this->SetFont('Arial', '', 11);
            $this->Cell(35);
            $this->Cell(60, 20, utf8_decode($this->sub_title), 0, 0, 'L');
            $this->Ln(20);
        } else {
            $this->Image($this->logo, 17, 5, 20);
            $this->SetFont('Arial', 'B', 18);
            $this->Cell(80);
            $this->Cell(30, 10, utf8_decode($this->title), 0, 0, 'C');
            $this->Cell(50);
            $this->SetFont('Arial', '', 11);
            /* $hoy = new \DateTime();
              $this->Cell(30, 10, $hoy->format("d/m/Y H:i"), 0, 5, 'R'); */
            $this->Ln(30);
        }

        //marca de agua
         $this->SetFont('Arial', 'B', 50);
          $this->SetTextColor(255, 192, 203);
          $this->RotatedText(35, 190, 'H E R N A N  D I A N A', 45); 
    }

// Pie de página
    function Footer() {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Número de página
        $this->Cell(0, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
    }

    function ChapterTitle($title, $fr = 93, $fg = 139, $fb = 86) {
        $font_size_antiguo = $this->FontSizePt;
        $this->SetFont('Arial', '', 12);
        $this->SetFillColor($fr, $fg, $fb);
        $this->SetTextColor(255, 255, 255);
        $this->Cell(0, 10, utf8_decode($title), 0, 1, 'L', true);
        $this->Ln(1);
        $this->SetFont('Arial', '', $font_size_antiguo);
    }

    function ChapterBody($file) {
        $txt = file_get_contents($file);
        $this->SetFont('Arial', '', 11);
        $this->SetTextColor(0, 0, 0);
        $this->MultiCell(0, 5, utf8_decode($txt));
        $this->Ln();
        $this->SetFont('', 'I');
    }

    function imprimeArchivoTexto($file, $title = false, $pagina_nueva = false) {
        if ($pagina_nueva) {
            $this->AddPage();
        } else {
            $this->Ln(0);
        }
        if ($title) {
            $this->ChapterTitle(($title));
        }
        $this->ChapterBody($file);
    }

    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

}

class Reporte_model extends A_model {
    /* @var $pdf Argit_PDF */
    /* @var  $cliente \Admin\Contrato */
    /* @var  $domicilio \Domicilio */
    /* @var  $banco Admin\Cuentabanco */
    /* @var  $cada_familiar Admin\Cliente_familiar */
    /* @var  $contrato Admin\Contrato */

    function marginCell(&$pdf, $with, $heigth = 12) {
        $pdf->Cell($with, $heigth, '', 0);
    }

    function formaCampo(&$pdf, $w, $h = 12, $nombre, $borde = 1, $espacio = true, $salto_linea = 0, $align = 'L') {
        $pdf->Cell($w, $h, utf8_decode($nombre), $borde, 0, $align);
        if ($espacio) {
            $this->marginCell($pdf, 1);
        }
        if ($salto_linea) {
            $pdf->Ln($salto_linea);
        }
    }
    function muestraFechaEscrita($fecha) {
        return $fecha->format("d") . ' ' . $this->meses[$fecha->format("m") - 1] . ' ' . $fecha->format("Y");
    }



    function cabeceraLocacion(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_header");
        $fecha = new \DateTime();
        $texto = str_replace("{DIA}", $fecha->format("d"), $texto);
        $texto = str_replace("{MES}", $this->meses[$fecha->format("m") - 1], $texto);
        $texto = str_replace("{AÑO}", $fecha->format("Y"), $texto);
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function locadorLocacion(&$pdf, $r, $g, $b, &$contrato) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_locadora");
        $texto = str_replace("{GENERO}", ($contrato->getLocador_sexo() == "masculino" ? "El señor" : "La señora"), $texto);
        $texto = str_replace("{NOMBRE_APELLIDO}", ($contrato->getLocador_nombre() . ' ' . $contrato->getLocador_apellido()), $texto);
        $texto = str_replace("{DOCUMENTO}", ("D.N.I N.º " . $contrato->getLocador_dni()), $texto);
        $texto = str_replace("{DOMICILIO}", ($contrato->getLocador_direccion()), $texto);
        $texto = str_replace("{NOMBRE_FIDEICOMISO}", "Víctor Álvarez Rodríguez y Reina Esther Rodríguez", $texto);
        $texto = str_replace("{REPRESENTANTE_NOMBRE_APELLIDO}", "Víctor Álvarez Rodríguez", $texto);
        $texto = str_replace("{REPRESENTANTE_DOCUMENTO}", "D.N.I. 13.243.817", $texto);
        $texto = str_replace("{NOMBRE_ESCRIBANIA}", "Ana M. G. RAIANO", $texto);
        $texto = str_replace("{GENERO_LOCADOR}", "La Locadora", $texto);
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function locatarioLocacion(&$pdf, $r, $g, $b, &$contrato) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_locataria");
        $texto = str_replace("{GENERO}", ($contrato->getLocatario_sexo() == "masculino" ? "El señor" : "La señora"), $texto);
        $texto = str_replace("{NOMBRE_APELLIDO}", ($contrato->getLocatario_nombre() . ' ' . $contrato->getLocatario_apellido()), $texto);
        $texto = str_replace("{DOCUMENTO}", ("D.N.I N.º " .muestraDni($contrato->getLocatario_dni())), $texto);
        $texto = str_replace("{DOMICILIO}", ($contrato->getLocatario_direccion()), $texto);
        $texto = str_replace("{GENERO_LOCATARIO}", "La Locataria", $texto);
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function fiadorLocacion(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_fiadores");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function segundaInmuebleLocacion(&$pdf, $r, $g, $b,&$contrato) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_segunda");
        $texto = str_replace("{TIPO_INMUEBLE}", strtoupper($contrato->getAtr_tipo_propiedad()->getNombre()), $texto);
        $texto = str_replace("{DOMICILIO}", ($contrato->getDomicilio()->getDireccion_completa()), $texto);
        $texto = str_replace("{ATRIBUTOS}", ($contrato->mostrarAtributosPDF()), $texto);
        $texto = str_replace("{DESCRIPCION}", ($contrato->getDescripcion()), $texto);
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function terceraLocacion(&$pdf, $r, $g, $b,&$contrato) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_tercera");
        $texto = str_replace("{PERIODO_LOCACION}", strtoupper($contrato->getPeriodoDuracionContrato()), $texto);
        $texto = str_replace("{FECHA_INICIO_CONTRATO}",$this->muestraFechaEscrita($contrato->getFecha_inicio_contrato()), $texto);
        $texto = str_replace("{FECHA_FIN_CONTRATO}", $this->muestraFechaEscrita($contrato->getFecha_fin_contrato()), $texto);
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_tercera_6meses");
        
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function cuartaValorLocacion(&$pdf, $r, $g, $b,&$contrato) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_cuarta_valor_alquiler");
        $texto = str_replace("{PERIODO_LOCACION}", strtoupper($contrato->getPeriodoDuracionContrato()), $texto);
        $texto = str_replace("{MONTO_TOTAL_LOCACION}", strtoupper($contrato->getMontoTexto($contrato->getTotal())), $texto);
        $texto = str_replace("{FORMA_PAGO}", ($contrato->getFormaPagoPDF()), $texto);
        $texto = str_replace("{FECHA_DE_PAGO}", ($contrato->getFechadePago()), $texto);
        $texto = str_replace("{DOMICILIO_LUGAR_PAGO}", ($contrato->getLugarDePago()), $texto);
        $texto = str_replace("{TELEFONO_LUGAR_PAGO}", ($contrato->getLocador_telefono()), $texto);
        $texto = str_replace("{ENTREGA_ANTICIPADA}", ($contrato->getMontoTexto($contrato->getAnticipo())), $texto);
        $texto = str_replace("{MES_ADELANTADO}", ($contrato->getPrimerMes()), $texto);
        $texto = str_replace("{AÑO_FINAL_ALQUILER}", ($contrato->getFecha_fin_contrato()->format("Y")), $texto);
        
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function quintaDestino(&$pdf, $r, $g, $b,&$contrato) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_quinta_destino");
        $texto = str_replace("{DESTINO_LOCACION}", ($contrato->getDestino_locacion()), $texto);
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function sextaMultas(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_sexto_multas");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function septimaImpuestos(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_septima_impuestos");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function octavoRetencion(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_octavo_multa_retencion_inmueble");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function nueveModificaInmueble(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_nueve_modificar_inmueble");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function decimaSubarrendar(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_decima_subarrendar");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function onceDesalojo(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_11_desalojo");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function doceSeguroIncendio(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_12_seguro_incendio");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function treceFiestas(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_13_fiestas");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function catorceDevolucionLlave(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_14_devolucion_llave");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function quinceDeposito(&$pdf, $r, $g, $b,&$contrato) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_15_deposito");
        $texto = str_replace("{MONTO_DEPOSITO}", $contrato->getMontoTexto($contrato->getDeposito()), $texto);
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function dieciInspeccion(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_16_inspeccion");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function diecisieteFiadores(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_17_fiadores");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    /*function dieciochoInmuebleProvinciaBsAs(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_18_inmueble_provincia_bsas");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }*/

    function renunciaLey14432(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $texto = file_get_contents(PATH_TEXTOS_REPORTES . "locacion_renuncia_ley14432");
        $pdf->MultiCell(0, 5, utf8_decode($texto), 0, 'J');
    }

    function contratoLocacion(&$pdf, $r, $g, $b, $contrato) {
        $pdf->SetFont('Arial', 'B', 13);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD, "CONTRATO DE LOCACION DE VIVIENDA", 0, true, 12, 'C');
        $pdf->SetFont('Arial', '', 11);
        $pdf->Ln(3);
        $this->cabeceraLocacion($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->locadorLocacion($pdf, $r, $g, $b,$contrato);
        $pdf->Ln(3);
        $this->locatarioLocacion($pdf, $r, $g, $b,$contrato);
        $pdf->Ln(3);
        $this->fiadorLocacion($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->segundaInmuebleLocacion($pdf, $r, $g, $b,$contrato);
        $pdf->Ln(3);
        $this->terceraLocacion($pdf, $r, $g, $b,$contrato);
        $pdf->Ln(3);
        $this->cuartaValorLocacion($pdf, $r, $g, $b,$contrato);
        $pdf->Ln(3);
        $this->quintaDestino($pdf, $r, $g, $b,$contrato);
        $pdf->Ln(3);
        $this->sextaMultas($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->septimaImpuestos($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->octavoRetencion($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->nueveModificaInmueble($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->decimaSubarrendar($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->onceDesalojo($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->doceSeguroIncendio($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->treceFiestas($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->catorceDevolucionLlave($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->quinceDeposito($pdf, $r, $g, $b,$contrato);
        $pdf->Ln(3);
        $this->dieciInspeccion($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->diecisieteFiadores($pdf, $r, $g, $b);
        $pdf->Ln(3);
        /*$this->dieciochoInmuebleProvinciaBsAs($pdf, $r, $g, $b);
        $pdf->Ln(3);*/
        $this->renunciaLey14432($pdf, $r, $g, $b);
    }

    function clienteContratoLocacion($con_id = false) {
        $cliente = $con_id ? $this->getCollectionByid("Admin\Contrato", $con_id) : new \Admin\Contrato();
        $pdf = new Argit_PDF('P', 'mm', 'A4', "Hernan Diana Inmobiliaria");
        $pdf->SetLeftMargin(18);
        $pdf->AddPage();
        $this->contratoLocacion($pdf, 0, 0, 0, $cliente);

        return $pdf;
    }

    public function __construct() {
        parent::__construct();
        $this->meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    }

}
