<?php

require_once APPPATH . "/third_party/Classes/PHPExcel.php";

/**
 * Description of Cliente_model
 * @author mato
 * @property Pedido\Pedido_model $pedido
 * @property Ordenfabricacion_model $orden
 * @property Excel $excel
 * @property Etapa_model $etapa
 */
class Reporte_cooperativa_model extends A_model {
    /* @var $pdf Argit_PDF */

    function marginCell(&$pdf, $with, $heigth = 12) {
        $pdf->Cell($with, $heigth, '', 0);
    }

    function formaCampo(&$pdf, $w, $h = 12, $nombre, $borde = 1, $espacio = true, $salto_linea = 0, $align = 'L') {
        $pdf->Cell($w, $h, utf8_decode($nombre), $borde, 0, $align);
        if ($espacio) {
            $this->marginCell($pdf, 1);
        }
        if ($salto_linea) {
            $pdf->Ln($salto_linea);
        }
    }

    function bloqueDatosPersonales(&$pdf, $r, $g, $b) {
        $pdf->ChapterTitle("Datos Personales del Asociado",25,87,123);
        $pdf->SetTextColor($r, $g, $b);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Apellido y nombre:", 1, false, 11);
        $this->formaCampo($pdf, 60, HEIGHT_FIELD_COOPERATIVA, "Fecha Nac:", 1);
        $this->formaCampo($pdf, 60, HEIGHT_FIELD_COOPERATIVA, "DNI:", 1);
        $this->formaCampo($pdf, 60, HEIGHT_FIELD_COOPERATIVA, "CUIL/CUIT:", 1,false,11);
        $this->formaCampo($pdf, 91, HEIGHT_FIELD_COOPERATIVA, "Nacionalidad:", 1);
        $this->formaCampo($pdf, 90, HEIGHT_FIELD_COOPERATIVA, "Estado Civil:", 1,false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Domicilio:", 1,false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Entre calles:", 1,false,11);
        $this->formaCampo($pdf, 60, HEIGHT_FIELD_COOPERATIVA, "Localidad:", 1);
        $this->formaCampo($pdf, 60, HEIGHT_FIELD_COOPERATIVA, "Provincia:", 1);
        $this->formaCampo($pdf, 60, HEIGHT_FIELD_COOPERATIVA, "Código Postal:", 1,false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Teléfono Particular:", 1,false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Teléfono Celular:", 1,false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Email:", 1);
    }

    function bloqueDatosLaborales(&$pdf, $r, $g, $b) {
        $pdf->ChapterTitle("Datos Laborales",25,87,123);
        $pdf->SetTextColor($r, $g, $b);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Empleador Repartición:", 1, false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Domicilio Laboral:", 1, false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Localidad:", 1, false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Provincia:", 1, false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Teléfono Laboral / Interno:", 1, false,11);
    }
    
      function bloqueDatosBancarios(&$pdf, $r, $g, $b) {
        $pdf->ChapterTitle("Datos Bancarios",25,87,123);
        $pdf->SetTextColor($r, $g, $b);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Nombre Banco:", 1, false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Nro Cuenta:", 1, false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "CBU:", 1, false,11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Titular de la Cuenta:", 1, false,11);
    }


    function bloqueAutorizaRetencion(&$pdf, $r, $g, $b) {
        $pdf->SetTextColor($r, $g, $b);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Buenos Aires ..... de .................. de 20....", 0, true, 11,'R');
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Firma.............................................", 0, true, 11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Aclaración........................................", 0, true, 11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Tipo y N° de Documento............................", 0, true, 11);
        $pdf->SetFont('Arial', 'B', 11);
        $this->formaCampo($pdf, 0, HEIGHT_FIELD_COOPERATIVA, "Certificación del promotor", 0, true, 11, 'L');
        $pdf->SetFont('Arial', '', 11);
        $pdf->imprimeArchivoTexto(PATH_TEXTOS_REPORTES . "certificado_promotor");
        $this->formaCampo($pdf, 80, HEIGHT_FIELD_COOPERATIVA, "Nombre y Apellido", 0, true);
        $this->formaCampo($pdf, 55, HEIGHT_FIELD_COOPERATIVA, "Firma", 0, true);
        $this->formaCampo($pdf, 80, HEIGHT_FIELD_COOPERATIVA, "DNI", 0, true,18);
        $pdf->SetFont('Arial', '', 9);
        $pdf->MultiCell(0, 5, utf8_decode("SR POSTULANTE A SOCIO NO PERMITA BAJO NINGUN CONCEPTO QUE LE COBREN SUMAS DE DINERO EN EFECTIVO PARA GESTIONAR SU SOLICITUD COMO SOCIO O CREDITOS"),1,'C');
    }

    function formularioAdhesion(&$pdf, $r, $g, $b) {
        $pdf->SetFont('Arial', 'B', 13);
        $pdf->Ln(20);
        $pdf->Image(PATH_IMG . 'user1-128x128.jpg', 18.2, 45, 27);
        $this->formaCampo($pdf, 0, 14, "SOLICITUD DE ASOCIACIÓN", 0, false, 15,'C');
        $pdf->Ln(10);
        $pdf->SetFont('Arial', '',11);
        $pdf->imprimeArchivoTexto(PATH_TEXTOS_REPORTES . "cop_alta_socio");
        $this->bloqueDatosPersonales($pdf, $r, $g, $b);
        $pdf->AddPage();
        $this->bloqueDatosLaborales($pdf, $r, $g, $b);
        $pdf->Ln(3);
        $this->bloqueDatosBancarios($pdf, $r, $g, $b);
        $this->bloqueAutorizaRetencion($pdf, $r, $g, $b);
    }

    function clienteDatosPersonales() {
        $pdf = new Argit_PDF('P', 'mm', 'A4', "METROPOLITANA","COOPERATIVA DE VIVIENDA, CREDITO, CONSUMO Y PROVISION", base_url("public/img/logo_cooperativa.png"));
        $pdf->SetLeftMargin(18);
        $pdf->AddPage();
        $this->formularioAdhesion($pdf, 0, 0, 0);

        return $pdf;
    }

    public function __construct() {
        parent::__construct();
        $this->load->library("excel");
    }


}
