<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * class Reporte_controller
 * @property Cliente_model $cliente
 * @property Ubicacion_model $ubicacion
 * @property Reporte_model $mutual
 * @property Reporte_cooperativa_model $cooperativa
 */
class Reporte_controller extends AW_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("reporte/reporte_model", "reporte");
        $this->load->model("reporte/reporte_cooperativa_model", "cooperativa");
    }

    function contratoLocacion($con_id=false) {
        $pdf = $this->reporte->clienteContratoLocacion($con_id);
        $pdf->Output();
    }


}
