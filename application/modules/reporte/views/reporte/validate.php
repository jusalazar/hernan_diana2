
<script>
    jQuery.validator.addMethod("espnumber", function (value, element) {
        return this.optional(element) || /^(\d+|\d+,\d{1,2})$/.test(value);
    }, "Especifique un formato de nro válido");

    var validate_form_function = form.validate({
        lang: 'es'
    });

</script>

