<?php
/**@var $cliente \Cliente\Cliente */
$atributos = array("id" => "formCliente", "class" => "form-horizontal");
echo form_open("cliente/cliente_controller/guardar", $atributos);
?>
    <input type="hidden" name="id" value="<?= $cliente->getId() ?>" id="id" required>
    <div class="form-group">
        <label for="nombre" class="col-sm-1 control-label">NOMBRE</label>
        <div class="col-sm-3">
            <?php
            $data = array(
                'name'      => 'nombre',
                'id'        => 'nombre',
                'value'     => $cliente->getNombre(),
                'maxlength' => '255',
                'class'     => 'form-control',
                'required'  => 'true'
            );
            echo form_input($data); ?>
        </div>
        <label for="razon_social" class="col-sm-1 control-label" style="padding-left:0;">RAZON SOCIAL</label>
        <div class="col-sm-3">
            <?php
            $data = array(
                'name'      => 'razon_social',
                'id'        => 'razon_social',
                'value'     => $cliente->getRazonSocial(),
                'maxlength' => '255',
                'class'     => 'form-control',
                'required'  => 'true'
            );
            echo form_input($data); ?>
        </div>
        <label for="telefono" class="col-sm-1 control-label">TELEFONO</label>
        <div class="col-sm-3">
            <?php
            $data = array(
                'name'      => 'telefono',
                'id'        => 'telefono',
                'value'     => $cliente->getTelefono(),
                'maxlength' => '64',
                'class'     => 'form-control',
                'espnumber' => 'true'
            );
            echo form_input($data); ?>
        </div>
    </div>

    <div class="form-group">

        <label for="email" class="col-sm-1 control-label">E-MAIL</label>
        <div class="col-sm-3">
            <?php
            $data = array(
                'name'      => 'email',
                'id'        => 'email',
                'value'     => $cliente->getEmail(),
                'maxlength' => '64',
                'class'     => 'form-control',
                'type'      => 'email'
            );
            echo form_input($data); ?>
        </div>
        <?php if (get_rol_id() == ADMIN_ID) : ?>
            <label for="vendedor" class="col-sm-1 control-label">VENDEDOR</label>
            <div class="col-sm-3">
                <?php
                $data = array(
                    'name'     => 'vendedor',
                    'id'       => 'vendedor',
                    'selected' => $cliente->getVendedor()->getId(),
                    'class'    => 'form-control',
                    'required' => 'true',
                    'options'  => $vendedores
                );
                echo form_dropdown($data); ?>
            </div>
        <?php endif; ?>
        <label for="segmento" class="col-sm-1 control-label">SEGMENTO</label>
        <div class="col-sm-3">
            <?php
            $data = array(
                'name'     => 'segmento',
                'id'       => 'segmento',
                'selected' => $cliente->getSegmento()->getId(),
                'class'    => 'form-control',
                'required' => 'true',
                'options'  => $segmentos
            );
            echo form_dropdown($data); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="activo" class="col-sm-1 control-label">ACTIVO</label>
        <div class="col-sm-3">
            <?php
            $data = array(
                'name'     => 'activo',
                'id'       => 'activo',
                'selected' => '1',
                'class'    => 'form-control',
                'required' => 'true',
                'options'  => array("0" => "INACTIVO", "1" => "ACTIVO")
            );
            echo form_dropdown($data); ?>
        </div>
    </div>
    <br>
    <h4 class="box-title">&nbsp;&nbsp;Domicilio</h4>
    <div class="divider"></div>
    <div id="domicilio">
        <?php
        $datos['domicilio'] = $cliente->getDomicilios()[0];
        $datos['contador'] = 0;
        $this->load->view("cliente/domicilio", $datos); ?>
    </div>
    <br>
    <div id="renglones">
        <?php $this->load->view("cliente/facturacion"); ?>
    </div>

    <div id="contactos">
        <?php $this->load->view("cliente/contacto"); ?>
    </div>
    <br>
    <div class="divider"></div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success pull-right">Guardar</button>
            <a href="javascript:void(0);" class="btn btn-info pull-right" onclick="limpiarForm();return false;"
               style="margin-right: 5px;">Nuevo</a>
        </div>
    </div>
<?php echo form_close(); ?>