<style>
    .bootstrap-table {
        text-transform: uppercase;
    }
    .nouppercase {
        text-transform: none;
    }
</style>
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php
            $atributos = array("id" => "formReporte", "class" => "form-horizontal");
            echo form_open("", $atributos);
            ?>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="hidden" name="reporte" value="reporte-general">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-primary">Generar reporte</button>
                </div>
            </div>
            </form>

        </div>

    </div>

</section>

</div>