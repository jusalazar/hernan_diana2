<?php
/* * @var $cliente \Cliente\Cliente */

$contador = 10;
foreach ($cliente->getFacturaciones() as $facturacion):
    ?>
    <div class="renglon">
        <h4 class="box-title">&nbsp;&nbsp;Domicilio facturación
            <a href="#" onclick="borrarRenglon($(this));return false;" class="btn btn-danger pull-right"
               style="margin-left:5px;position: relative;top: -10px;"><strong>-</strong></a>
            <a href="#" onclick="agregarRenglon($(this));return false;"
               class="btn btn-success pull-right" style="position: relative;top: -10px;"><strong>+</strong></a>
        </h4>
        <div class="divider"></div>
        <div class="form-group">
            <label for="fac_cuit" class="col-sm-1 control-label">CUIT</label>
            <?php echo form_hidden('fac_id[' . $contador . ']', $facturacion->getId()); ?>
            <div class="col-sm-2">
                <?php
                $data = array(
                    'name' => 'fac_cuit[' . $contador . ']',
                    'value' => $facturacion->getCuit(),
                    'class' => 'form-control',
                    'required' => 'true',
                    'minlength' => '10',
                    'espnumber' => 'true'
                );
                echo form_input($data);
                ?>
            </div>
            <label for="fac_razon_social" class="col-sm-1 control-label" style="padding-left:0;">RAZON SOCIAL</label>
            <div class="col-sm-8">
                <?php
                $data = array(
                    'name' => 'fac_razon_social[' . $contador . ']',
                    'value' => $facturacion->getRazonSocial(),
                    'maxlength' => '255',
                    'class' => 'form-control',
                    'required' => 'true'
                );
                echo form_input($data);
                ?>
            </div>
        </div>

        <?php
        $datos['domicilios'] = $facturacion->getDomicilio();
        $datos['contador'] = $contador;
        $this->load->view("cliente/domicilio", $datos);
        ?>
    </div>
    <?php $contador++;
endforeach;
?>
