<?php
/* * @var $cliente \Cliente\Cliente */
$contador = 100;
foreach ($cliente->getContactos() as $contacto):
    ?>
    <div class="renglonContacto">
        <h4 class="box-title">&nbsp;&nbsp;Contacto
            <a href="#" onclick="borrarRenglonContacto($(this));return false;" class="btn btn-danger pull-right"
               style="margin-left:5px;position: relative;top: -10px;"><strong>-</strong></a>
            <a href="#" onclick="agregarRenglonContacto($(this));return false;"
               class="btn btn-success pull-right" style="position: relative;top: -10px;"><strong>+</strong></a>
        </h4>

        <div class="divider"></div>
        <div class="form-group">
            <label for="contacto_nombre" class="col-sm-1 control-label">NOMBRE Y APELLIDO</label>
            <div class="col-sm-2" style="padding-right: 0;">
                <?php
                $data = array(
                    'name' => 'contacto_nombre[' . $contador . ']',
                    'value' => $contacto->getNombre(),
                    'class' => 'form-control contacto_nombre',
                    'required' => 'true'
                );
                echo form_input($data);
                ?>
            </div>
            <label for="email" class="col-sm-1 control-label">Email</label>
            <div class="col-sm-2">
                <?php
                $data = array(
                    'name' => 'contacto_email[' . $contador . ']',
                    'value' => $contacto->getEmail(),
                    'class' => 'form-control contacto_email',
                );
                echo form_input($data);
                ?>
            </div>
            <label for="celular" class="col-sm-1 control-label">CELULAR</label>
            <div class="col-sm-2">
                <?php
                $data = array(
                    'name' => 'contacto_celular[' . $contador . ']',
                    'value' => $contacto->getCelular(),
                    'class' => 'form-control contacto_celular',
                );
                echo form_input($data);
                ?>
            </div>
            <label for="telefono" class="col-sm-1 control-label">TELEFONO</label>
            <div class="col-sm-2">
                <?php
                $data = array(
                    'name' => 'contacto_telefono[' . $contador . ']',
                    'value' => $contacto->getTelefono(),
                    'class' => 'form-control contacto_telefono',
                );
                echo form_input($data);
                ?>
            </div>

            <br><br>

        </div>
    </div>
    <?php
    $contador++;
endforeach;
?>

