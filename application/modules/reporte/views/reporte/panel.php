<?php
/**@var $cliente \Cliente\Cliente */
?>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">CLIENTE Nº <?= $cliente->getId() ?></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>

            </div>
        </div>
        <div class="box-body">
            <!-- context menu -->


            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Resumen</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Editar</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Pedidos</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <?php $this->load->view("cliente/resumen"); ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <?php
                            $this->load->view("cliente/formulario"); ?>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            En construcción.
                        </div>


                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>


        </div>
        <!-- /.box-body -->

        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>

</div>
