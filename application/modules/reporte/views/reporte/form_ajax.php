<style>
    [data-notify="progressbar"] {
        margin-bottom: 0px;
        position: absolute;
        bottom: 0px;
        left: 0px;
        width: 100%;
        height: 5px;
    }
</style>

<script>


    $(document).ready(function () {
        var options = {
            beforeSubmit: reValidar, // pre-submit callback
            success: showResponse, // post-submit callback

            // other available options:
            //url:       url         // override for form's 'action' attribute
            //type:      type        // 'get' or 'post', override for form's 'method' attribute
            dataType: 'json', // 'xml', 'script', or 'json' (expected server response type)
            //clearForm: true        // clear all form fields after successful submit
            //resetForm: true        // reset the form after successful submit

            // $.ajax options can be used here too, for example:
            timeout: 3000
        };


        // bind to the form's submit event
        form.submit(function () {

            // inside event callbacks 'this' is the DOM element so we first
            // wrap it in a jQuery object and then invoke ajaxSubmit
            if ($("#formCliente").valid()) {
                $(this).ajaxSubmit(options);
                window.location.href = get_base_url() + "clientes";
            }

            // !!! Important !!!
            // always return false to prevent standard browser submit and page navigation
            return false;
        });


    });

    function reValidar(formData, jqForm, options) {
        $(".contacto_nombre").each(function () {
            console.log($(this));
            if($(this).val().length == 0) {
                alert("falta nombre");
            }
        });

        return true;
    }


    // post-submit callback
    function showResponse(responseText, statusText, xhr, $form) {
        notificar(responseText.mensaje, responseText.exito);
        //table.bootstrapTable("refresh");
        //limpiarForm();

    }

    function limpiarForm() {
        id.val(0);
        form[0].reset();
    }


</script>

