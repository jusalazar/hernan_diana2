<?php
/**@var $cliente \Cliente\Cliente */
?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4><strong>CLIENTE</strong></h4>
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Razon social</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Vendedor</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= $cliente->getNombre() ?></td>
                <td><?= $cliente->getRazonSocial() ?></td>
                <td><?= $cliente->getEmail() ?></td>
                <td><?= $cliente->getTelefono() ?></td>
                <td><?= mb_strtoupper($cliente->getVendedor()->getNombrePersona(), "utf8"); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4><strong>DOMICILIO</strong></h4>
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th>Calle</th>
                <th>Nro</th>
                <th>Piso</th>
                <th>Depto</th>
                <th>C.P.</th>
                <th>País</th>
                <th>Provincia</th>
                <th>Localidad</th>
                <th>Teléfono</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cliente->getDomicilios() as $domicilio): ?>
                <tr>
                    <td><?= $domicilio->getCalle() ?></td>
                    <td><?= $domicilio->getNro() ?></td>
                    <td><?= $domicilio->getPiso() ?></td>
                    <td><?= $domicilio->getDepto() ?></td>
                    <td><?= $domicilio->getCodigoPostal() ?></td>
                    <td><?= $domicilio->getPais() ?></td>
                    <td><?= $domicilio->getProvincia() ?></td>
                    <td><?= $domicilio->getLocalidad() ?></td>
                    <td><?= $domicilio->getTelefono() ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4><strong>CONTACTOS</strong></h4>
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th>Nombre y Apellido</th>
                <th>Email</th>
                <th>Celular</th>
                <th>Teléfono</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cliente->getContactos() as $contacto): ?>
                <tr>
                    <td><?= $contacto->getNombre() ?></td>
                    <td><?= $contacto->getEmail() ?></td>
                    <td><?= $contacto->getCelular() ?></td>
                    <td><?= $contacto->getTelefono() ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4><strong>DOMICILIO FACTURACIÓN</strong></h4>
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th>CUIT</th>
                <th>Razón social</th>
                <th>Calle</th>
                <th>Nro</th>
                <th>Piso</th>
                <th>Depto</th>
                <th>C.P.</th>
                <th>País</th>
                <th>Provincia</th>
                <th>Localidad</th>
                <th>Teléfono</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cliente->getFacturaciones() as $facturacion): ?>
                <tr>
                    <td><?= $facturacion->getCuit() ?></td>
                    <td><?= $facturacion->getRazonSocial() ?></td>
                    <td><?= $facturacion->getDomicilio()->getCalle() ?></td>
                    <td><?= $facturacion->getDomicilio()->getNro() ?></td>
                    <td><?= $facturacion->getDomicilio()->getPiso() ?></td>
                    <td><?= $facturacion->getDomicilio()->getDepto() ?></td>
                    <td><?= $facturacion->getDomicilio()->getCodigoPostal() ?></td>
                    <td><?= $facturacion->getDomicilio()->getPais() ?></td>
                    <td><?= $facturacion->getDomicilio()->getProvincia() ?></td>
                    <td><?= $facturacion->getDomicilio()->getLocalidad() ?></td>
                    <td><?= $facturacion->getDomicilio()->getTelefono() ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>