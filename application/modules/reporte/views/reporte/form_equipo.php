<?php
/** @var $orden \Equipo\Orden */
?>
<style>
    [data-notify="progressbar"] {
        margin-bottom: 0px;
        position: absolute;
        bottom: 0px;
        left: 0px;
        width: 100%;
        height: 5px;
    }
</style>

<script>
    var form = $("#formEquipo");
    var extra = $("#extra");
    var material_piso = $("#material_piso");
    var material_lateral = $("#material_lateral");
    var espesor_piso = $("#espesor_piso");
    var espesor_lateral = $("#espesor_lateral");
    var formEtapas = $("#formEtapas");
    var largo = $("#largo");

    $(document).ready(function () {
        var options = {
            //beforeSubmit: showRequest, // pre-submit callback
            success: showResponse, // post-submit callback

            // other available options:
            //url:       url         // override for form's 'action' attribute
            //type:      type        // 'get' or 'post', override for form's 'method' attribute
            dataType: 'json', // 'xml', 'script', or 'json' (expected server response type)
            //clearForm: true        // clear all form fields after successful submit
            //resetForm: true        // reset the form after successful submit

            // $.ajax options can be used here too, for example:
            timeout: 3000
        };


        // bind to the form's submit event
        form.submit(function () {
            if (form.valid()) {
                $(this).ajaxSubmit(options);
            }
            return false;
        });

        formEtapas.submit(function () {
            if (form.valid()) {
                $(this).ajaxSubmit(options);
            }
            return false;
        });


        extra.select2({
            language: "es",
            theme: "bootstrap",
            placeholder: "Elija extras",
            allowClear: true,
            width: "100%"
        });

        habilitarEdicion();

    });

    var materiales = <?php echo json_encode($materialesJson)?>;

    // post-submit callback
    function showResponse(responseText, statusText, xhr, $form) {
        notificar(responseText.mensaje, responseText.exito);


    }

    function limpiarForm() {
        form[0].reset();
    }

    var estado = $(".estado");

    estado.change(function () {
        console.log($(this).val());
        if ($(this).val() == "3") {
            trPadre = $(this).parent().parent();
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = (day < 10 ? '0' : '') + day + '/' +
                (month < 10 ? '0' : '') + month + '/' + +d.getFullYear();
            trPadre.find(".fechaFin").val(output);
            trProximo = $(this).closest('tr').next('tr');
            trProximo.find(".fechaInicio").val(output);
        }
    });


    function habilitarEdicion() {
        if (largo.attr("readonly") != "readonly") {
            $('#formEquipo input').attr('readonly', 'readonly');
            $('#formEquipo select').attr('readonly', 'readonly');
        } else {
            $('#formEquipo input').removeAttr( "readonly" );
            $('#formEquipo select').removeAttr( "readonly" );
        }
    }


</script>

