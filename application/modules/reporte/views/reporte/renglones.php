<script>

    var renglon = $(".renglon").first();
    var renglonContacto = $(".renglonContacto").first();
    var facturacion = $("#renglones");
    var contactos = $("#contactos");

    var $paises = <?php echo json_encode(array_values($paises))?>;
    var $provincias =  <?php echo json_encode(array_values($provincias))?>;
    var cont = 11;
    var contContacto = 100;
    var nuevoRenglon = $('<div>').append(renglon.clone()).html();
    var nuevoRenglonContacto = $('<div>').append(renglonContacto.clone()).html();
    function agregarRenglon(btn) {
        cont++;
        html = nuevoRenglon.replace(/\[10\]/g, "["+cont+"]");
        facturacion.append(html);
        facturacion.append('<div class="divider"></div>');
        $(".renglon").last().find("input:text").val("").end();
        createTypeAhead($(".pais"), $paises);
        createTypeAhead($(".provincia"), $provincias);
    }

    function agregarRenglonContacto(btn) {
        contContacto++;
        html = nuevoRenglonContacto.replace(/\[0\]/g, "["+contContacto+"]");
        contactos.append(html);
        contactos.append('<div class="divider"></div>');
        $(".renglonContacto").last().find("input:text").val("").end();
        $('#formCliente').removeData('validator');
        $('#formCliente').validate();


    }
    function borrarRenglon(btn) {
        cajaDomicilio = $("#renglones");
        cantidad = $("#renglones > .renglon").length;

        if (cantidad > 1) {
            $(".renglon").last().remove();
            $(".divider").last().remove();


        }

    }
    function borrarRenglonContacto(btn) {
        cajaDomicilio = $("#contactos");
        cantidad = $("#contactos > .renglonContacto").length;
        console.log(cantidad);
        if (cantidad > 1) {
            $(".renglonContacto").last().remove();
            $(".divider").last().remove();
        }

    }

    function createTypeAhead($element, $source) {
        $element.typeahead({
            source: $source,
            autoSelect: true
        });
    }

    $(document).ready(function () {

        createTypeAhead($(".pais"), $paises);
        createTypeAhead($(".provincia"), $provincias);
    });


    function upperCaseF(a) {
        setTimeout(function () {
            a.value = a.value.toUpperCase();
        }, 1);
    }

</script>