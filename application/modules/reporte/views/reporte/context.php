<script src="<?= base_url("public/js/typeahead/bootstrap3-typeahead.min.js") ?>"></script>
<script>
    var form = $("#formCliente");
    var table = $("#tablaClientes");

    var $inputPais = $('.pais');
    var $inputProvincia = $('.provincia');


    $(window).load(function () {
        table.bootstrapTable({
            contextMenu: '#context-menu',
            onClickRow: function (row, $el) {
                table.find('.danger').removeClass('danger');
                $el.addClass('danger');
            },
            onContextMenuItem: function (row, $el) {
                if ($el.data("item") == "ver") {
                    var win = window.open('<?=base_url("ver-cliente-")?>' + row.id, '_blank');
                    if (win) {
                        //Browser has allowed it to be opened
                        win.focus();
                    } else {
                        //Browser has blocked it
                        alert('Please allow popups for this website');
                    }
                }
                if ($el.data("item") == "delete") {
                    avisar(row.id);

                }
            }
        });
    });

    var id = $("#id");
    var nombre = $("#nombre");
    var activo = $("#activo");
    var producto = $("#producto");
    var material_piso = $("#material_piso");
    var material_lateral = $("#material_lateral");
    var espesor_piso = $("#espesor_piso");
    var espesor_lateral = $("#espesor_lateral");
    var extra = $("#extra");


    function avisar(idCliente) {

        swal({
                title: "Está seguro?",
                text: "Los registros se perderán para siempre!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, borrar!",
                closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    borrarCliente(idCliente);
                }
                swal.close();
            });
    }

    function borrarCliente(idCliente) {
        url = "<?= base_url('cliente/cliente_controller/borrarCliente') ?>";
        $.post(url, {id: idCliente,<?= $this->security->get_csrf_token_name(); ?>:
        "<?= $this->security->get_csrf_hash() ?>"
    },
        function (data) {
            notificar(data.mensaje, data.exito);
            table.bootstrapTable("refresh");
        }

    ,
        "json"
    )
        ;
    }


</script>

