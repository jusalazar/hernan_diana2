<?php
/**@var $domicilio \Cliente\Domicilio */
?>
<div class="form-group">
    <label for="calle" class="col-sm-1 control-label">CALLE</label>
    <div class="col-sm-3">
        <?php
        $data = array(
            'name' => 'dom_calle['.$contador.']',
            'value' => $domicilio->getCalle(),
            'class' => 'form-control',
        );
        echo form_input($data); ?>
    </div>
    <label for="nro" class="col-sm-1 control-label">Nro</label>
    <div class="col-sm-1">
        <?php
        $data = array(
            'name' => 'dom_nro['.$contador.']',

            'value' => $domicilio->getNro(),
            'class' => 'form-control',
            'number' => 'true'
        );
        echo form_input($data); ?>
    </div>
    <label for="piso" class="col-sm-1 control-label">PISO</label>
    <div class="col-sm-1">
        <?php
        $data = array(
            'name' => 'dom_piso['.$contador.']',
            'id' => 'piso',
            'value' => $domicilio->getPiso(),
            'class' => 'form-control',
            'number' => 'true'
        );
        echo form_input($data); ?>
    </div>
    <label for="depto" class="col-sm-1 control-label">DEPTO</label>
    <div class="col-sm-1">
        <?php
        $data = array(
            'name' => 'dom_depto['.$contador.']',
            'id' => 'depto',
            'value' => $domicilio->getDepto(),
            'class' => 'form-control',

        );
        echo form_input($data); ?>
    </div>
    <label for="cp" class="col-sm-1 control-label">C.P.</label>
    <div class="col-sm-1">
        <?php
        $data = array(
            'name' => 'dom_cod_postal['.$contador.']',
            'id' => 'cod_postal',
            'value' => $domicilio->getCodigoPostal(),
            'class' => 'form-control',

        );
        echo form_input($data); ?>
    </div>
</div>
<div class="form-group">
    <label for="pais" class="col-sm-1 control-label">PAIS</label>
    <div class="col-sm-2">
        <?php
        $data = array(
            'name' => 'dom_pais['.$contador.']',
            'id' => 'pais',
            'value' => $domicilio->getPais(),
            'class' => 'form-control pais',
            'autocomplete' => 'off',
            'onkeydown' => 'upperCaseF(this)'


        );
        echo form_input($data); ?>
    </div>
    <label for="provincia" class="col-sm-1 control-label">PROVINCIA</label>
    <div class="col-sm-2">
        <?php
        $data = array(
            'name' => 'dom_provincia['.$contador.']',
            'id' => 'provincia',
            'value' => $domicilio->getProvincia(),
            'class' => 'form-control provincia',
            'autocomplete' => 'off',
            'onkeydown' => 'upperCaseF(this)'

        );
        echo form_input($data); ?>
    </div>
    <label for="localidad" class="col-sm-1 control-label">LOCALIDAD</label>
    <div class="col-sm-2">
        <?php
        $data = array(
            'name' => 'dom_localidad['.$contador.']',
            'id' => 'localidad',
            'value' => $domicilio->getLocalidad(),
            'class' => 'form-control',
            'onkeydown' => 'upperCaseF(this)'

        );
        echo form_input($data); ?>
    </div>
    <label for="telefono" class="col-sm-1 control-label">TELEFONO</label>
    <div class="col-sm-2">
        <?php
        $data = array(
            'name' => 'dom_telefono['.$contador.']',
            'id' => 'telefono',
            'value' => $domicilio->getTelefono(),
            'class' => 'form-control',

        );
        echo form_input($data); ?>
    </div>
    <br><br>
</div>



