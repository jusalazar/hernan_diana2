<?php

defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * @property Atributo_model $atributo
 * @property Contrato_model $contrato
 */
class Web_controller extends AW_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("admin/contacto/contacto_model", "contacto");
        $this->load->model("admin/atributo/atributo_model", "atributo");
        $this->load->model("admin/contrato/contrato_model", "contrato");
    }

    public function index() {
        $template = $this->twig->load('index.twig');
        $data["page_title"] = "Hernan Diana";
        $data["tipo_propiedades"] = $this->atributo->getTipoPropiedades();
        $data["tipo_garantias"] = $this->atributo->getTipoGarantias();
        $data["atributos_propiedades"] = $this->atributo->getAtributosPropiedades();
        $data["estado_propiedades"] = $this->atributo->getEstadosPropiedades();
        echo $template->render($data);
    }

    public function contrato_alquiler() {
        /* @var $contrato Admin\Contrato */
        if ($this->post()) {
            $data = converArraySerializeToArrayData($this->post("data"));
            $contrato = $this->contrato->insertarContratoAlquiler($data);
            $codigo = "#HD" . $contrato->getLocador_dni() . "-" . $contrato->getId();
            echo json_encode($codigo);
        } else {
            $template = $this->twig->load('index.twig');
            $data["page_title"] = "Hernan Diana";
            $data["tipo_propiedades"] = $this->atributo->getTipoPropiedades();
            $data["tipo_garantias"] = $this->atributo->getTipoGarantias();
            $data["atributos_propiedades"] = $this->atributo->getAtributosPropiedades();
            $data["estado_propiedades"] = $this->atributo->getEstadosPropiedades();
            echo $template->render($data);
        }
    }

    function contacto() {
        $data = array();
        if ($this->post()) {
            $estado = $this->contacto->insertaWeb($this->post(), "contacto");
            $data["estado"] = $estado;
        }
        $template = $this->twig->load('contacto.twig');
        echo $template->render($data);
    }

}
