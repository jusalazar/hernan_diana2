<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Utilidad_model extends A_Model {

    public function __construct() {
        parent::__construct();
    }

    public function log_js($information) {
        try {
            $mensaje = "Accion:" . $information["accion"] . " Mensaje:" . $information["mensaje"] . PHP_EOL;
            file_put_contents(JS_LOG_ERROR."log.txt", $mensaje,FILE_APPEND);
            return 1;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
    }

}
