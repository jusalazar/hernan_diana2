var id = $("#id");
var descripcion = $("#descripcion");
var fecha = $("#fecha");
var movible = $("#movible");
var form = $("#formFeriados");


function completarForm(row) {
    id.val(row.id);
    descripcion.val(row.title);
    fecha.val(row.start.format("DD/MM/YYYY"));
    movible.val(parseInt(+row.movible));
}

function avisar(id) {
    swal({
            title: "Está seguro?",
            text: "Los registros se perderán para siempre!",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, borrar!",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                destroy(id);
            }
            swal.close();
        });
}

function limpiarForm() {
    id.val(0);
    form[0].reset();
}

function destroy(id) {
    url = "admin/feriado_controller/destroy" + "/" + id;
    $.get(url, function (data) {
        notificar(data.mensaje, data.success);
    }, "json");
}
;

$(document).ready(function () {
    var options = {
        success: showResponse, // post-submit callback
        dataType: 'json', // 'xml', 'script', or 'json' (expected server response type)
        timeout: 10000
    };


    // bind to the form's submit event
    form.submit(function () {
        if (form.valid()) {
            $(this).ajaxSubmit(options);
        }
        return false;
    });
});

// post-submit callback
function showResponse(responseText, statusText, xhr, $form) {
    notificar(responseText.mensaje, responseText.success);
    limpiarForm();
    calendar.fullCalendar( 'refetchEvents' );
}

var options = {
    locale: "es",
    themeSystem: "bootstrap3",
    weekNumbers: true,
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
    },
    events: 'admin/feriado_controller/index',
    eventClick: function (calEvent, jsEvent, view) {
        console.log(calEvent.start.toDate());
        completarForm(calEvent);
    }
};
var calendar = $('#calendar');
calendar.fullCalendar(options);